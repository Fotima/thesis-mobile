import 'package:flutter/material.dart';

// Color constants used on UI
class AppColors {
  static const black = Color(0xFF1D1C24);
  // static const black = Color(0xFF201d2b);

  static const accent = Color(0xFF575ce5);
  static const accentAlt = Color(0xFF54c1fb);
  static const accentBright = Color(0xFF6d71f9);
  static const accentDark = Color(0xFF2b2e72);
  static const background = Color(0xFFf2f6fd);
  static const orange = Color(0xFFf99b4e);
  static const red = Color(0xFFeb4b4b);
  static const redTransparent = Color(0xFFfff0f1);
  static const greenTransparent = Color(0xFFe7fff5);
  static const accentTransparent = Color(0xFFdce7fa);
  static const orangeTransparent = Color(0xfffff4ea);
  static const blueTransparent = Color(0xffcaedff);
  static const mint = Color(0xFF06d0c2);
  static const mintTransparent = Color(0xFFcef2f7);

  static const green = Color(0xFF219653);
  static const lightGreen = Color(0xFF00C169);

  static const greyDisabled = Color(0xFFE7E7E7);
  static const fontColor = Color(0xFF84869A);
  static const textFieldHintColor = Color(0xFF84869A);
  static const textFieldBackground = Color(0xFFd8dcf8);
  static const textFieldBakcGrounAlt = Color(0xFFfcfbfc);

  static const appBarColor = Color(0xFF24232D);
  static const textColor = Color(0xFF6C757D);
  // static const appBarColor = Color(0xFF282731);
  static const yellow = Color(0xFFFFE984);

  static const white = Color(0xFFFFFFFF);
  static const dotColor = Color(0xFF3C3C47);
  static const grey = Color(0xFF35333F);
  static const dartGrey = Color(0xFF2C2B38);
}
