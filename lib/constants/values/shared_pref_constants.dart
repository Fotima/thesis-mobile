// Shared preference key constants

abstract class ShPrefKeys {
  static const token = 'token';
  static const firebaseToken = 'firebaseToken';
  static const initialOpen = 'initialOpen';
  static const myId = 'myId';
  static const roleRank = 'roleRank';

  static const permissions = "permissions";
}
