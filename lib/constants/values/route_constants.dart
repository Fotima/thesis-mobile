// Role constants used on navigation
class AppRoutes {
  static const authPage = "/authPage";
  static const homePage = "/homePage";
  static const newTaskPage = "/newTaskPage";
  static const profileEditPage = "/profileEditPage";
  static const schedulePage = "/schedulePage";
  static const taskArchivePage = "/taskArchivePage";
  static const taskListPage = "/taskListPage";
  static const newAnnouncementPage = "/newAnnouncementPage";
}
