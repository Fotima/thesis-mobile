// URL endpoints
class AppUrls {
  // static const base = 'http://localhost:9700/api/v1';
  // static const webSocketChat = 'ws://localhost:9700/api/v1/ws/chat';
  // static const webSocketChat = 'ws://10.0.2.2:5000/api/v1/ws/chat/room';

  // static const base = 'http://10.0.2.2:5000/api/v1';

  static const base =
      'http://ec2-54-247-109-62.eu-west-1.compute.amazonaws.com/api/v1';
  static const webSocketChat =
      'ws://ec2-54-247-109-62.eu-west-1.compute.amazonaws.com/api/v1/ws/chat';

  static const login = '/signup';
  static const emailConfirm = '/user/confirm';
  static const userContacts = '/user/contacts';
  static const userContactsRank = '/user/contacts/rank';

  static const getMyTasks = '/task/me';
  static const updateTaskStatus = '/task/status/update';
  static const myNotes = '/notes/my';
  static const createCallHistory = '/notes/create';
  static const addNoteToCallHistory = '/notes/addnote';
  static const myProfileData = '/user/mobile/profile';
  static const updateMyStatus = '/user/status';
  static const updateMyProfile = '/user/mobile/update';
  static const getAllUsers = '/user/list';
  static const usersLowerRank = '/user/byRank';

  static const updateFirebaseToken = '/firebase/save';
  static const roleLowerRank = '/role/lowerRank';
  static const createTask = '/task/create';
  static const filterTasks = '/task/filter';

  static const getMyChats = '/chat/list';
  static const sendMessage = '/chat/message/send';
  static const getMessagesOfChat = '/chat/messages';
  static const createNewChatRoom = '/chat/create';

  static const getAllAnnouncement = '/announcement/list';
  static const createAnnouncement = '/announcement/create';
}
