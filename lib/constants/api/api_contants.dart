// API constants given on the server
class ApiConstants {
  static const createAnnouncement = "CREATE_ANNOUNCEMENT";
  static const createTask = "CREATE_TASK";
  static const callAllUsers = "CALL_ALL_USERS";
  static const callUsersLowerRank = "CALL_ALL_USERS_LOWER_RANK";
  static const messageAllUsers = "MESSAGE_ALL_USERS";
  static const messageLowerRankUsers = "MESSAGE_ALL_USERS_LOWER_RANK";
}
