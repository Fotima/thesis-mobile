// Icon asset items names
class AppIcons {
  static const base = 'assets/icons';
  static const medicine = '$base/medicine.svg';
  static const note = '$base/note.svg';
  static const call = '$base/phone.svg';
  static const chat = '$base/chat.svg';
  static const checklist = '$base/checklist.svg';
  static const checklistalt = '$base/checklistalt.svg';

  static const empty = '$base/empty.svg';
  static const patients = '$base/patients.svg';
  static const user = '$base/user.svg';
  static const users = '$base/users.svg';
  static const personalInfo = '$base/personal_info.svg';
  static const personalCard = '$base/personal_card.svg';
  static const schedule = '$base/schedule.svg';
  static const checkIn = '$base/check_in.svg';
  static const checkOut = '$base/check_out.svg';
  static const archive = '$base/archive.svg';
  static const searchIcon = '$base/search.svg';

  static const logOut = '$base/log_out.svg';
  static const contacts = '$base/contacts.svg';
  static const send = '$base/send.svg';
  static const noData = '$base/no_data.svg';
  static const news = '$base/news.svg';
  static const newAnnouncement = '$base/new_announcement.svg';
}
