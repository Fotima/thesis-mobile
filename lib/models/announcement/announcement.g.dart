// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'announcement.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

AnnouncementInfo _$AnnouncementInfoFromJson(Map<String, dynamic> json) {
  return AnnouncementInfo(
    date: json['date'] as int?,
    description: json['description'] as String?,
    id: json['id'] as int?,
    title: json['title'] as String?,
    userShort: json['userShort'] == null
        ? null
        : UserShort.fromJson(json['userShort'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$AnnouncementInfoToJson(AnnouncementInfo instance) =>
    <String, dynamic>{
      'id': instance.id,
      'title': instance.title,
      'description': instance.description,
      'userShort': instance.userShort,
      'date': instance.date,
    };

AnnouncementPost _$AnnouncementPostFromJson(Map<String, dynamic> json) {
  return AnnouncementPost(
    title: json['title'] as String?,
    description: json['description'] as String?,
  );
}

Map<String, dynamic> _$AnnouncementPostToJson(AnnouncementPost instance) =>
    <String, dynamic>{
      'title': instance.title,
      'description': instance.description,
    };
