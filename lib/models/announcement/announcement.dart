import 'package:hospitalproject/models/api/user_short/user_short.dart';
import 'package:json_annotation/json_annotation.dart';

part 'announcement.g.dart';

@JsonSerializable()
class AnnouncementInfo {
  int? id;
  String? title;
  String? description;
  UserShort? userShort;
  int? date;
  AnnouncementInfo({
    this.date = 0,
    this.description = "",
    this.id = 0,
    this.title = '',
    this.userShort,
  });

  factory AnnouncementInfo.fromJson(Map<String, dynamic> json) =>
      _$AnnouncementInfoFromJson(json);

  Map<String, dynamic> toJson() => _$AnnouncementInfoToJson(this);
}

@JsonSerializable()
class AnnouncementPost {
  String? title;
  String? description;

  AnnouncementPost({required this.title, this.description});

  factory AnnouncementPost.fromJson(Map<String, dynamic> json) =>
      _$AnnouncementPostFromJson(json);

  Map<String, dynamic> toJson() => _$AnnouncementPostToJson(this);
}
