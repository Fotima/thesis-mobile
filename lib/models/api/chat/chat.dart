import 'dart:convert';

import 'package:hospitalproject/models/api/chat_room/chat_room.dart';
import 'package:hospitalproject/models/api/pageable/pageable.dart';
import 'package:hospitalproject/models/api/user_short/user_short.dart';
import 'package:json_annotation/json_annotation.dart';
part 'chat.g.dart';

@JsonSerializable()
class WSMessage {
  String? type;
  ChatCreatePost? data;
  WSMessage({this.type = 'NEW_CHAT_MESSAGE', this.data});

  factory WSMessage.fromJson(Map<String, dynamic> json) =>
      _$WSMessageFromJson(json);

  Map<String, dynamic> toJson() => _$WSMessageToJson(this);
  // String toJson() {
  //   var message = {"type": "NEW_CHAT_MESSAGE", "data": data.toJson()};
  //   return json.encode(data);
  // }
}

@JsonSerializable()
class ChatCreatePost {
  int? chatId;
  String? message;

  ChatCreatePost({this.chatId, this.message = ""});

  factory ChatCreatePost.fromJson(Map<String, dynamic> json) =>
      _$ChatCreatePostFromJson(json);

  Map<String, dynamic> toJson() => _$ChatCreatePostToJson(this);
}

@JsonSerializable()
class ChatMessagePageable {
  ChatRoom? chatRoom;
  int totalElements;
  bool last;
  Pageable? pageable;
  List<ChatMessageByDate>? content;

  ChatMessagePageable({
    this.chatRoom,
    this.content,
    this.last = true,
    this.pageable,
    this.totalElements = 0,
  });

  factory ChatMessagePageable.fromJson(Map<String, dynamic> json) =>
      _$ChatMessagePageableFromJson(json);

  Map<String, dynamic> toJson() => _$ChatMessagePageableToJson(this);
}

@JsonSerializable()
class ChatMessageByDate {
  int? date;
  List<ChatMessage>? messages;

  ChatMessageByDate({
    this.date,
    this.messages,
  });
  factory ChatMessageByDate.fromJson(Map<String, dynamic> json) =>
      _$ChatMessageByDateFromJson(json);

  Map<String, dynamic> toJson() => _$ChatMessageByDateToJson(this);
}

@JsonSerializable()
class ChatMessage {
  int? id;
  int? created;
  UserShort? fromUser;
  String message;

  ChatMessage({this.created, this.fromUser, this.id, this.message = ""});

  factory ChatMessage.fromJson(Map<String, dynamic> json) =>
      _$ChatMessageFromJson(json);

  Map<String, dynamic> toJson() => _$ChatMessageToJson(this);
}

@JsonSerializable()
class ChatMessagePost {
  int? chatId;
  int? withUserId;
  ChatMessagePost({
    this.chatId,
    this.withUserId,
  });

  factory ChatMessagePost.fromJson(Map<String, dynamic> json) =>
      _$ChatMessagePostFromJson(json);

  Map<String, dynamic> toJson() => _$ChatMessagePostToJson(this);
}
