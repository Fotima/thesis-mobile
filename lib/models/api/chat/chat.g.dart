// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'chat.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

WSMessage _$WSMessageFromJson(Map<String, dynamic> json) {
  return WSMessage(
    type: json['type'] as String?,
    data: json['data'] == null
        ? null
        : ChatCreatePost.fromJson(json['data'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$WSMessageToJson(WSMessage instance) => <String, dynamic>{
      'type': instance.type,
      'data': instance.data,
    };

ChatCreatePost _$ChatCreatePostFromJson(Map<String, dynamic> json) {
  return ChatCreatePost(
    chatId: json['chatId'] as int?,
    message: json['message'] as String?,
  );
}

Map<String, dynamic> _$ChatCreatePostToJson(ChatCreatePost instance) =>
    <String, dynamic>{
      'chatId': instance.chatId,
      'message': instance.message,
    };

ChatMessagePageable _$ChatMessagePageableFromJson(Map<String, dynamic> json) {
  return ChatMessagePageable(
    chatRoom: json['chatRoom'] == null
        ? null
        : ChatRoom.fromJson(json['chatRoom'] as Map<String, dynamic>),
    content: (json['content'] as List<dynamic>?)
        ?.map((e) => ChatMessageByDate.fromJson(e as Map<String, dynamic>))
        .toList(),
    last: json['last'] as bool,
    pageable: json['pageable'] == null
        ? null
        : Pageable.fromJson(json['pageable'] as Map<String, dynamic>),
    totalElements: json['totalElements'] as int,
  );
}

Map<String, dynamic> _$ChatMessagePageableToJson(
        ChatMessagePageable instance) =>
    <String, dynamic>{
      'chatRoom': instance.chatRoom,
      'totalElements': instance.totalElements,
      'last': instance.last,
      'pageable': instance.pageable,
      'content': instance.content,
    };

ChatMessageByDate _$ChatMessageByDateFromJson(Map<String, dynamic> json) {
  return ChatMessageByDate(
    date: json['date'] as int?,
    messages: (json['messages'] as List<dynamic>?)
        ?.map((e) => ChatMessage.fromJson(e as Map<String, dynamic>))
        .toList(),
  );
}

Map<String, dynamic> _$ChatMessageByDateToJson(ChatMessageByDate instance) =>
    <String, dynamic>{
      'date': instance.date,
      'messages': instance.messages,
    };

ChatMessage _$ChatMessageFromJson(Map<String, dynamic> json) {
  return ChatMessage(
    created: json['created'] as int?,
    fromUser: json['fromUser'] == null
        ? null
        : UserShort.fromJson(json['fromUser'] as Map<String, dynamic>),
    id: json['id'] as int?,
    message: json['message'] as String,
  );
}

Map<String, dynamic> _$ChatMessageToJson(ChatMessage instance) =>
    <String, dynamic>{
      'id': instance.id,
      'created': instance.created,
      'fromUser': instance.fromUser,
      'message': instance.message,
    };

ChatMessagePost _$ChatMessagePostFromJson(Map<String, dynamic> json) {
  return ChatMessagePost(
    chatId: json['chatId'] as int?,
    withUserId: json['withUserId'] as int?,
  );
}

Map<String, dynamic> _$ChatMessagePostToJson(ChatMessagePost instance) =>
    <String, dynamic>{
      'chatId': instance.chatId,
      'withUserId': instance.withUserId,
    };
