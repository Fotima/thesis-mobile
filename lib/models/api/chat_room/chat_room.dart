import 'package:hospitalproject/models/api/user_short/user_short.dart';
import 'package:json_annotation/json_annotation.dart';

part 'chat_room.g.dart';

@JsonSerializable()
class ChatRoom {
  int? chatId;
  int? createdAt;
  int? lastMessageTime;
  List<UserShort>? listOfUser;
  String? lastMessage;

  ChatRoom({
    this.chatId,
    this.createdAt,
    this.lastMessageTime,
    this.listOfUser,
    this.lastMessage,
  });

  factory ChatRoom.fromJson(Map<String, dynamic> json) =>
      _$ChatRoomFromJson(json);

  Map<String, dynamic> toJson() => _$ChatRoomToJson(this);
}
