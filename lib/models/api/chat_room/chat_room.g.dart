// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'chat_room.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ChatRoom _$ChatRoomFromJson(Map<String, dynamic> json) {
  return ChatRoom(
    chatId: json['chatId'] as int?,
    createdAt: json['createdAt'] as int?,
    lastMessageTime: json['lastMessageTime'] as int?,
    listOfUser: (json['listOfUser'] as List<dynamic>?)
        ?.map((e) => UserShort.fromJson(e as Map<String, dynamic>))
        .toList(),
    lastMessage: json['lastMessage'] as String?,
  );
}

Map<String, dynamic> _$ChatRoomToJson(ChatRoom instance) => <String, dynamic>{
      'chatId': instance.chatId,
      'createdAt': instance.createdAt,
      'lastMessageTime': instance.lastMessageTime,
      'listOfUser': instance.listOfUser,
      'lastMessage': instance.lastMessage,
    };
