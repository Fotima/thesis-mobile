import 'package:json_annotation/json_annotation.dart';

part 'pageable.g.dart';

@JsonSerializable()
class Pageable {
  PageSort sort;

  int offset;

  int pageSize;

  int pageNumber;

  bool paged;

  bool unpaged;

  Pageable(
    this.sort,
    this.offset,
    this.paged,
    this.pageNumber,
    this.pageSize,
    this.unpaged,
  );

  factory Pageable.fromJson(Map<String, dynamic> json) =>
      _$PageableFromJson(json);

  Map<String, dynamic> toJson() => _$PageableToJson(this);
}

@JsonSerializable()
class PageSort {
  bool sorted;

  bool unsorted;

  bool empty;

  PageSort(this.sorted, this.unsorted, this.empty);

  factory PageSort.fromJson(Map<String, dynamic> json) =>
      _$PageSortFromJson(json);

  Map<String, dynamic> toJson() => _$PageSortToJson(this);
}
