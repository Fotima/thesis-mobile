// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'pageable.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Pageable _$PageableFromJson(Map<String, dynamic> json) {
  return Pageable(
    PageSort.fromJson(json['sort'] as Map<String, dynamic>),
    json['offset'] as int,
    json['paged'] as bool,
    json['pageNumber'] as int,
    json['pageSize'] as int,
    json['unpaged'] as bool,
  );
}

Map<String, dynamic> _$PageableToJson(Pageable instance) => <String, dynamic>{
      'sort': instance.sort,
      'offset': instance.offset,
      'pageSize': instance.pageSize,
      'pageNumber': instance.pageNumber,
      'paged': instance.paged,
      'unpaged': instance.unpaged,
    };

PageSort _$PageSortFromJson(Map<String, dynamic> json) {
  return PageSort(
    json['sorted'] as bool,
    json['unsorted'] as bool,
    json['empty'] as bool,
  );
}

Map<String, dynamic> _$PageSortToJson(PageSort instance) => <String, dynamic>{
      'sorted': instance.sorted,
      'unsorted': instance.unsorted,
      'empty': instance.empty,
    };
