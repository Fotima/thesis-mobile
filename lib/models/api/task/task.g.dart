// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'task.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

TaskCreate _$TaskCreateFromJson(Map<String, dynamic> json) {
  return TaskCreate(
    assignedRoleId: json['assignedRoleId'] as int?,
    assignedUserIds: (json['assignedUserIds'] as List<dynamic>?)
        ?.map((e) => e as int)
        .toList(),
    description: json['description'] as String?,
    priority: json['priority'] as int?,
    taskEnd: json['taskEnd'] as String?,
    taskPlacement: json['taskPlacement'] as String?,
    taskStart: json['taskStart'] as String?,
    title: json['title'] as String?,
  );
}

Map<String, dynamic> _$TaskCreateToJson(TaskCreate instance) =>
    <String, dynamic>{
      'title': instance.title,
      'description': instance.description,
      'assignedUserIds': instance.assignedUserIds,
      'assignedRoleId': instance.assignedRoleId,
      'taskPlacement': instance.taskPlacement,
      'taskStart': instance.taskStart,
      'taskEnd': instance.taskEnd,
      'priority': instance.priority,
    };

TaskInfo _$TaskInfoFromJson(Map<String, dynamic> json) {
  return TaskInfo(
    assignedDate: json['assignedDate'] as int,
    assignedRole: json['assignedRole'] == null
        ? null
        : RoleShort.fromJson(json['assignedRole'] as Map<String, dynamic>),
    assignedUsers: (json['assignedUsers'] as List<dynamic>?)
        ?.map((e) => UserShort.fromJson(e as Map<String, dynamic>))
        .toList(),
    creatorUser:
        UserShort.fromJson(json['creatorUser'] as Map<String, dynamic>),
    description: json['description'] as String,
    id: json['id'] as int,
    priority: json['priority'] as int,
    status: json['status'] as int,
    taskEnd: json['taskEnd'] as int?,
    taskPlacement: json['taskPlacement'] as String,
    taskStart: json['taskStart'] as int?,
    title: json['title'] as String,
    lastModifiedBy: json['lastModifiedBy'] == null
        ? null
        : UserShort.fromJson(json['lastModifiedBy'] as Map<String, dynamic>),
    lastModifiedDate: json['lastModifiedDate'] as int?,
    assignedUserNames: (json['assignedUserNames'] as List<dynamic>?)
        ?.map((e) => e as String)
        .toList(),
  );
}

Map<String, dynamic> _$TaskInfoToJson(TaskInfo instance) => <String, dynamic>{
      'id': instance.id,
      'title': instance.title,
      'description': instance.description,
      'assignedUsers': instance.assignedUsers,
      'assignedUserNames': instance.assignedUserNames,
      'creatorUser': instance.creatorUser,
      'assignedRole': instance.assignedRole,
      'taskPlacement': instance.taskPlacement,
      'assignedDate': instance.assignedDate,
      'taskStart': instance.taskStart,
      'taskEnd': instance.taskEnd,
      'status': instance.status,
      'priority': instance.priority,
      'lastModifiedBy': instance.lastModifiedBy,
      'lastModifiedDate': instance.lastModifiedDate,
    };

TaskStatusPost _$TaskStatusPostFromJson(Map<String, dynamic> json) {
  return TaskStatusPost(
    status: json['status'] as int,
    taskId: json['taskId'] as int,
  );
}

Map<String, dynamic> _$TaskStatusPostToJson(TaskStatusPost instance) =>
    <String, dynamic>{
      'taskId': instance.taskId,
      'status': instance.status,
    };

TaskFilterPost _$TaskFilterPostFromJson(Map<String, dynamic> json) {
  return TaskFilterPost(
    assignedUsers: (json['assignedUsers'] as List<dynamic>?)
        ?.map((e) => e as int)
        .toList(),
    createdUsers:
        (json['createdUsers'] as List<dynamic>?)?.map((e) => e as int).toList(),
    fromDate: json['fromDate'] as int?,
    myTasks: json['myTasks'] as bool,
    toDate: json['toDate'] as int?,
  );
}

Map<String, dynamic> _$TaskFilterPostToJson(TaskFilterPost instance) =>
    <String, dynamic>{
      'fromDate': instance.fromDate,
      'toDate': instance.toDate,
      'assignedUsers': instance.assignedUsers,
      'createdUsers': instance.createdUsers,
      'myTasks': instance.myTasks,
    };
