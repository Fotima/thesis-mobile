import 'package:hospitalproject/models/api/role/role.dart';
import 'package:hospitalproject/models/api/user_short/user_short.dart';
import 'package:json_annotation/json_annotation.dart';
part 'task.g.dart';

@JsonSerializable()
class TaskCreate {
  String? title;
  String? description;
  List<int>? assignedUserIds;
  int? assignedRoleId;
  String? taskPlacement;
  String? taskStart;
  String? taskEnd;
  int? priority;
  TaskCreate({
    this.assignedRoleId,
    this.assignedUserIds,
    this.description,
    this.priority,
    this.taskEnd,
    this.taskPlacement,
    this.taskStart,
    this.title = "",
  });

  factory TaskCreate.fromJson(Map<String, dynamic> json) =>
      _$TaskCreateFromJson(json);

  Map<String, dynamic> toJson() => _$TaskCreateToJson(this);
}

@JsonSerializable()
class TaskInfo {
  int id;
  String title;
  String description;
  List<UserShort>? assignedUsers;
  List<String>? assignedUserNames;
  UserShort creatorUser;
  RoleShort? assignedRole;
  String taskPlacement;
  int assignedDate;
  int? taskStart;
  int? taskEnd;
  int status;
  int priority;
  UserShort? lastModifiedBy;
  int? lastModifiedDate;
  TaskInfo({
    required this.assignedDate,
    this.assignedRole,
    this.assignedUsers,
    required this.creatorUser,
    this.description = '',
    required this.id,
    required this.priority,
    required this.status,
    this.taskEnd,
    this.taskPlacement = "",
    this.taskStart,
    required this.title,
    this.lastModifiedBy,
    this.lastModifiedDate,
    this.assignedUserNames,
  });

  factory TaskInfo.fromJson(Map<String, dynamic> json) =>
      _$TaskInfoFromJson(json);

  Map<String, dynamic> toJson() => _$TaskInfoToJson(this);
}

@JsonSerializable()
class TaskStatusPost {
  int taskId;
  int status;

  TaskStatusPost({required this.status, required this.taskId});

  factory TaskStatusPost.fromJson(Map<String, dynamic> json) =>
      _$TaskStatusPostFromJson(json);

  Map<String, dynamic> toJson() => _$TaskStatusPostToJson(this);
}

@JsonSerializable()
class TaskFilterPost {
  int? fromDate;
  int? toDate;
  List<int>? assignedUsers;
  List<int>? createdUsers;
  bool myTasks;

  TaskFilterPost({
    this.assignedUsers,
    this.createdUsers,
    this.fromDate,
    this.myTasks = false,
    this.toDate,
  });

  factory TaskFilterPost.fromJson(Map<String, dynamic> json) =>
      _$TaskFilterPostFromJson(json);

  Map<String, dynamic> toJson() => _$TaskFilterPostToJson(this);
}
