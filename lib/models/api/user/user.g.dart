// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'user.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

UserProfile _$UserProfileFromJson(Map<String, dynamic> json) {
  return UserProfile(
    checkedIn: json['checkedIn'] as bool,
    dob: json['dob'] as int,
    email: json['email'] as String,
    employeeId: json['employeeId'] as String,
    firstName: json['firstName'] as String,
    id: json['id'] as int,
    lastName: json['lastName'] as String,
    phoneNumber: json['phoneNumber'] as String,
    schedule: (json['schedule'] as List<dynamic>?)
        ?.map((e) => WorkingHour.fromJson(e as Map<String, dynamic>))
        .toList(),
    role: json['role'] as String,
  );
}

Map<String, dynamic> _$UserProfileToJson(UserProfile instance) =>
    <String, dynamic>{
      'id': instance.id,
      'email': instance.email,
      'firstName': instance.firstName,
      'lastName': instance.lastName,
      'phoneNumber': instance.phoneNumber,
      'dob': instance.dob,
      'employeeId': instance.employeeId,
      'checkedIn': instance.checkedIn,
      'schedule': instance.schedule,
      'role': instance.role,
    };

WorkingHour _$WorkingHourFromJson(Map<String, dynamic> json) {
  return WorkingHour(
    endTime: json['endTime'] as String,
    name: json['name'] as String,
    startTime: json['startTime'] as String,
  );
}

Map<String, dynamic> _$WorkingHourToJson(WorkingHour instance) =>
    <String, dynamic>{
      'name': instance.name,
      'startTime': instance.startTime,
      'endTime': instance.endTime,
    };

UserProfileUpdatePost _$UserProfileUpdatePostFromJson(
    Map<String, dynamic> json) {
  return UserProfileUpdatePost(
    firstName: json['firstName'] as String,
    lastName: json['lastName'] as String,
    phoneNumber: json['phoneNumber'] as String,
  );
}

Map<String, dynamic> _$UserProfileUpdatePostToJson(
        UserProfileUpdatePost instance) =>
    <String, dynamic>{
      'firstName': instance.firstName,
      'lastName': instance.lastName,
      'phoneNumber': instance.phoneNumber,
    };

UserStatusPost _$UserStatusPostFromJson(Map<String, dynamic> json) {
  return UserStatusPost(
    status: json['status'] as bool,
  );
}

Map<String, dynamic> _$UserStatusPostToJson(UserStatusPost instance) =>
    <String, dynamic>{
      'status': instance.status,
    };

UserByRole _$UserByRoleFromJson(Map<String, dynamic> json) {
  return UserByRole(
    role: json['role'] as String,
    users: (json['users'] as List<dynamic>)
        .map((e) => UserShort.fromJson(e as Map<String, dynamic>))
        .toList(),
  );
}

Map<String, dynamic> _$UserByRoleToJson(UserByRole instance) =>
    <String, dynamic>{
      'role': instance.role,
      'users': instance.users,
    };
