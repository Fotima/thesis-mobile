import 'package:hospitalproject/models/api/role/role.dart';
import 'package:hospitalproject/models/api/user_short/user_short.dart';
import 'package:json_annotation/json_annotation.dart';
part 'user.g.dart';

@JsonSerializable()
class UserProfile {
  int id;
  String email;
  String firstName;
  String lastName;
  String phoneNumber;
  int dob;
  String employeeId;
  bool checkedIn;
  List<WorkingHour>? schedule;
  String role;

  UserProfile({
    this.checkedIn = false,
    this.dob = 0,
    this.email = "",
    this.employeeId = "",
    this.firstName = "",
    this.id = 0,
    this.lastName = "",
    this.phoneNumber = "",
    this.schedule,
    this.role = "",
  });
  factory UserProfile.fromJson(Map<String, dynamic> json) =>
      _$UserProfileFromJson(json);

  Map<String, dynamic> toJson() => _$UserProfileToJson(this);
}

@JsonSerializable()
class WorkingHour {
  String name;
  String startTime;
  String endTime;

  WorkingHour(
      {required this.endTime, required this.name, required this.startTime});

  factory WorkingHour.fromJson(Map<String, dynamic> json) =>
      _$WorkingHourFromJson(json);

  Map<String, dynamic> toJson() => _$WorkingHourToJson(this);
}

@JsonSerializable()
class UserProfileUpdatePost {
  String firstName;
  String lastName;
  String phoneNumber;

  UserProfileUpdatePost({
    this.firstName = "",
    this.lastName = "",
    this.phoneNumber = "",
  });

  factory UserProfileUpdatePost.fromJson(Map<String, dynamic> json) =>
      _$UserProfileUpdatePostFromJson(json);

  Map<String, dynamic> toJson() => _$UserProfileUpdatePostToJson(this);
}

@JsonSerializable()
class UserStatusPost {
  bool status;
  UserStatusPost({required this.status});

  factory UserStatusPost.fromJson(Map<String, dynamic> json) =>
      _$UserStatusPostFromJson(json);

  Map<String, dynamic> toJson() => _$UserStatusPostToJson(this);
}

@JsonSerializable()
class UserByRole {
  String role;
  List<UserShort> users;

  UserByRole({required this.role, required this.users});

  factory UserByRole.fromJson(Map<String, dynamic> json) =>
      _$UserByRoleFromJson(json);

  Map<String, dynamic> toJson() => _$UserByRoleToJson(this);
}
