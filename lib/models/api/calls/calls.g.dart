// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'calls.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CallsHistory _$CallsHistoryFromJson(Map<String, dynamic> json) {
  return CallsHistory(
    callTime: json['callTime'] as int,
    calledUser: UserShort.fromJson(json['calledUser'] as Map<String, dynamic>),
    callingUser:
        UserShort.fromJson(json['callingUser'] as Map<String, dynamic>),
    id: json['id'] as int,
    note: json['note'] as String?,
  );
}

Map<String, dynamic> _$CallsHistoryToJson(CallsHistory instance) =>
    <String, dynamic>{
      'id': instance.id,
      'callTime': instance.callTime,
      'callingUser': instance.callingUser,
      'calledUser': instance.calledUser,
      'note': instance.note,
    };

CallHistoryPost _$CallHistoryPostFromJson(Map<String, dynamic> json) {
  return CallHistoryPost(
    calledUserId: json['calledUserId'] as int,
  );
}

Map<String, dynamic> _$CallHistoryPostToJson(CallHistoryPost instance) =>
    <String, dynamic>{
      'calledUserId': instance.calledUserId,
    };

CallHistoryNotePost _$CallHistoryNotePostFromJson(Map<String, dynamic> json) {
  return CallHistoryNotePost(
    id: json['id'] as int,
    note: json['note'] as String,
  );
}

Map<String, dynamic> _$CallHistoryNotePostToJson(
        CallHistoryNotePost instance) =>
    <String, dynamic>{
      'id': instance.id,
      'note': instance.note,
    };
