import 'package:hospitalproject/models/api/user_short/user_short.dart';
import 'package:json_annotation/json_annotation.dart';

part 'calls.g.dart';

@JsonSerializable()
class CallsHistory {
  int id;
  int callTime;
  UserShort callingUser;
  UserShort calledUser;
  String? note;

  CallsHistory({
    required this.callTime,
    required this.calledUser,
    required this.callingUser,
    required this.id,
    this.note,
  });

  factory CallsHistory.fromJson(Map<String, dynamic> json) =>
      _$CallsHistoryFromJson(json);

  Map<String, dynamic> toJson() => _$CallsHistoryToJson(this);
}

@JsonSerializable()
class CallHistoryPost {
  int calledUserId;

  CallHistoryPost({required this.calledUserId});

  factory CallHistoryPost.fromJson(Map<String, dynamic> json) =>
      _$CallHistoryPostFromJson(json);

  Map<String, dynamic> toJson() => _$CallHistoryPostToJson(this);
}

@JsonSerializable()
class CallHistoryNotePost {
  int id;
  String note;

  CallHistoryNotePost({
    required this.id,
    required this.note,
  });
  factory CallHistoryNotePost.fromJson(Map<String, dynamic> json) =>
      _$CallHistoryNotePostFromJson(json);

  Map<String, dynamic> toJson() => _$CallHistoryNotePostToJson(this);
}
