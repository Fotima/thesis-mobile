import 'package:hospitalproject/models/api/role/role.dart';
import 'package:json_annotation/json_annotation.dart';
part 'user_short.g.dart';

@JsonSerializable()
class UserShort {
  int id;
  String firstName;
  String lastName;
  RoleShort? role;
  String? phoneNumber;

  UserShort(
      {required this.firstName,
      this.lastName = "",
      required this.id,
      this.phoneNumber,
      this.role});

  factory UserShort.fromJson(Map<String, dynamic> json) =>
      _$UserShortFromJson(json);

  Map<String, dynamic> toJson() => _$UserShortToJson(this);
}

@JsonSerializable()
class UserShortAuth {
  int id;
  String firstName;
  String lastName;
  UserRole? role;

  UserShortAuth(
      {required this.firstName,
      this.lastName = "",
      required this.id,
      this.role});

  factory UserShortAuth.fromJson(Map<String, dynamic> json) =>
      _$UserShortAuthFromJson(json);

  Map<String, dynamic> toJson() => _$UserShortAuthToJson(this);
}
