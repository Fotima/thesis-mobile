// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'user_short.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

UserShort _$UserShortFromJson(Map<String, dynamic> json) {
  return UserShort(
    firstName: json['firstName'] as String,
    lastName: json['lastName'] as String,
    id: json['id'] as int,
    phoneNumber: json['phoneNumber'] as String?,
    role: json['role'] == null
        ? null
        : RoleShort.fromJson(json['role'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$UserShortToJson(UserShort instance) => <String, dynamic>{
      'id': instance.id,
      'firstName': instance.firstName,
      'lastName': instance.lastName,
      'role': instance.role,
      'phoneNumber': instance.phoneNumber,
    };

UserShortAuth _$UserShortAuthFromJson(Map<String, dynamic> json) {
  return UserShortAuth(
    firstName: json['firstName'] as String,
    lastName: json['lastName'] as String,
    id: json['id'] as int,
    role: json['role'] == null
        ? null
        : UserRole.fromJson(json['role'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$UserShortAuthToJson(UserShortAuth instance) =>
    <String, dynamic>{
      'id': instance.id,
      'firstName': instance.firstName,
      'lastName': instance.lastName,
      'role': instance.role,
    };
