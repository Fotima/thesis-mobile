// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'role.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

RoleShort _$RoleShortFromJson(Map<String, dynamic> json) {
  return RoleShort(
    id: json['id'] as int,
    name: json['name'] as String,
    rank: json['rank'] as int?,
  );
}

Map<String, dynamic> _$RoleShortToJson(RoleShort instance) => <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'rank': instance.rank,
    };

UserRole _$UserRoleFromJson(Map<String, dynamic> json) {
  return UserRole(
    id: json['id'] as int,
    name: json['name'] as String,
    rank: json['rank'] as int?,
    permissions: (json['permissions'] as List<dynamic>?)
        ?.map((e) => e as String)
        .toList(),
  );
}

Map<String, dynamic> _$UserRoleToJson(UserRole instance) => <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'rank': instance.rank,
      'permissions': instance.permissions,
    };
