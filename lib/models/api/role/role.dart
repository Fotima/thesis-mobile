import 'package:json_annotation/json_annotation.dart';
part 'role.g.dart';

@JsonSerializable()
class RoleShort {
  int id;
  String name;
  int? rank;

  RoleShort({
    required this.id,
    required this.name,
    this.rank,
  });

  factory RoleShort.fromJson(Map<String, dynamic> json) =>
      _$RoleShortFromJson(json);

  Map<String, dynamic> toJson() => _$RoleShortToJson(this);
}

@JsonSerializable()
class UserRole {
  int id;
  String name;
  int? rank;
  List<String>? permissions;

  UserRole({required this.id, required this.name, this.rank, this.permissions});

  factory UserRole.fromJson(Map<String, dynamic> json) =>
      _$UserRoleFromJson(json);

  Map<String, dynamic> toJson() => _$UserRoleToJson(this);
}
