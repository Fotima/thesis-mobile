import 'package:json_annotation/json_annotation.dart';
part 'base.g.dart';

@JsonSerializable()
class BaseResponse {
  bool result;
  BaseResponse({this.result = false});
  factory BaseResponse.fromJson(Map<String, dynamic> json) =>
      _$BaseResponseFromJson(json);

  Map<String, dynamic> toJson() => _$BaseResponseToJson(this);
}
