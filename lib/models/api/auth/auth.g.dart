// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'auth.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

LoginPost _$LoginPostFromJson(Map<String, dynamic> json) {
  return LoginPost(
    password: json['password'] as String,
    username: json['username'] as String,
  );
}

Map<String, dynamic> _$LoginPostToJson(LoginPost instance) => <String, dynamic>{
      'username': instance.username,
      'password': instance.password,
    };

LoginResponse _$LoginResponseFromJson(Map<String, dynamic> json) {
  return LoginResponse(
    accessToken: json['access_token'] as String,
    expireDate: json['expire_date'] as int,
    accountConfirmed: json['accountConfirmed'] as bool,
    user: UserShortAuth.fromJson(json['user'] as Map<String, dynamic>),
    hasLowerRoles: json['hasLowerRoles'] as bool,
  );
}

Map<String, dynamic> _$LoginResponseToJson(LoginResponse instance) =>
    <String, dynamic>{
      'access_token': instance.accessToken,
      'expire_date': instance.expireDate,
      'accountConfirmed': instance.accountConfirmed,
      'user': instance.user,
      'hasLowerRoles': instance.hasLowerRoles,
    };
