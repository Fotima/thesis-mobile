import 'package:hospitalproject/models/api/user_short/user_short.dart';
import 'package:json_annotation/json_annotation.dart';
part 'auth.g.dart';

@JsonSerializable()
class LoginPost {
  String username;
  String password;

  LoginPost({
    required this.password,
    required this.username,
  });

  factory LoginPost.fromJson(Map<String, dynamic> json) =>
      _$LoginPostFromJson(json);

  Map<String, dynamic> toJson() => _$LoginPostToJson(this);
}

@JsonSerializable()
class LoginResponse {
  @JsonKey(name: 'access_token')
  String accessToken;
  @JsonKey(name: 'expire_date')
  int expireDate;

  bool accountConfirmed;

  UserShortAuth user;

  bool hasLowerRoles;

  LoginResponse({
    required this.accessToken,
    this.expireDate = 0,
    this.accountConfirmed = false,
    required this.user,
    this.hasLowerRoles = false,
  });

  factory LoginResponse.fromJson(Map<String, dynamic> json) =>
      _$LoginResponseFromJson(json);

  Map<String, dynamic> toJson() => _$LoginResponseToJson(this);
}
