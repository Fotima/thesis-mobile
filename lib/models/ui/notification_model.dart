class NotificationModelIOS {
  int? id;
  String headline;
  String? type;
  AlertMessage? alert;
  NotificationModelIOS({this.id, this.headline = "", this.alert, this.type});
  factory NotificationModelIOS.fromJson(Map<String, dynamic> message) {
    try {
      Map<String, dynamic> notification = Map.from(message['aps']);
      // Map<String, dynamic> data = Map.from(notification['data']);

      return NotificationModelIOS(
          headline: message['text'],
          id: int.parse(message['id']),
          type: message['type'],
          alert: AlertMessage.fromJson(Map.from(notification['alert'])));
    } catch (e) {
      print(e);
      return NotificationModelIOS();
    }
  }
}

class AlertMessage {
  NotificationContent? content;
  AlertMessage({this.content});
  factory AlertMessage.fromJson(Map<String, dynamic> json) {
    return AlertMessage(content: NotificationContent.fromJson(Map.from(json)));
  }
}

class NotificationContent {
  String body;
  String title;
  NotificationContent({this.body = "", this.title = ""});
  factory NotificationContent.fromJson(Map<String, dynamic> json) {
    return NotificationContent(title: json['title'], body: json['body']);
  }
}

class NotificationModelAndroid {
  int? id;
  String headline;
  String body;
  String title;
  String? type;
  NotificationModelAndroid(
      {this.id,
      this.body = "",
      this.title = "",
      this.headline = "",
      this.type});
  factory NotificationModelAndroid.fromJson(Map<String, dynamic> message) {
    try {
      Map<String, dynamic> notifcation = Map.from(message['notification']);
      Map<String, dynamic> json = Map.from(message['data']);

      return NotificationModelAndroid(
          headline: json['text'],
          id: int.parse(json['id']),
          body: notifcation['body'] ?? '',
          type: json['type'],
          title: notifcation['title'] ?? '');
    } catch (e) {
      print(e);
      return NotificationModelAndroid();
    }
  }
}
