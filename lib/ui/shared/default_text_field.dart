import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:hospitalproject/constants/values/color_constants.dart';
// Custom textfield used on the project

class DefaultTextField extends StatelessWidget {
  final bool isRequired;
  final TextEditingController? controller;
  final String? labelText;
  final String? hintText;
  final String? errorText;
  final bool showErrorText;
  final bool enabled;
  final bool showCursor;
  final FocusNode? focusNode;
  final TextInputType keyboardType;
  final bool showRequiredSign;
  final bool password;
  final Function? onFieldSubmitted;
  final TextInputAction textInputAction;
  final Function? onChanged;
  final Widget? leadingIcon;
  final Widget? trailingWidget;
  final Widget? suffixIcon;
  final String? leadingText;
  final List<TextInputFormatter>? inputFormatters;
  final FormFieldValidator<String>? customValidator;
  final VoidCallback? onTap;
  final Function? onSaved;
  final Color cursorColor;
  final int? maxLines;
  final String? initialValue;
  final String descriptionText;
  final bool isPhoneTextField;
  final int? maxLength;
  final TextCapitalization? textCapitalization;
  final bool underlineInputBorder;
  final EdgeInsetsGeometry? contentPadding;
  final bool autofocus;
  final bool readOnly;
  final Color? fillColor;

  DefaultTextField({
    this.isRequired = false,
    required this.controller,
    this.labelText,
    this.leadingIcon,
    this.password = false,
    this.hintText,
    this.errorText,
    this.showErrorText = true,
    this.enabled = true,
    this.showCursor = true,
    this.inputFormatters,
    this.leadingText,
    this.onChanged,
    this.onFieldSubmitted,
    this.onTap,
    this.trailingWidget,
    this.suffixIcon,
    this.onSaved,
    this.showRequiredSign = false,
    this.textInputAction = TextInputAction.done,
    this.keyboardType = TextInputType.text,
    this.customValidator,
    this.cursorColor = AppColors.accent,
    this.maxLines,
    this.initialValue,
    this.focusNode,
    this.descriptionText = '',
    this.isPhoneTextField = false,
    this.maxLength,
    this.textCapitalization,
    this.underlineInputBorder = false,
    this.contentPadding,
    this.autofocus = false,
    this.readOnly = false,
    this.fillColor,
  });

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      initialValue: initialValue,
      readOnly: readOnly,
      focusNode: focusNode,
      onChanged: (String value) {
        if (onChanged != null) {
          onChanged!(value);
        }
      },
      validator: (text) {
        if (isRequired && text != null && text.isEmpty) {
          return 'Required field';
        }
        if (customValidator != null) {
          return customValidator!(text);
        }
        return null;
      },
      maxLength: maxLength,
      textCapitalization: textCapitalization ?? TextCapitalization.none,
      autovalidateMode: AutovalidateMode.disabled,
      textInputAction: textInputAction,
      controller: controller,
      autofocus: autofocus,
      keyboardType: keyboardType,
      inputFormatters: inputFormatters ?? [],
      obscureText: password,
      cursorColor: cursorColor,
      enabled: enabled,
      showCursor: showCursor,
      onTap: onTap,
      maxLines: maxLines,
      style: TextStyle(
        color: AppColors.black,
        fontSize: 16,
      ),
      decoration: InputDecoration(
        prefixIcon: leadingIcon,
        prefixIconConstraints: BoxConstraints(
          minHeight: 20,
        ),
        contentPadding: contentPadding ??
            EdgeInsets.symmetric(
              vertical: 14,
              horizontal: 16,
            ),
        suffix: trailingWidget,
        suffixIcon: suffixIcon,
        prefixText: leadingText,

        hintText: hintText,
        hintStyle: TextStyle(
          color: AppColors.fontColor,
          fontSize: 16,
        ),
        filled: true,
        fillColor: fillColor ?? AppColors.background.withOpacity(.4),
        // fillColor: AppColors.textFieldBakcGrounAlt,
        floatingLabelBehavior: FloatingLabelBehavior.always,
        border: OutlineInputBorder(
          borderSide: BorderSide(color: AppColors.textFieldBackground),
          borderRadius: BorderRadius.circular(8),
        ),

        enabledBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(8),
          borderSide: BorderSide(color: AppColors.textFieldBackground),
        ),
        focusedBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(8),
          borderSide: BorderSide(color: AppColors.textFieldBackground),
        ),
        disabledBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(8),
          borderSide: BorderSide(color: AppColors.textFieldBackground),
        ),
        // errorText: showErrorText ? errorText : null,
        // errorBorder: underlineInputBorder
        //     ? UnderlineInputBorder(
        //         borderSide: BorderSide(
        //           color: AppColors.red,
        //           width: 1.0,
        //         ),
        //       )
        //     : OutlineInputBorder(
        //         borderSide: BorderSide(color: AppColors.red, width: 1.0),
        //         borderRadius: getBorderRadius(radius: 12),
        //       ),
      ),
    );
  }
}
