import 'package:flutter/material.dart';
import 'package:hospitalproject/constants/values/color_constants.dart';
import 'package:hospitalproject/ui/shared/sliver_appbar_delegate.dart';
// Custom sliver widget used on the project

class MySliverStickyHeader extends StatelessWidget {
  final Widget? child;
  final double minHeight;
  final double maxHeight;
  final bool addHorizontalPadding;
  final bool pinned;

  MySliverStickyHeader({
    this.child,
    this.maxHeight = 60.0,
    this.minHeight = 60.0,
    this.addHorizontalPadding = true,
    this.pinned = true,
  });

  @override
  Widget build(BuildContext context) {
    return SliverPersistentHeader(
      pinned: pinned,
      delegate: SliverAppBarDelegate(
        minHeight: minHeight,
        maxHeight: maxHeight,
        child: Container(
          padding: addHorizontalPadding
              ? EdgeInsets.only(
                  left: 16.0,
                  right: 16.0,
                )
              : null,
          color: AppColors.background,
          child: child ?? Container(),
        ),
      ),
    );
  }
}
