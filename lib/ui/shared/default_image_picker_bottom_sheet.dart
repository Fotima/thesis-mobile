import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'bottom_sheet/default_bottom_sheet.dart';
// Custom image picker bottom sheet  used on the project

class DefaultImagePickerBottomSheet extends StatelessWidget {
  final VoidCallback? onGallerySelect;
  final VoidCallback? onCameraSelect;

  DefaultImagePickerBottomSheet({
    required this.onGallerySelect,
    required this.onCameraSelect,
  });
  static showBottomSheet(
    BuildContext context, {
    required VoidCallback? onGallerySelect,
    required VoidCallback? onCameraSelect,
  }) {
    return showModalBottomSheet(
      context: context,
      builder: (context) {
        return DefaultImagePickerBottomSheet(
          onGallerySelect: onGallerySelect,
          onCameraSelect: onCameraSelect,
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    // if (Platform.isIOS) {
    //   return Theme(
    //     data: ThemeData.light(),
    //     child: DefaultCupertinoBottomSheet(
    //       actions: [
    //         CupertinoActionSheetAction(
    //           child: Text20(
    //             'Gallery',
    //           ),
    //           isDefaultAction: true,
    //           onPressed: onGallerySelect,
    //         ),
    //         CupertinoActionSheetAction(
    //           child: Text20(
    //             'Camera',
    //           ),
    //           isDefaultAction: true,
    //           onPressed: onCameraSelect,
    //         )
    //       ],
    //     ),
    //   );
    // }

    return DefaultBottomSheet(
      addBottomPadding: true,
      children: [
        DefaultMaterialActionSheetAction(
          onPressed: onGallerySelect,
          text: 'Gallery',
        ),
        DefaultMaterialActionSheetAction(
          onPressed: onCameraSelect,
          text: 'Camera',
        ),
      ],
    );
  }
}
