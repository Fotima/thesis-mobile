import 'dart:io';

import 'package:flutter/material.dart';
import 'package:hospitalproject/constants/values/color_constants.dart';

import 'app_text.dart';
// Custom appbar widget used on the project

class DefaultAppBar extends StatelessWidget implements PreferredSizeWidget {
  final String title;
  final List<Widget>? actions;
  final PreferredSizeWidget? bottom;
  final bool centerTitle;
  final double? fontSize;
  final FontWeight? fontWeight;
  final bool automaticallyImplyLeading;
  final Function? onBackPressed;

  DefaultAppBar({
    this.actions,
    required this.title,
    this.bottom,
    this.centerTitle = true,
    this.fontSize,
    this.fontWeight,
    this.automaticallyImplyLeading = true,
    this.onBackPressed,
  });

  @override
  Widget build(BuildContext context) {
    return AppBar(
      elevation: 0,
      centerTitle: centerTitle,
      leading: onBackPressed == null
          ? null
          : IconButton(
              icon: Platform.isIOS
                  ? Icon(Icons.arrow_back_ios)
                  : Icon(Icons.arrow_back),
              onPressed: () {
                onBackPressed!();
              },
            ),
      title: AppText(
        title,
        fontSize: fontSize ?? 20,
        color: AppColors.white,
        fontWeight: fontWeight ?? FontWeight.w700,
      ),
      actions: actions,
      bottom: bottom,
      automaticallyImplyLeading: automaticallyImplyLeading,
    );
  }

  @override
  Size get preferredSize => AppBar().preferredSize;
}
