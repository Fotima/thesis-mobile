import 'package:flutter/material.dart';
import 'package:hospitalproject/constants/values/color_constants.dart';
import 'package:hospitalproject/ui/shared/text16.dart';
// Custom check box widget used on the project

class CustomCheckBox extends StatelessWidget {
  final String text;
  final bool value;
  final VoidCallback? onChanged;
  final EdgeInsets? padding;

  CustomCheckBox({
    this.text = '',
    required this.value,
    this.onChanged,
    this.padding,
  });

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onChanged,
      child: Container(
        padding: padding,
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            AnimatedContainer(
              duration: Duration(milliseconds: 50),
              curve: Curves.easeIn,
              height: 20,
              width: 20,
              alignment: Alignment.center,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(4),
                border: Border.all(
                    color: value ? Colors.transparent : AppColors.fontColor),
                color: value ? AppColors.accent : AppColors.black,
              ),
              child: AnimatedSwitcher(
                duration: Duration(milliseconds: 2),
                child: value
                    ? Icon(
                        Icons.done,
                        size: 14,
                        color: AppColors.white,
                      )
                    : Container(),
              ),
            ),
            SizedBox(width: 12),
            Expanded(child: Text16(text))
          ],
        ),
      ),
    );
  }
}
