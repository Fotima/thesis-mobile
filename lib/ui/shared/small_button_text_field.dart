import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:hospitalproject/constants/values/color_constants.dart';
// Custom  widget used on the project

class SmallButtonOrTextField extends StatelessWidget {
  final String name;
  final Color color;
  final Color backgroundColor;
  final String iconUrl;
  final bool buttonOrTextField; //true = button

  SmallButtonOrTextField(
      {this.name = '',
      this.color = AppColors.white,
      this.backgroundColor = AppColors.white,
      this.iconUrl = '',
      this.buttonOrTextField = false});

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      child: buttonOrTextField
          ? ElevatedButton(
              onPressed: () {},
              child: iconUrl.isNotEmpty
                  ? Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        SvgPicture.asset(iconUrl),
                        SizedBox(width: 8),
                        Text(name),
                      ],
                    )
                  : Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(name),
                      ],
                    ),
              style: ElevatedButton.styleFrom(
                onPrimary: this.color,
                elevation: 0,
                primary: backgroundColor,
                shape: new RoundedRectangleBorder(
                  borderRadius: new BorderRadius.circular(8.0),
                ),
              ),
            )
          : TextFormField(
// ignore: deprecated_member_use
              style: TextStyle(color: color),
              decoration: InputDecoration(
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(10.0),
                  borderSide: BorderSide.none,
                ),
                hintStyle: TextStyle(color: color, fontSize: 16),
                hintText: name,
                filled: true,
                fillColor: backgroundColor,
                contentPadding: EdgeInsets.all(10),
              ),
            ),
      height: 48,
      width: 160,
    );
  }
}
