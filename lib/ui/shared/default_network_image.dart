import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:hospitalproject/constants/values/color_constants.dart';
import 'package:shimmer/shimmer.dart';
// Custom cached network image widget used on the project

class DefaultNetworkImage extends StatelessWidget {
  final String imageUrl;
  final double? width, height;
  final Color placeholderColor;
  final BoxFit fit;
  final double borderRadius;
  final bool hasTopBorderRadius;

  DefaultNetworkImage(
    this.imageUrl, {
    this.borderRadius = 0,
    this.height,
    this.width,
    this.hasTopBorderRadius = false,
    this.fit = BoxFit.cover,
    this.placeholderColor = AppColors.greyDisabled,
  });

  @override
  Widget build(BuildContext context) {
    return CachedNetworkImage(
      imageUrl: imageUrl,
      width: width,
      height: height,
      imageBuilder: (context, imageProvider) => Container(
        decoration: BoxDecoration(
          borderRadius: hasTopBorderRadius
              ? BorderRadius.only(
                  topLeft: Radius.circular(borderRadius),
                  topRight: Radius.circular(borderRadius),
                )
              : BorderRadius.circular(borderRadius),
          image: DecorationImage(image: imageProvider, fit: fit),
        ),
      ),
      placeholder: getPlaceholder,
      errorWidget: (context, url, error) => Container(
        padding: EdgeInsets.all(15.0),
      ),
    );
  }

  Widget getPlaceholder(context, url) {
    return Shimmer.fromColors(
      baseColor: Colors.grey[500]!,
      highlightColor: Colors.grey[100]!,
      direction: ShimmerDirection.rtl,
      child: Container(
        width: width,
        height: height,
        decoration: BoxDecoration(
          color: placeholderColor,
          borderRadius: BorderRadius.circular(borderRadius),
        ),
      ),
    );
  }
}
