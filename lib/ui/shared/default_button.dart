import 'package:flutter/material.dart';
import 'package:hospitalproject/constants/values/color_constants.dart';

import 'app_text.dart';
// Custom button widget used on the project

class DefaultButton extends StatelessWidget {
  final VoidCallback? onPressed;
  final String? text;
  final Widget? child;
  final bool isFilled;
  final Color? backgroundColor;
  final double? height;
  final bool enabled;
  final bool addBorder;
  final EdgeInsets? padding;

  DefaultButton({
    this.child,
    this.isFilled = true,
    required this.onPressed,
    this.backgroundColor,
    this.text,
    this.height,
    this.enabled = true,
    this.addBorder = false,
    this.padding,
  });

  @override
  Widget build(BuildContext context) {
    return Opacity(
      opacity: enabled ? 1 : .5,
      child: MaterialButton(
        onPressed: () {
          if (enabled && onPressed != null) {
            onPressed!();
          }
        },
        focusElevation: 0,
        hoverElevation: 0,
        highlightElevation: 0,
        elevation: 0,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(8),
          side: BorderSide(
            color: addBorder ? AppColors.white : Colors.transparent,
            width: addBorder ? 1 : 0,
          ),
        ),
        padding: padding ?? EdgeInsets.all(0),
        color: backgroundColor ??
            (isFilled ? AppColors.accent : Colors.transparent),
        child: AnimatedContainer(
          duration: Duration(milliseconds: 500),
          height: height ?? 52,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(8),
            border: Border.all(
                color: isFilled ? AppColors.accent : AppColors.fontColor),
            boxShadow: backgroundColor == null && isFilled
                ? [
                    BoxShadow(
                      color: Colors.black.withOpacity(.1),
                      offset: Offset(0, 4),
                      blurRadius: 4,
                    ),
                  ]
                : [],
          ),
          child: child ??
              Center(
                child: AppText(
                  text ?? '',
                  color: isFilled ? AppColors.white : AppColors.black,
                  fontSize: 16,
                  fontWeight: FontWeight.w500,
                ),
              ),
        ),
      ),
    );
  }
}
