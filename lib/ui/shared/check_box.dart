import 'package:flutter/material.dart';
import 'package:hospitalproject/constants/values/color_constants.dart';
// Custom check box widget used on the project

class CustomCheckBox extends StatefulWidget {
  final bool isActive;

  const CustomCheckBox({this.isActive = false});
  @override
  _CustomCheckBoxState createState() => _CustomCheckBoxState();
}

class _CustomCheckBoxState extends State<CustomCheckBox> {
  @override
  Widget build(BuildContext context) {
    return Container(
        child: Padding(
      padding: const EdgeInsets.all(8.0),
      child: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(6),
          border: widget.isActive
              ? Border.all(color: Colors.transparent)
              : Border.all(color: AppColors.textFieldHintColor, width: 1.0),
          color: widget.isActive ? AppColors.accent : Colors.transparent,
        ),
        width: 20,
        height: 20,
        child: widget.isActive
            ? Center(
                child: Icon(
                Icons.done,
                color: AppColors.white,
                size: 16.0,
              ))
            : Container(),
      ),
    ));
  }
}
