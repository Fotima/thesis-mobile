import 'package:flutter/material.dart';
import 'package:hospitalproject/constants/values/color_constants.dart';
import 'package:hospitalproject/ui/shared/text16.dart';
// Custom radio button widget used on the project

class CustomRadioButton extends StatelessWidget {
  final int value;
  final String text;
  final int groupValue;
  final VoidCallback onChanged;
  final EdgeInsets? padding;

  CustomRadioButton({
    required this.value,
    this.text = '',
    required this.groupValue,
    required this.onChanged,
    this.padding,
  });

  @override
  Widget build(BuildContext context) {
    final bool isChecked = groupValue == value;
    return GestureDetector(
      onTap: onChanged,
      child: Container(
        padding: padding,
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            AnimatedContainer(
              duration: Duration(milliseconds: 100),
              curve: Curves.easeIn,
              height: 20,
              width: 20,
              alignment: Alignment.center,
              decoration: BoxDecoration(
                shape: BoxShape.circle,
                border: Border.all(
                    color:
                        isChecked ? Colors.transparent : AppColors.fontColor),
                color: isChecked ? AppColors.accent : AppColors.black,
              ),
              child: isChecked
                  ? Container(
                      height: 10,
                      width: 10,
                      decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        color: AppColors.white,
                      ),
                    )
                  : Container(),
            ),
            SizedBox(width: 12),
            Expanded(child: Text16(text))
          ],
        ),
      ),
    );
  }
}
