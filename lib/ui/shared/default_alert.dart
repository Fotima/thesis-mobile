import 'package:flutter/material.dart';
import 'package:hospitalproject/constants/values/color_constants.dart';
import 'package:hospitalproject/ui/shared/text16.dart';

import 'default_button.dart';
// Custom alert box used on the project

class DefaulAlert extends StatelessWidget {
  final String? text;
  final String? buttonText;
  final VoidCallback? onTap;
  final bool showCloseButton;
  final Widget? body;

  DefaulAlert({
    this.text,
    this.buttonText,
    this.onTap,
    this.body,
    this.showCloseButton = false,
  });

  static Future<dynamic> show(
    BuildContext context, {
    String? text,
    String? buttonText,
    VoidCallback? onTap,
    bool showCloseButton = false,
    Widget? body,
  }) async {
    return showDialog(
      context: context,
      builder: (context) {
        return DefaulAlert(
          text: text,
          buttonText: buttonText,
          onTap: onTap,
          body: body,
          showCloseButton: showCloseButton,
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return DefaultDialogContainer(
      child: Column(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          body ?? Text16(text ?? '', color: AppColors.white),
          showCloseButton ? SizedBox(height: 42) : Container(),
          showCloseButton
              ? DefaultButton(
                  text: buttonText ?? 'OK',
                  isFilled: false,
                  onPressed: () {
                    Navigator.pop(context, true);
                    if (onTap != null) {
                      onTap!();
                    }
                  },
                )
              : Container(),
        ],
      ),
    );
  }
}

class DefaultDialogContainer extends StatelessWidget {
  final Widget? child;
  final double? height;
  final double? horizontalPadding;

  DefaultDialogContainer({
    this.child,
    this.height,
    this.horizontalPadding,
  });

  @override
  Widget build(BuildContext context) {
    return Dialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(8),
      ),
      child: Container(
        padding: EdgeInsets.all(16),
        decoration: BoxDecoration(
          color: AppColors.black,
          borderRadius: BorderRadius.circular(8),
        ),
        child: child,
      ),
    );
  }
}
