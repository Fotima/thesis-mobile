import 'package:flutter/cupertino.dart';

import '../app_text.dart';
import '../text20.dart';

// UI for custom cupertino bottom sheet for an application
class DefaultCupertinoBottomSheet extends StatelessWidget {
  final String? title;
  final String? message;
  final List<CupertinoActionSheetAction>? actions;
  final bool withCancelButton;

  DefaultCupertinoBottomSheet({
    this.title,
    this.message,
    required this.actions,
    this.withCancelButton = true,
  });

  @override
  Widget build(BuildContext context) {
    return CupertinoActionSheet(
      title: title != null && title!.isNotEmpty ? AppText(title ?? '') : null,
      message: message != null && message!.isNotEmpty
          ? AppText(message ?? '')
          : null,
      actions: actions,
      cancelButton: withCancelButton
          ? CupertinoActionSheetAction(
              child: Text20(
                'Cancel',
              ),
              onPressed: () {
                Navigator.pop(context);
              },
            )
          : null,
    );
  }
}
