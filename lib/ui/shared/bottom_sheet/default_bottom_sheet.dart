import 'package:flutter/material.dart';
import 'package:hospitalproject/constants/values/color_constants.dart';

import '../text16.dart';
import '../text18.dart';

// UI for custom bottom sheet for an application
class DefaultBottomSheet extends StatelessWidget {
  final bool addBottomPadding;
  final List<DefaultMaterialActionSheetAction> children;
  final String? title;
  final Widget? actionButton;

  const DefaultBottomSheet({
    this.children = const [],
    this.addBottomPadding = true,
    this.title,
    this.actionButton,
  });

  @override
  Widget build(BuildContext context) {
    final double bottom = MediaQuery.of(context).padding.bottom;
    final double bottom2 = MediaQuery.of(context).viewInsets.bottom;

    return Theme(
      data: Theme.of(context).copyWith(canvasColor: Colors.transparent),
      child: Container(
        height: children.length * 56 < MediaQuery.of(context).size.height - 90
            ? null
            : MediaQuery.of(context).size.height - 90,
        padding: EdgeInsets.only(
          top: 8,
          bottom: addBottomPadding ? bottom + 16 + bottom2 : 0,
        ),
        decoration: BoxDecoration(
          color: AppColors.white,
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(16),
            topRight: Radius.circular(16),
          ),
        ),
        child: SingleChildScrollView(
          child: Wrap(
            children: <Widget>[
              BottomSheetTopIcon(),
              title == null
                  ? Container()
                  : BottomSheetTitle(title: title ?? ''),
              SizedBox(height: 14),
              Column(children: children),
              actionButton == null
                  ? Container()
                  : Padding(
                      padding: EdgeInsets.only(top: 16),
                      child: actionButton,
                    ),
              // SizedBox(height: bottom + 16 + bottom2)
            ],
          ),
        ),
      ),
    );
  }
}

class BottomSheetTitle extends StatelessWidget {
  final String title;

  BottomSheetTitle({this.title = ''});
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 24),
      padding: EdgeInsets.symmetric(vertical: 12),
      alignment: Alignment.bottomCenter,
      decoration: BoxDecoration(
        border: Border(
          bottom: BorderSide(color: AppColors.fontColor, width: .5),
        ),
      ),
      child: Text18(
        title,
        textAlign: TextAlign.center,
        fontWeight: FontWeight.w600,
      ),
    );
  }
}

class BottomSheetTopIcon extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: Alignment.center,
      child: Container(
        height: 5,
        width: 40.0,
        decoration: BoxDecoration(
          color: Color(0xFFBEC1C4),
          borderRadius: BorderRadius.circular(10),
        ),
      ),
    );
  }
}

class DefaultMaterialActionSheetAction extends StatelessWidget {
  final double radius;
  final Function? onPressed;
  final Color color;
  final double elevation;
  final double hightLightElevation;
  final double height;
  final double padding;
  final String? text;
  final dynamic value;
  final dynamic selectedValue;
  final Widget? leadingIcon;
  final bool returnString;
  final Color? fontColor;
  final Widget? trailingIcon;
  DefaultMaterialActionSheetAction({
    this.radius = 0,
    this.onPressed,
    this.color = AppColors.white,
    this.elevation = 0,
    this.height = 56,
    this.padding = 0,
    required this.text,
    this.hightLightElevation = 0,
    this.leadingIcon,
    this.returnString = false,
    this.selectedValue,
    this.value,
    this.fontColor,
    this.trailingIcon,
  });

  @override
  Widget build(BuildContext context) {
    return Material(
      elevation: elevation,
      borderRadius: BorderRadius.circular(radius),
      child: MaterialButton(
        elevation: elevation,
        highlightElevation: hightLightElevation,
        padding: EdgeInsets.all(padding),
        height: height,
        color: color,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(radius),
        ),
        onPressed: () {
          Navigator.pop(context, value);
          if (onPressed != null) {
            if (returnString) {
              onPressed!(text);
            } else {
              onPressed!();
            }
          }
        },
        child: Container(
          height: height,
          width: MediaQuery.of(context).size.width,
          alignment: Alignment.centerLeft,
          padding: EdgeInsets.symmetric(horizontal: 24),
          child: Row(
            children: [
              leadingIcon ?? Container(),
              leadingIcon == null ? Container() : SizedBox(width: 16),
              Expanded(
                child: Text16(
                  text ?? '',
                  color: fontColor != null
                      ? fontColor!
                      : selectedValue == value
                          ? AppColors.black
                          : AppColors.fontColor,
                  textAlign: TextAlign.left,
                ),
              ),
              SizedBox(width: 8),
              trailingIcon == null ? Container() : trailingIcon!,
              selectedValue == value
                  ? Icon(
                      Icons.check,
                      color: AppColors.green,
                    )
                  : Container()
            ],
          ),
        ),
      ),
    );
  }
}
