import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';
import 'package:hospitalproject/constants/values/color_constants.dart';
// Custom floating message box used on the project

void showMessage(String text,
    {String? title,
    bool isError = false,
    Duration? duration,
    bool alert = false}) {
  BotToast.showSimpleNotification(
    title: title != null
        ? title
        : alert
            ? "Alert"
            : isError
                ? 'Error'
                : 'Success',
    subTitle: text,
    duration: duration ?? Duration(seconds: 3),
    backgroundColor: alert
        ? AppColors.orange
        : isError
            ? AppColors.red
            : AppColors.lightGreen,
    closeIcon: Icon(
      Icons.close,
      color: AppColors.white,
    ),
  );
}
