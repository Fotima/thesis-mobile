import 'package:flutter/material.dart';
import 'package:hospitalproject/constants/values/color_constants.dart';
// Custom text widget used on the project

class AppText extends StatelessWidget {
  final String text;
  final TextStyle? style;
  final TextAlign? textAlign;
  final TextOverflow? overflow;
  final double? fontSize;
  final String? fontFamily;
  final FontWeight fontWeight;
  final double? lineHeight;
  final Color? color;

  AppText(
    this.text, {
    this.style,
    this.textAlign,
    this.overflow,
    this.fontFamily,
    this.fontSize,
    this.fontWeight = FontWeight.normal,
    this.lineHeight,
    this.color = AppColors.black,
  });

  @override
  Widget build(BuildContext context) {
    return Text(
      text,
      textAlign: textAlign,
      overflow: overflow,
      style: TextStyle(
        fontSize: fontSize ?? 14,
        fontFamily: fontFamily,
        fontWeight: fontWeight,
        height: lineHeight,
        color: color,
      ),
    );
  }
}
