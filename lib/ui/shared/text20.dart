import 'package:flutter/material.dart';
import 'package:hospitalproject/constants/values/color_constants.dart';
// Custom text widget of 20 font size used on the project

class Text20 extends StatelessWidget {
  final String text;
  final TextStyle? style;
  final TextAlign? textAlign;
  final TextOverflow? overflow;
  final String? fontFamily;
  final FontWeight fontWeight;
  final double? lineHeight;
  final Color color;

  Text20(
    this.text, {
    this.style,
    this.textAlign,
    this.overflow,
    this.fontFamily,
    this.fontWeight = FontWeight.normal,
    this.lineHeight,
    this.color = AppColors.black,
  });

  @override
  Widget build(BuildContext context) {
    return Text(
      text,
      textAlign: textAlign,
      overflow: overflow,
      style: TextStyle(
        fontSize: 20,
        fontFamily: fontFamily,
        fontWeight: fontWeight,
        height: lineHeight,
        color: color,
      ),
    );
  }
}
