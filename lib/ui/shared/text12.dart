import 'package:flutter/material.dart';
import 'package:hospitalproject/constants/values/color_constants.dart';
// Custom text widget of 12 font size used on the project

class Text12 extends StatelessWidget {
  final String text;
  final TextStyle? style;
  final TextAlign? textAlign;
  final TextOverflow? overflow;
  final String? fontFamily;
  final FontWeight fontWeight;
  final double? lineHeight;
  final Color color;
  final int? maxLines;

  Text12(
    this.text, {
    this.style,
    this.textAlign,
    this.overflow,
    this.fontFamily,
    this.fontWeight = FontWeight.normal,
    this.lineHeight,
    this.color = AppColors.black,
    this.maxLines,
  });

  @override
  Widget build(BuildContext context) {
    return Text(
      text,
      textAlign: textAlign,
      overflow: overflow,
      maxLines: maxLines,
      style: TextStyle(
        fontSize: 12,
        fontFamily: fontFamily,
        fontWeight: fontWeight,
        height: lineHeight,
        color: color,
      ),
    );
  }
}
