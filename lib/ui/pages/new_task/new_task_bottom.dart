import 'package:flutter/material.dart';
import 'package:hospitalproject/constants/values/color_constants.dart';
import 'package:hospitalproject/models/api/role/role.dart';
import 'package:hospitalproject/models/api/user_short/user_short.dart';
import 'package:hospitalproject/ui/shared/bottom_sheet/default_bottom_sheet.dart';
import 'package:hospitalproject/ui/shared/default_text_field.dart';
import 'package:hospitalproject/ui/shared/text16.dart';
import 'package:hospitalproject/ui/shared/text18.dart';
import 'package:rxdart/rxdart.dart';

// Custom widget for bottom modal sheet to show list of roles availabe to select for a new task
class LowerRoleBottom extends StatefulWidget {
  final bool addBottomPadding;
  final List<RoleShort> values;
  final String? title;
  final Widget? actionButton;

  const LowerRoleBottom({
    required this.values,
    this.addBottomPadding = true,
    this.title,
    this.actionButton,
  });

  @override
  _LowerRoleBottomState createState() => _LowerRoleBottomState();
}

class _LowerRoleBottomState extends State<LowerRoleBottom> {
  late TextEditingController searchController;
  List<RoleShort> initialValues = [];
  @override
  void initState() {
    searchController = TextEditingController();
    initialValues.addAll(widget.values);
    _getValuesSubject.add(initialValues);
    searchController.addListener(() {
      if (searchController.text.isNotEmpty) {
        List<RoleShort> filterResult = initialValues
            .where((element) => element.name
                .toLowerCase()
                .contains(searchController.text.toLowerCase()))
            .toList();
        _getValuesSubject.add(filterResult);
      } else {
        _getValuesSubject.add(initialValues);
      }
    });
    super.initState();
  }

  Stream<List<RoleShort>> get getValues => _getValuesSubject.stream;

  final _getValuesSubject = BehaviorSubject<List<RoleShort>>();

  @override
  void dispose() {
    _getValuesSubject.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final double bottom = MediaQuery.of(context).padding.bottom;
    final double bottom2 = MediaQuery.of(context).viewInsets.bottom;
    return Container(
      height:
          widget.values.length * 56 < MediaQuery.of(context).size.height - 90
              ? null
              : MediaQuery.of(context).size.height - 90,
      padding: EdgeInsets.only(
        top: 8,
        bottom: widget.addBottomPadding ? bottom + 16 + bottom2 : 0,
      ),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(24),
          topRight: Radius.circular(24),
        ),
      ),
      child: SingleChildScrollView(
        child: StreamBuilder<List<dynamic>>(
          stream: getValues,
          initialData: initialValues,
          builder: (BuildContext context, AsyncSnapshot snapshot) {
            return Wrap(
              children: <Widget>[
                BottomSheetTopIcon(),
                widget.title == null
                    ? Container()
                    : BottomSheetTitle(title: widget.title ?? ''),
                SizedBox(height: 14),
                TimetableSearch(searchController: searchController),
                SizedBox(height: 14),
                ListView.builder(
                    shrinkWrap: true,
                    primary: false,
                    itemCount: snapshot.data.length,
                    itemBuilder: (context, index) {
                      RoleShort item = snapshot.data[index];

                      return DropDownItems(
                        onPressed: (dynamic selected) {
                          Navigator.pop(context, selected);
                        },
                        item: item,
                        text: item.name,
                      );
                    }),

                widget.actionButton == null
                    ? Container()
                    : Padding(
                        padding: EdgeInsets.only(top: 16),
                        child: widget.actionButton,
                      ),
                // SizedBox(height: bottom + 16 + bottom2)
              ],
            );
          },
        ),
      ),
    );
  }
}

// Custom widget for search textfield for bottom sheet

class TimetableSearch extends StatelessWidget {
  final TextEditingController searchController;

  TimetableSearch({
    required this.searchController,
  });
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(bottom: 14, top: 12),
      padding: EdgeInsets.symmetric(horizontal: 16),
      child: DefaultTextField(
        contentPadding: EdgeInsets.symmetric(horizontal: 16),
        controller: searchController,
        hintText: "Search",
        suffixIcon: Icon(Icons.search, color: AppColors.fontColor),
      ),
    );
  }
}

// Custom widget for dropw down item container
class DropDownItems extends StatelessWidget {
  final double radius;
  final Function onPressed;
  final Color color;
  final double elevation;
  final double hightLightElevation;
  final double height;
  final double padding;
  final dynamic item;
  final Widget? leadingIcon;
  final String text;
  final bool selected;

  DropDownItems({
    this.radius = 0,
    required this.onPressed,
    this.color = Colors.white,
    this.elevation = 0,
    this.height = 56,
    this.padding = 0,
    required this.item,
    this.hightLightElevation = 0,
    this.leadingIcon,
    required this.text,
    this.selected = false,
  });

  @override
  Widget build(BuildContext context) {
    return Material(
      elevation: elevation,
      borderRadius: BorderRadius.circular(radius),
      child: MaterialButton(
        elevation: elevation,
        highlightElevation: hightLightElevation,
        padding: EdgeInsets.all(padding),
        height: height,
        color: color,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(radius),
        ),
        onPressed: () {
          onPressed(item);
        },
        child: Container(
          height: height,
          width: MediaQuery.of(context).size.width,
          alignment: Alignment.centerLeft,
          padding: EdgeInsets.symmetric(horizontal: 16),
          child: Row(
            children: [
              leadingIcon ?? Container(),
              leadingIcon == null ? Container() : SizedBox(width: 8),
              Expanded(
                child: Text16(
                  text,
                  textAlign: TextAlign.left,
                ),
              ),
              selected ? SizedBox(width: 8) : Container(),
              selected
                  ? Icon(
                      Icons.done,
                      color: AppColors.green,
                    )
                  : Container()
            ],
          ),
        ),
      ),
    );
  }
}

// Custom widget for bottom modal sheet to show list of users availabe to select for a new task

class LowerUserBottom extends StatefulWidget {
  final bool addBottomPadding;
  final List<UserShort> values;
  final String? title;
  final Widget? actionButton;
  final List<int>? selected;
  const LowerUserBottom({
    required this.values,
    this.addBottomPadding = true,
    this.title,
    this.actionButton,
    this.selected,
  });

  @override
  _LowerUserBottomState createState() => _LowerUserBottomState();
}

class _LowerUserBottomState extends State<LowerUserBottom> {
  late TextEditingController searchController;
  List<UserShort> initialValues = [];
  List<UserShort> selectedValues = [];

  @override
  void initState() {
    searchController = TextEditingController();
    initialValues.addAll(widget.values);
    _getValuesSubject.add(initialValues);
    searchController.addListener(() {
      if (searchController.text.isNotEmpty) {
        List<UserShort> filterResult = initialValues
            .where((element) =>
                element.firstName
                    .toLowerCase()
                    .contains(searchController.text.toLowerCase()) ||
                element.lastName
                    .toLowerCase()
                    .contains(searchController.text.toLowerCase()))
            .toList();
        _getValuesSubject.add(filterResult);
      } else {
        _getValuesSubject.add(initialValues);
      }
    });
    if (widget.selected != null) {
      selectedValues.addAll(widget.values
          .where((element) => widget.selected!.contains(element.id))
          .toList());
    }
    _selectedUsersSubject.add(selectedValues);

    super.initState();
  }

  Stream<List<UserShort>> get getValues => _getValuesSubject.stream;
  final _getValuesSubject = BehaviorSubject<List<UserShort>>();

  Stream<List<UserShort>> get selectedUsers => _selectedUsersSubject.stream;
  final _selectedUsersSubject = BehaviorSubject<List<UserShort>>();

  @override
  void dispose() {
    _getValuesSubject.close();
    _selectedUsersSubject.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final double bottom = MediaQuery.of(context).padding.bottom;
    final double bottom2 = MediaQuery.of(context).viewInsets.bottom;
    return Container(
      height:
          widget.values.length * 56 < MediaQuery.of(context).size.height - 90
              ? null
              : MediaQuery.of(context).size.height - 90,
      padding: EdgeInsets.only(
        top: 8,
        bottom: widget.addBottomPadding ? bottom + 16 + bottom2 : 0,
      ),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(24),
          topRight: Radius.circular(24),
        ),
      ),
      child: SingleChildScrollView(
        child: StreamBuilder<List<dynamic>>(
          stream: getValues,
          initialData: initialValues,
          builder: (BuildContext context, AsyncSnapshot snapshot) {
            return Wrap(
              children: <Widget>[
                BottomSheetTopIcon(),
                Container(
                  margin: EdgeInsets.symmetric(horizontal: 24),
                  padding: EdgeInsets.symmetric(vertical: 12),
                  alignment: Alignment.bottomCenter,
                  child: Row(
                    children: [
                      Expanded(child: Container()),
                      Expanded(
                        child: Text18(
                          widget.title ?? '',
                          textAlign: TextAlign.center,
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                      Expanded(
                        child: GestureDetector(
                          onTap: () {
                            Navigator.pop(context, selectedValues);
                          },
                          child: Text16(
                            'Confirm',
                            color: AppColors.accent,
                            textAlign: TextAlign.right,
                            fontWeight: FontWeight.w500,
                          ),
                        ),
                      )
                    ],
                  ),
                ),
                SizedBox(height: 14),
                TimetableSearch(searchController: searchController),
                SizedBox(height: 14),
                StreamBuilder<List<UserShort>>(
                  stream: selectedUsers,
                  initialData: [],
                  builder: (BuildContext context, AsyncSnapshot selectedSnap) {
                    return ListView.builder(
                      shrinkWrap: true,
                      primary: false,
                      itemCount: snapshot.data.length,
                      itemBuilder: (context, index) {
                        UserShort item = snapshot.data[index];
                        bool selected = selectedSnap.data.contains(item);

                        return DropDownItems(
                          onPressed: (dynamic selectedItem) {
                            if (selected) {
                              selectedValues.remove(selectedItem);
                              _selectedUsersSubject.add(selectedValues);
                            } else {
                              selectedValues.add(selectedItem);
                              _selectedUsersSubject.add(selectedValues);
                            }
                            // Navigator.pop(context, select
                            // ed);
                          },
                          item: item,
                          text: '${item.firstName} ${item.lastName}',
                          selected: selected,
                        );
                      },
                    );
                  },
                ),

                widget.actionButton == null
                    ? Container()
                    : Padding(
                        padding: EdgeInsets.only(top: 16),
                        child: widget.actionButton,
                      ),
                // SizedBox(height: bottom + 16 + bottom2)
              ],
            );
          },
        ),
      ),
    );
  }
}
