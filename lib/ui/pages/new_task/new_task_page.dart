import 'dart:io';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:flutter/material.dart';
import 'package:hospitalproject/blocs/new_task/new_task_bloc.dart';
import 'package:hospitalproject/constants/values/color_constants.dart';
import 'package:hospitalproject/models/api/user_short/user_short.dart';
import 'package:hospitalproject/ui/pages/new_task/new_task_bottom.dart';
import 'package:hospitalproject/ui/shared/bottom_sheet/default_bottom_sheet.dart';
import 'package:hospitalproject/ui/shared/default_button.dart';
import 'package:hospitalproject/ui/shared/default_text_field.dart';
import 'package:hospitalproject/ui/shared/text14.dart';
import 'package:hospitalproject/ui/shared/text18.dart';
import 'package:hospitalproject/ui/shared/text20.dart';
import 'package:hospitalproject/ui/shared/top_message.dart';
import 'package:hospitalproject/utils/screen_util.dart';
import 'package:intl/intl.dart';
// UI view for New task screen

class NewTaskPage extends StatefulWidget {
  @override
  _NewTaskPageState createState() => _NewTaskPageState();
}

class _NewTaskPageState extends State<NewTaskPage> {
  late NewTaskBloc bloc;
  TextEditingController nameController = TextEditingController();
  TextEditingController descriptionController = TextEditingController();
  TextEditingController locationController = TextEditingController();
  TextEditingController startController = TextEditingController();
  TextEditingController endController = TextEditingController();
  TextEditingController assignController = TextEditingController();
  TextEditingController priorityController = TextEditingController();
  GlobalKey<FormState> formKey = GlobalKey<FormState>();
  @override
  void initState() {
    bloc = NewTaskBloc();
    // When the screen is loaded, request BloC to load initial data from the server
    WidgetsBinding.instance!.addPostFrameCallback((timeStamp) {
      bloc.loadInitialData();
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => bloc,
      child: Scaffold(
        body: Container(
          height: screenHeight(context),
          child: Stack(
            children: [
              Body(
                nameController: nameController,
                descriptionController: descriptionController,
                locationController: locationController,
                priorityController: priorityController,
                startController: startController,
                endController: endController,
                assignController: assignController,
                formKey: formKey,
              ),
              MyAppBar(),
            ],
          ),
        ),
      ),
    );
  }
}

// Custom appbar widget for new task screen
class MyAppBar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(
          top: statusBarHeight(context) + 20, right: 24, left: 24),
      height: statusBarHeight(context) + 80,
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          GestureDetector(
            child: Icon(
              Platform.isIOS ? Icons.arrow_back_ios : Icons.arrow_back,
              color: AppColors.black,
              size: 24,
            ),
            onTap: () {
              Navigator.pop(context);
            },
          ),
          Expanded(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text20(
                  "Create new Task",
                  fontWeight: FontWeight.w600,
                  textAlign: TextAlign.center,
                ),
                SizedBox(width: 24)
              ],
            ),
          )
        ],
      ),
    );
  }
}

// Custom body widget for new task screen
class Body extends StatelessWidget {
  final TextEditingController nameController,
      descriptionController,
      locationController,
      startController,
      endController,
      assignController,
      priorityController;
  final GlobalKey<FormState> formKey;

  Body({
    required this.assignController,
    required this.descriptionController,
    required this.endController,
    required this.locationController,
    required this.nameController,
    required this.priorityController,
    required this.startController,
    required this.formKey,
  });

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<NewTaskBloc, NewTaskState>(
      builder: (context, state) {
        return Positioned(
          top: statusBarHeight(context) + 80,
          child: Container(
            padding: EdgeInsets.only(top: 24, left: 24, right: 24),
            height: screenHeight(context) - (statusBarHeight(context) + 80),
            width: MediaQuery.of(context).size.width,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(32),
              boxShadow: [
                BoxShadow(
                  color: AppColors.accentTransparent.withOpacity(.5),
                  blurRadius: 8,
                  spreadRadius: 4,
                  offset: Offset(0, 4),
                )
              ],
              color: AppColors.white,
            ),
            // if the sate is loading, show progrees indicator
            child: state.loading
                ? Center(
                    child: CircularProgressIndicator(),
                  )
                // else show the form
                : Form(
                    key: formKey,
                    child: SingleChildScrollView(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          DefaultTextField(
                            controller: nameController,
                            hintText: 'Title',
                            isRequired: true,
                          ),
                          SizedBox(height: 18),
                          DefaultTextField(
                            controller: descriptionController,
                            hintText: 'Description',
                            maxLines: 4,
                          ),
                          SizedBox(height: 18),
                          DefaultTextField(
                            controller: locationController,
                            hintText: 'Location',
                          ),
                          SizedBox(height: 18),
                          StartTimeContainer(startController: startController),
                          SizedBox(height: 18),
                          EndTimeContainer(
                            endController: endController,
                          ),
                          SizedBox(height: 24),
                          Text18(
                            "Assign to",
                            fontWeight: FontWeight.w500,
                          ),
                          SizedBox(height: 2),
                          Text14(
                            "You can assign task to a User or to a Role ",
                            color: AppColors.fontColor,
                          ),
                          SizedBox(height: 24),
                          AssignSwitch(
                            assignController: assignController,
                          ),
                          SizedBox(height: 18),
                          PrioritySelect(
                            priorityController: priorityController,
                            selectedPriority: state.taskCreate!.priority ?? -1,
                          ),
                          SizedBox(height: 24),
                          DefaultButton(
                            onPressed: () {
                              if (formKey.currentState!.validate()) {
                                if (state.taskCreate!.taskStart == null) {
                                  showMessage(
                                      "Please select when the task should be started approximately",
                                      alert: true);
                                } else if (state.taskCreate!.assignedRoleId ==
                                        null &&
                                    state.taskCreate!.assignedUserIds == null) {
                                  showMessage(
                                      "You should select a user or a role to assign task",
                                      alert: true);
                                } else {
                                  BlocProvider.of<NewTaskBloc>(context)
                                      .createNewTask(
                                          nameController.text,
                                          descriptionController.text,
                                          locationController.text);
                                }
                              }
                            },
                            text: state.ontopLoading ? null : 'Create',
                            child: state.ontopLoading
                                ? Center(
                                    child: CircularProgressIndicator(),
                                  )
                                : null,
                            enabled: !state.ontopLoading,
                          ),
                          SizedBox(height: 24),
                        ],
                      ),
                    ),
                  ),
          ),
        );
      },
    );
  }
}

// Customw widget to select the priority level of a task
class PrioritySelect extends StatelessWidget {
  final TextEditingController priorityController;
  final int selectedPriority;
  PrioritySelect({
    required this.priorityController,
    this.selectedPriority = -1,
  });
  final taskPriority = ['Low', 'Normal', 'High'];
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () async {
        final value = await showModalBottomSheet(
          context: context,
          isScrollControlled: true,
          isDismissible: true,
          backgroundColor: Colors.transparent,
          builder: (context) {
            return DefaultBottomSheet(
              title: "Select Priority",
              children: taskPriority
                  .map(
                    (e) => DefaultMaterialActionSheetAction(
                      text: e,
                      selectedValue: selectedPriority,
                      value: taskPriority.indexOf(e),
                    ),
                  )
                  .toList(),
            );
          },
        );
        if (value != null) {
          priorityController.text = taskPriority[value];
          BlocProvider.of<NewTaskBloc>(context).updatePriority(value);
        }
      },
      child: Container(
        height: 52,
        child: Stack(
          children: [
            DefaultTextField(
              controller: priorityController,
              hintText: "Select Priority",
              enabled: false,
            ),
            Positioned(
              right: 16,
              top: 10,
              child: Icon(Icons.keyboard_arrow_down_rounded),
            )
          ],
        ),
      ),
    );
  }
}

// Custom switch widget to select assigning side for a new task
class AssignSwitch extends StatelessWidget {
  final TextEditingController assignController;
  AssignSwitch({required this.assignController});
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<NewTaskBloc, NewTaskState>(
      builder: (context, state) {
        return Column(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                AssignTypeContainer(
                  title: 'User',
                  selected: state.selectedAssignType,
                  index: 0,
                  controller: assignController,
                ),
                AssignTypeContainer(
                  title: 'Role',
                  selected: state.selectedAssignType,
                  index: 1,
                  controller: assignController,
                ),
              ],
            ),
            SizedBox(height: 32),
            DrowDown(
              isRole: state.selectedAssignType == 1,
              controller: assignController,
            ),
          ],
        );
      },
    );
  }
}

// Custom drop down widget to select role or a user from the list for a new task
class DrowDown extends StatelessWidget {
  final bool isRole;
  final TextEditingController controller;
  DrowDown({this.isRole = false, required this.controller});
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<NewTaskBloc, NewTaskState>(
      builder: (context, state) {
        return GestureDetector(
          onTap: () async {
            final value = await showModalBottomSheet(
              context: context,
              isScrollControlled: true,
              isDismissible: true,
              backgroundColor: Colors.transparent,
              builder: (context) {
                return isRole
                    ? LowerRoleBottom(
                        title: "Select Role",
                        values: state.roles!,
                      )
                    : LowerUserBottom(
                        values: state.users!,
                        title: "Select Users",
                        selected: state.taskCreate!.assignedUserIds,
                      );
              },
            );
            if (value != null) {
              if (isRole) {
                controller.text = value.name;
              } else {
                List<UserShort> users = value;
                controller.text = users.map((e) => '${e.firstName}').join(' ,');
              }
              BlocProvider.of<NewTaskBloc>(context)
                  .updateAssigned(isRole, value);
            }
          },
          child: Container(
            height: 52,
            child: Stack(
              children: [
                DefaultTextField(
                  controller: controller,
                  hintText: isRole ? "Select Role" : "Select Users",
                  enabled: false,
                ),
                Positioned(
                  right: 16,
                  top: 10,
                  child: Icon(Icons.keyboard_arrow_down_rounded),
                )
              ],
            ),
          ),
        );
      },
    );
  }
}

// Custom widget to select assigning party for a new task
class AssignTypeContainer extends StatelessWidget {
  final String title;
  final int index;
  final int selected;
  final TextEditingController controller;
  AssignTypeContainer({
    this.index = 0,
    this.title = '',
    this.selected = 0,
    required this.controller,
  });

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        controller.clear();
        BlocProvider.of<NewTaskBloc>(context).updateAssignType(index);
      },
      child: AnimatedContainer(
        duration: Duration(milliseconds: 200),
        padding: EdgeInsets.symmetric(horizontal: 32, vertical: 8),
        alignment: Alignment.center,
        child: Text18(
          title,
          color: selected == index ? AppColors.black : AppColors.fontColor,
        ),
        decoration: BoxDecoration(
            color: selected == index ? AppColors.background : AppColors.white,
            borderRadius: BorderRadius.circular(8)),
      ),
    );
  }
}

// Custom widget for end time of a new task
class EndTimeContainer extends StatelessWidget {
  final TextEditingController endController;
  EndTimeContainer({required this.endController});
  final DateFormat format = DateFormat("dd/MM/yyyy HH:mm");

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<NewTaskBloc, NewTaskState>(
      builder: (context, state) {
        return GestureDetector(
          onTap: () {
            DatePicker.showDateTimePicker(
              context,
              showTitleActions: true,
              minTime: DateTime.now(),
              theme: DatePickerTheme(
                  backgroundColor: AppColors.white,
                  doneStyle: TextStyle(
                    color: AppColors.accent,
                  ),
                  cancelStyle: TextStyle(
                    color: AppColors.fontColor,
                  )),
              maxTime: DateTime.now().add(
                Duration(
                  days: 7,
                ),
              ),
              onConfirm: (date) {
                endController.text = format.format(date);
                BlocProvider.of<NewTaskBloc>(context)
                    .updateEndDate(endController.text);
              },
              currentTime: state.taskCreate!.taskEnd == null
                  ? DateTime.now()
                  : format.parse(state.taskCreate!.taskEnd!),
              locale: LocaleType.en,
            );
          },
          child: Container(
            height: 52,
            child: Stack(
              children: [
                DefaultTextField(
                  controller: endController,
                  hintText: 'End',
                  enabled: false,
                ),
                Positioned(
                  right: 16,
                  top: 10,
                  child: Icon(Icons.today),
                )
              ],
            ),
          ),
        );
      },
    );
  }
}

// Custom widget for start time of a new task
class StartTimeContainer extends StatelessWidget {
  final TextEditingController startController;
  StartTimeContainer({required this.startController});
  final DateFormat format = DateFormat("dd/MM/yyyy HH:mm");

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<NewTaskBloc, NewTaskState>(
      builder: (context, state) {
        return GestureDetector(
          onTap: () {
            DatePicker.showDateTimePicker(
              context,
              showTitleActions: true,
              minTime: DateTime.now(),
              currentTime: state.taskCreate!.taskStart == null
                  ? DateTime.now()
                  : format.parse(state.taskCreate!.taskStart!),
              theme: DatePickerTheme(
                  backgroundColor: AppColors.white,
                  doneStyle: TextStyle(
                    color: AppColors.accent,
                  ),
                  cancelStyle: TextStyle(
                    color: AppColors.fontColor,
                  )),
              maxTime: DateTime.now().add(
                Duration(
                  days: 7,
                ),
              ),
              onConfirm: (date) {
                startController.text = format.format(date);
                BlocProvider.of<NewTaskBloc>(context)
                    .updateStartDate(startController.text);
              },
              locale: LocaleType.en,
            );
          },
          child: Container(
            height: 52,
            child: Stack(
              children: [
                DefaultTextField(
                  controller: startController,
                  hintText: 'Start',
                  enabled: false,
                ),
                Positioned(
                  right: 16,
                  top: 10,
                  child: Icon(Icons.today),
                )
              ],
            ),
          ),
        );
      },
    );
  }
}
