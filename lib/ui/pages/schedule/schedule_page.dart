import 'dart:io';

import 'package:flutter/material.dart';
import 'package:hospitalproject/constants/values/color_constants.dart';
import 'package:hospitalproject/models/api/user/user.dart';
import 'package:hospitalproject/ui/shared/text16.dart';
import 'package:hospitalproject/ui/shared/text18.dart';
import 'package:hospitalproject/ui/shared/text24.dart';
import 'package:hospitalproject/utils/my_utils.dart';
import 'package:hospitalproject/utils/screen_util.dart';
import 'package:rxdart/subjects.dart';
// UI view for Schedule screen

class SchedulePage extends StatefulWidget {
  final UserProfile userProfile;

  SchedulePage({required this.userProfile});
  @override
  _SchedulePageState createState() => _SchedulePageState();
}

class _SchedulePageState extends State<SchedulePage> {
  @override
  void initState() {
    _selectedWeekDaySubject.add(DateTime.now().weekday - 1);
    super.initState();
  }

  Stream<int> get selectedWeekDay => _selectedWeekDaySubject.stream;
  final _selectedWeekDaySubject = BehaviorSubject<int>();

  @override
  void dispose() {
    // dispose the stream before the screen is disposed
    _selectedWeekDaySubject.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: StreamBuilder(
        stream: _selectedWeekDaySubject,
        initialData: DateTime.now().weekday - 1,
        builder: (BuildContext context, AsyncSnapshot snapshot) {
          return CustomScrollView(
            slivers: [
              MyAppBar(),
              CalendarDates(
                selectedDate: snapshot.data,
                onSelected: (int index) {
                  _selectedWeekDaySubject.add(index);
                },
              ),
              widget.userProfile.schedule!.length == 0
                  ? SliverToBoxAdapter(child: Container())
                  : ScheduleItem(
                      workingHour: widget.userProfile.schedule![snapshot.data],
                    ),
            ],
          );
        },
      ),
    );
  }
}
// Custom widget for schedule item

class ScheduleItem extends StatelessWidget {
  final WorkingHour workingHour;
  ScheduleItem({
    required this.workingHour,
  });
  @override
  Widget build(BuildContext context) {
    return SliverToBoxAdapter(
      child: Container(
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(8),
            color: AppColors.orangeTransparent,
            boxShadow: [
              BoxShadow(
                  color: AppColors.accentTransparent.withOpacity(.5),
                  blurRadius: 4,
                  spreadRadius: 4,
                  offset: Offset(0, 2))
            ]),
        margin: EdgeInsets.symmetric(horizontal: 16, vertical: 32),
        padding: EdgeInsets.symmetric(vertical: 18),
        child: Row(
          children: [
            Container(
              width: 3,
              height: 70,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(1),
                  color: AppColors.orange),
            ),
            SizedBox(width: 16),
            Expanded(
                child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text18(
                  workingHour.name,
                  fontWeight: FontWeight.w500,
                ),
                SizedBox(height: 8),
                Text16(
                  workingHour.startTime.isNotEmpty
                      ? '${workingHour.startTime} - ${workingHour.endTime} '
                      : '-',
                  color: AppColors.fontColor,
                ),
              ],
            ))
          ],
        ),
      ),
    );
  }
}
// Custom widget of an appbar for schedule page

class MyAppBar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SliverToBoxAdapter(
      child: Container(
          padding: EdgeInsets.only(
            left: 16,
            right: 16,
            top: statusBarHeight(context) + 20,
            bottom: 24,
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              GestureDetector(
                child: Icon(
                  Platform.isIOS ? Icons.arrow_back_ios : Icons.arrow_back,
                  color: AppColors.black,
                  size: 24,
                ),
                onTap: () {
                  Navigator.pop(context);
                },
              ),
              SizedBox(height: 20),
              Text24(
                'My Schedule',
                fontWeight: FontWeight.w600,
              ),
            ],
          )),
    );
  }
}

// Calendar dates for custom calendar view
class CalendarDates extends StatelessWidget {
  final int selectedDate;
  final Function onSelected;
  CalendarDates({
    required this.selectedDate,
    required this.onSelected,
  });
  final weekDay = MyUtils.daysInRange(MyUtils.firstDayOfWeek(DateTime.now()),
          MyUtils.lastDayOfWeek(DateTime.now()))
      .toList()
      .sublist(0, 7);
  final List<String> shortWeekdays = const [
    "M",
    "T",
    "W",
    "T",
    "F",
    "S",
    "S",
  ];

  @override
  Widget build(BuildContext context) {
    return SliverToBoxAdapter(
      child: Container(
          margin: EdgeInsets.symmetric(horizontal: 16),
          padding: EdgeInsets.only(bottom: 24),
          decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(8),
              boxShadow: [
                BoxShadow(
                  color: AppColors.accentTransparent.withOpacity(.5),
                  blurRadius: 8,
                  spreadRadius: 4,
                  offset: Offset(0, 4),
                )
              ]),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                padding:
                    EdgeInsets.only(left: 12, right: 12, top: 24, bottom: 16),
                child: Text16(
                  MyUtils.getTodayDate(),
                  color: AppColors.fontColor,
                ),
              ),
              GridView.builder(
                padding: EdgeInsets.symmetric(horizontal: 12),
                shrinkWrap: true,
                physics: NeverScrollableScrollPhysics(),
                itemCount: shortWeekdays.length,
                gridDelegate: new SliverGridDelegateWithFixedCrossAxisCount(
                    childAspectRatio: 25 / 40, crossAxisCount: 7),
                itemBuilder: (context, index) {
                  String day = shortWeekdays[index];
                  int date = weekDay[index].day;
                  bool selected = index == selectedDate;
                  return GestureDetector(
                    onTap: () {
                      onSelected(index);
                    },
                    child: CalendarListTile(
                        date: date, weekDay: day, isToday: selected),
                  );
                },
              ),
            ],
          )),
    );
  }
}

// Custom container for calendar date for calendar widget
class CalendarListTile extends StatelessWidget {
  final String weekDay;
  final int date;
  final bool isToday;

  CalendarListTile({
    this.isToday = false,
    required this.date,
    required this.weekDay,
  });
  @override
  Widget build(BuildContext context) {
    return AnimatedContainer(
      curve: Curves.easeIn,
      duration: Duration(milliseconds: 200),
      padding: EdgeInsets.only(left: 4, right: 4, top: 8, bottom: 2),
      margin: EdgeInsets.symmetric(horizontal: 5, vertical: 1),
      decoration: BoxDecoration(
        color: isToday ? AppColors.accent : AppColors.white,
        borderRadius: BorderRadius.circular(20.0),
        boxShadow: <BoxShadow>[
          BoxShadow(
            color: isToday ? AppColors.background : Colors.white,
            blurRadius: 1.0,
            spreadRadius: 1.0,
          )
        ],
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Text16(
            weekDay,
            color: isToday ? Colors.white : Colors.black,
            textAlign: TextAlign.center,
            fontWeight: FontWeight.w500,
          ),
          SizedBox(height: 4),
          Container(
            height: 36,
            width: 36,
            decoration: BoxDecoration(
              shape: BoxShape.circle,
              color: AppColors.accentTransparent,
            ),
            child: Center(
              child: Text16(
                date.toString(),
                color: isToday ? Colors.black : AppColors.fontColor,
                textAlign: TextAlign.center,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
