import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:flutter_svg/svg.dart';
import 'package:hospitalproject/blocs/task_archive/task_archive_bloc.dart';
import 'package:hospitalproject/constants/assets/icon_constants.dart';
import 'package:hospitalproject/constants/values/color_constants.dart';
import 'package:hospitalproject/constants/values/route_constants.dart';
import 'package:hospitalproject/models/api/user_short/user_short.dart';
import 'package:hospitalproject/ui/pages/new_task/new_task_bottom.dart';
import 'package:hospitalproject/ui/shared/default_button.dart';
import 'package:hospitalproject/ui/shared/default_text_field.dart';
import 'package:hospitalproject/ui/shared/text16.dart';
import 'package:hospitalproject/ui/shared/text18.dart';
import 'package:hospitalproject/ui/shared/text20.dart';
import 'package:hospitalproject/ui/shared/top_message.dart';
import 'package:hospitalproject/utils/api_utils.dart';
import 'package:hospitalproject/utils/screen_util.dart';
// UI view for Task Archive screen

class TaskArchivePage extends StatefulWidget {
  @override
  _TaskArchivePageState createState() => _TaskArchivePageState();
}

class _TaskArchivePageState extends State<TaskArchivePage> {
  late TaskArchiveBloc bloc;
  TextEditingController fromController = TextEditingController();
  TextEditingController toController = TextEditingController();

  @override
  void initState() {
    bloc = TaskArchiveBloc();
    // When the UI is loaded, load list of users from the server through BloC
    WidgetsBinding.instance!.addPostFrameCallback((timeStamp) {
      bloc.loadUsers();
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => bloc,
      child: GestureDetector(
        onTap: () {
          FocusScope.of(context).unfocus();
        },
        child: Scaffold(
          body: Container(
            height: screenHeight(context),
            child: Stack(
              children: [
                MyAppBar(),
                MyBody(
                  fromController: fromController,
                  toController: toController,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

// Custom widget for the body content
class MyBody extends StatelessWidget {
  final TextEditingController fromController, toController;
  MyBody({
    required this.fromController,
    required this.toController,
  });
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<TaskArchiveBloc, TaskArchiveState>(
      builder: (context, state) {
        return Positioned(
          top: screenHeight(context) * 1 / 3,
          child: Container(
            padding: EdgeInsets.only(top: 48, left: 24, right: 24, bottom: 24),
            height: screenHeight(context) * 2 / 3,
            width: MediaQuery.of(context).size.width,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(32),
              boxShadow: [
                BoxShadow(
                  color: AppColors.accentTransparent.withOpacity(.5),
                  blurRadius: 8,
                  spreadRadius: 4,
                  offset: Offset(0, 4),
                )
              ],
              color: AppColors.white,
            ),
            child: state.loading
                ? Center(
                    child: CircularProgressIndicator(),
                  )
                : SingleChildScrollView(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text18(
                          "Assigned date",
                          fontWeight: FontWeight.w500,
                        ),
                        SizedBox(height: 12),
                        Row(
                          children: [
                            Expanded(
                              child: DefaultTextField(
                                controller: fromController,
                                hintText: 'From',
                                readOnly: true,
                                onTap: () async {
                                  final value = await openDatePicker(context,
                                      selected: state.post!.fromDate);
                                  if (value != null) {
                                    DateTime date = value;
                                    fromController.text =
                                        ApiUtils.getFormattedDate(
                                            date.millisecondsSinceEpoch);
                                    BlocProvider.of<TaskArchiveBloc>(context)
                                        .updateFromDate(date);
                                  }
                                },
                              ),
                            ),
                            SizedBox(width: 12),
                            Expanded(
                              child: DefaultTextField(
                                controller: toController,
                                hintText: 'To',
                                readOnly: true,
                                onTap: () async {
                                  final value = await openDatePicker(context,
                                      selected: state.post!.toDate);
                                  if (value != null) {
                                    DateTime date = value;
                                    toController.text =
                                        ApiUtils.getFormattedDate(
                                            date.millisecondsSinceEpoch);
                                    BlocProvider.of<TaskArchiveBloc>(context)
                                        .updateToDate(date);
                                  }
                                },
                              ),
                            )
                          ],
                        ),
                        SizedBox(height: 28),
                        CreateByMeFilter(),
                        SizedBox(height: 28),
                        TaskAuthorFilter(),
                        SizedBox(height: 28),
                        CommonWith(),
                        SizedBox(height: 48),
                        Buttons(
                          fromController: fromController,
                          toController: toController,
                        ),
                      ],
                    ),
                  ),
          ),
        );
      },
    );
  }

// Date picker to select start and end date for a filtering form
  Future openDatePicker(BuildContext context, {int? selected}) async {
    return await DatePicker.showDatePicker(
      context,
      showTitleActions: true,
      theme: DatePickerTheme(
          backgroundColor: AppColors.white,
          doneStyle: TextStyle(
            color: AppColors.accent,
          ),
          cancelStyle: TextStyle(
            color: AppColors.fontColor,
          )),
      maxTime: DateTime.now(),
      onConfirm: (date) {},
      currentTime: selected == null
          ? DateTime.now()
          : DateTime.fromMillisecondsSinceEpoch(selected),
      locale: LocaleType.en,
    );
  }
}

// Custom widget for "create by me" switch on task archive from
class CreateByMeFilter extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<TaskArchiveBloc, TaskArchiveState>(
      builder: (context, state) {
        return Column(
          children: [
            Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Text18(
                  "Created by me",
                  fontWeight: FontWeight.w500,
                ),
                SizedBox(width: 24),
                GestureDetector(
                  onTap: () {
                    BlocProvider.of<TaskArchiveBloc>(context)
                        .updateCreateByMe(!state.post!.myTasks);
                  },
                  child: AnimatedContainer(
                    duration: Duration(milliseconds: 200),
                    height: 20,
                    width: 20,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(3),
                      color: state.post!.myTasks
                          ? AppColors.accent
                          : Colors.transparent,
                      border: Border.all(
                        color: AppColors.accent,
                      ),
                    ),
                    child: Center(
                      child: Icon(
                        Icons.check,
                        color: AppColors.white,
                        size: 16,
                      ),
                    ),
                  ),
                )
              ],
            )
          ],
        );
      },
    );
  }
}

// Custom buttons of reset and apply
class Buttons extends StatelessWidget {
  final TextEditingController fromController, toController;

  Buttons({required this.fromController, required this.toController});
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<TaskArchiveBloc, TaskArchiveState>(
      builder: (context, state) {
        return Container(
          child: Row(
            children: [
              Expanded(
                child: DefaultButton(
                  onPressed: () {
                    FocusScope.of(context).unfocus();
                    BlocProvider.of<TaskArchiveBloc>(context)
                        .resetFilterValues();
                    fromController.clear();
                    toController.clear();
                  },
                  text: 'Reset',
                  isFilled: false,
                ),
              ),
              SizedBox(width: 12),
              Expanded(
                child: DefaultButton(
                  onPressed: () {
                    Navigator.pushNamed(context, AppRoutes.taskListPage,
                        arguments: state.post);
                  },
                  text: 'Apply',
                ),
              ),
            ],
          ),
        );
      },
    );
  }
}

// Custom widget for "Common with" box on the task archive form
class CommonWith extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<TaskArchiveBloc, TaskArchiveState>(
      builder: (context, state) {
        return Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              children: [
                Text18(
                  "Common  with",
                  fontWeight: FontWeight.w500,
                ),
                IconButton(
                  icon: Icon(
                    Icons.keyboard_arrow_down_rounded,
                    color: state.post!.myTasks
                        ? AppColors.fontColor
                        : AppColors.black,
                  ),
                  onPressed: () async {
                    if (!state.post!.myTasks) {
                      final value = await showModalBottomSheet(
                        context: context,
                        isScrollControlled: true,
                        isDismissible: true,
                        backgroundColor: Colors.transparent,
                        builder: (context) {
                          return LowerUserBottom(
                            values: state.users!,
                            title: "Select Users",
                            selected: state.post!.assignedUsers,
                            // selected: state.taskCreate!.assignedUserIds,
                          );
                        },
                      );
                      if (value != null) {
                        List<UserShort> users = value;
                        if (users.length > 4) {
                          showMessage("Please select less than 4 users",
                              alert: true);
                        } else {
                          BlocProvider.of<TaskArchiveBloc>(context)
                              .updateCommonWith(users);
                        }
                      }
                    }
                  },
                )
              ],
            ),
            SizedBox(height: 6),
            Wrap(
              spacing: 8.0,
              runSpacing: 4.0,
              children: state.commonWithUsers!
                  .map((e) => Container(
                        padding:
                            EdgeInsets.symmetric(horizontal: 12, vertical: 8),
                        decoration: BoxDecoration(
                          color: AppColors.accent.withOpacity(.3),
                          borderRadius: BorderRadius.circular(24),
                        ),
                        child: Text16('${e.firstName} ${e.lastName}'),
                      ))
                  .toList(),
            )
          ],
        );
      },
    );
  }
}

// Custom widget for "task author" box on the task archive form

class TaskAuthorFilter extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<TaskArchiveBloc, TaskArchiveState>(
      builder: (context, state) {
        return Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              children: [
                Text18(
                  "Assigned by",
                  fontWeight: FontWeight.w500,
                ),
                IconButton(
                  icon: Icon(
                    Icons.keyboard_arrow_down_rounded,
                    color: state.post!.myTasks
                        ? AppColors.fontColor
                        : AppColors.black,
                  ),
                  onPressed: () async {
                    if (!state.post!.myTasks) {
                      final value = await showModalBottomSheet(
                        context: context,
                        isScrollControlled: true,
                        isDismissible: true,
                        backgroundColor: Colors.transparent,
                        builder: (context) {
                          return LowerUserBottom(
                            values: state.users!,
                            title: "Select Users",
                            selected: state.post!.createdUsers,
                            // selected: state.taskCreate!.assignedUserIds,
                          );
                        },
                      );
                      if (value != null) {
                        List<UserShort> users = value;

                        BlocProvider.of<TaskArchiveBloc>(context)
                            .updateCreatorUsers(users);
                      }
                    }
                  },
                )
              ],
            ),
            SizedBox(height: 6),
            Wrap(
              spacing: 8.0,
              runSpacing: 4.0,
              children: state.assignedByUsers!
                  .map((e) => Container(
                        padding:
                            EdgeInsets.symmetric(horizontal: 12, vertical: 8),
                        decoration: BoxDecoration(
                          color: AppColors.accent.withOpacity(.3),
                          borderRadius: BorderRadius.circular(24),
                        ),
                        child: Text16('${e.firstName} ${e.lastName}'),
                      ))
                  .toList(),
            )
          ],
        );
      },
    );
  }
}

// Custom appbar for task archive screen
class MyAppBar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(
          top: statusBarHeight(context) + 20, right: 24, left: 24),
      // height: statusBarHeight(context) + 80,
      child: Column(
        children: [
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              GestureDetector(
                child: Icon(
                  Platform.isIOS ? Icons.arrow_back_ios : Icons.arrow_back,
                  color: AppColors.black,
                  size: 24,
                ),
                onTap: () {
                  Navigator.pop(context);
                },
              ),
              Expanded(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text20(
                      "Task Archive",
                      fontWeight: FontWeight.w600,
                      textAlign: TextAlign.center,
                    ),
                    SizedBox(width: 24)
                  ],
                ),
              )
            ],
          ),
          SizedBox(height: 32),
          SvgPicture.asset(
            AppIcons.searchIcon,
            height: screenHeight(context) / 7,
          )
        ],
      ),
    );
  }
}
