import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/svg.dart';
import 'package:hospitalproject/blocs/auth/auth_bloc.dart';
import 'package:hospitalproject/constants/assets/icon_constants.dart';
import 'package:hospitalproject/constants/values/color_constants.dart';
import 'package:hospitalproject/ui/shared/app_text.dart';
import 'package:hospitalproject/ui/shared/default_button.dart';
import 'package:hospitalproject/ui/shared/default_text_field.dart';
import 'package:hospitalproject/ui/shared/text14.dart';
import 'package:hospitalproject/ui/shared/text16.dart';
import 'package:hospitalproject/utils/text/email_validator.dart';
// UI view for Auth screen

class AuthPage extends StatefulWidget {
  @override
  _AuthPageState createState() => _AuthPageState();
}

class _AuthPageState extends State<AuthPage> {
  final TextEditingController userNameController = TextEditingController();
  final TextEditingController passwordController = TextEditingController();
  GlobalKey<FormState> formKey = GlobalKey<FormState>();
  @override
  void initState() {
    // userNameController.text = 'nurse1@gmail.com';
    // passwordController.text = '123';
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final screenHeight = MediaQuery.of(context).size.height;
    return BlocProvider(
      create: (context) => AuthBloc(),
      child: BlocBuilder<AuthBloc, AuthState>(
        builder: (context, state) {
          return Scaffold(
            body: SingleChildScrollView(
              physics: BouncingScrollPhysics(),
              padding: EdgeInsets.symmetric(
                horizontal: 24,
                vertical: 24,
              ),
              child: Form(
                key: formKey,
                child: Column(
                  children: [
                    Container(
                      height: screenHeight / 3,
                      padding: EdgeInsets.all(8),
                      margin: EdgeInsets.only(top: 24),
                      child: SvgPicture.asset(AppIcons.medicine),
                    ),
                    Container(
                      height: screenHeight / 2,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          AppText(
                            "Log in to use features",
                            fontWeight: FontWeight.bold,
                            fontSize: 26,
                          ),
                          SizedBox(height: 6),
                          Text14(
                            'Only for users who already have an account in the system',
                            color: AppColors.textFieldHintColor,
                          ),
                          SizedBox(height: 20),
                          Text16(
                            'Email',
                            color: AppColors.textFieldHintColor,
                          ),
                          SizedBox(height: 8),
                          DefaultTextField(
                            controller: userNameController,
                            hintText: 'Jane92@gmail.com',
                            keyboardType: TextInputType.emailAddress,
                            isRequired: true,
                            fillColor: AppColors.textFieldBakcGrounAlt,
                            customValidator: (String? text) {
                              if (text != null) {
                                if (!EmailValidator.isEmail(text)) {
                                  return 'Please, enter valid email';
                                }
                              }
                            },
                          ),
                          SizedBox(height: 16),
                          Text16(
                            'Password',
                            color: AppColors.textFieldHintColor,
                          ),
                          SizedBox(height: 8),
                          DefaultTextField(
                            controller: passwordController,
                            hintText: '*****',
                            password: true,
                            maxLines: 1,
                            isRequired: true,
                            fillColor: AppColors.textFieldBakcGrounAlt,
                          ),
                        ],
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.only(
                        right: 24,
                        left: 24,
                        bottom: MediaQuery.of(context).padding.bottom,
                      ),
                      child: DefaultButton(
                        onPressed: () {
                          // validate the form and call login function on the BLoC
                          if (formKey.currentState!.validate()) {
                            BlocProvider.of<AuthBloc>(context).login(
                                userNameController.text.trim(),
                                passwordController.text.trim());
                            // Navigator.pushNamed(context, AppRoutes.homePage,
                            //     arguments: LoginResponse(
                            //         accessToken: "accessToken",
                            //         user: UserShort(
                            //             firstName: "Fotima",
                            //             lastName: "Khayrullaeva",
                            //             id: 0)));
                          }
                        },
                        text: state.loading ? null : "Login",
                        enabled: !state.loading,
                        child: state.loading
                            ? Center(
                                child: CircularProgressIndicator(),
                              )
                            : null,
                      ),
                    ),
                  ],
                ),
              ),
            ),
            // bottomSheet:
          );
        },
      ),
    );
  }
}
