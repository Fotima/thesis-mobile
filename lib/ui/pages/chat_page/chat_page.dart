import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/svg.dart';
import 'package:hospitalproject/blocs/chat/chat_bloc.dart';
import 'package:hospitalproject/constants/assets/icon_constants.dart';
import 'package:hospitalproject/constants/values/color_constants.dart';
import 'package:hospitalproject/models/api/chat/chat.dart';
import 'package:hospitalproject/models/api/user_short/user_short.dart';
import 'package:hospitalproject/ui/shared/text14.dart';
import 'package:hospitalproject/ui/shared/text18.dart';
import 'package:hospitalproject/utils/api_utils.dart';
import 'package:hospitalproject/utils/screen_util.dart';
import 'package:hospitalproject/utils/shared_pref/shared_pref_helper.dart';
import 'package:rxdart/subjects.dart';
// UI view for Auth screen

class ChatPage extends StatefulWidget {
  final int? chatId;
  final int? withUserId;
  final UserShort withUser;
  ChatPage({this.chatId, this.withUserId, required this.withUser});
  @override
  _ChatPageState createState() => _ChatPageState();
}

class _ChatPageState extends State<ChatPage> {
  late ChatBloc bloc;
  @override
  void initState() {
    bloc = ChatBloc();
    // when the ui is built, call the function from the Bloc class
    WidgetsBinding.instance!.addPostFrameCallback((timeStamp) {
      bloc.loadChatMessages(0, widget.withUserId, widget.chatId);
    });
    super.initState();
  }

  @override
  void dispose() {
    // close the ws channel when the page is disposed
    bloc.state.channel!.sink.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => bloc,
      child: Scaffold(
        body: Container(
          height: screenHeight(context),
          width: screenWidth(context),
          child: Stack(
            children: [
              Body(),
              MyAppBar(
                withUser: widget.withUser,
              ),
              ChatTextField(
                withUserId: widget.withUserId,
              ),
            ],
          ),
        ),
      ),
    );
  }
}

// UI of Body widget of chat page
class Body extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ChatBloc, ChatState>(
      builder: (context, state) {
        return Positioned(
          top: statusBarHeight(context) + 70,
          child: Container(
            height: screenHeight(context) -
                (statusBarHeight(context) + paddingBottomHeight(context) + 120),
            width: screenWidth(context),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(32),
              boxShadow: [
                BoxShadow(
                  color: AppColors.accentTransparent.withOpacity(.5),
                  blurRadius: 8,
                  spreadRadius: 4,
                  offset: Offset(0, 4),
                )
              ],
              color: AppColors.accentTransparent,
            ),
            child: state.loading
                ? Center(
                    child: CircularProgressIndicator(),
                  )
                : MessageBoard(),
          ),
        );
      },
    );
  }
}

// Custom message board widget of Chat page
class MessageBoard extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ChatBloc, ChatState>(
      builder: (context, state) {
        return Container(
          margin: EdgeInsets.symmetric(horizontal: 16),
          child: ListView.builder(
            shrinkWrap: true,
            primary: false,
            reverse: true,
            itemCount: state.messageByDate!.length,
            itemBuilder: (context, index) {
              return MessageOfTheDay(
                  messageByDate: state.messageByDate![index]);
            },
          ),
        );
      },
    );
  }
}

// custom widget of a date containt and list of message for this day
class MessageOfTheDay extends StatelessWidget {
  final ChatMessageByDate messageByDate;
  MessageOfTheDay({required this.messageByDate});
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          Container(
            padding: EdgeInsets.symmetric(horizontal: 16, vertical: 8),
            decoration: BoxDecoration(
                color: AppColors.background,
                borderRadius: BorderRadius.circular(16)),
            child: Text14(ApiUtils.getDateMonth(messageByDate.date!)),
          ),
          SizedBox(height: 12),
          ListView.builder(
            shrinkWrap: true,
            primary: false,
            itemCount: messageByDate.messages!.length,
            itemBuilder: (context, index) {
              return MessageContainer(
                message: messageByDate.messages![index],
              );
            },
          ),
          SizedBox(height: 12),
        ],
      ),
    );
  }
}
// Custom message container for chat page

class MessageContainer extends StatelessWidget {
  final ChatMessage message;
  MessageContainer({
    required this.message,
  });
  @override
  Widget build(BuildContext context) {
    final isMy = SharedPrefHelper.getMyId() == message.fromUser!.id;
    return Container(
      margin: EdgeInsets.only(bottom: 16),
      child: Column(
        crossAxisAlignment:
            isMy ? CrossAxisAlignment.end : CrossAxisAlignment.start,
        children: <Widget>[
          Container(
            padding: EdgeInsets.all(16.0),
            margin: EdgeInsets.only(left: isMy ? 48 : 0, right: isMy ? 0 : 48),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(18),
                border:
                    Border.all(color: AppColors.accentTransparent, width: 1),
                color: isMy ? AppColors.accent : AppColors.background),
            child: Container(
              child: Text14(
                message.message,
                color: isMy ? Colors.white : AppColors.black,
              ),
            ),
          ),
          SizedBox(height: 8),
          Container(
            padding: EdgeInsets.only(left: 8),
            child: Text14(ApiUtils.getTime(message.created!),
                color: AppColors.fontColor),
          )
        ],
      ),
    );
  }
}

// Custom textfield for chat page
class ChatTextField extends StatefulWidget {
  final int? withUserId;
  ChatTextField({this.withUserId});
  @override
  _ChatTextFieldState createState() => _ChatTextFieldState();
}

class _ChatTextFieldState extends State<ChatTextField> {
  Stream<bool> get showSendButton => _showSendButtonSubject.stream;
  final _showSendButtonSubject = BehaviorSubject<bool>();
  TextEditingController searchController = TextEditingController();
  @override
  void initState() {
    _showSendButtonSubject.add(false);
    super.initState();
  }

  @override
  void dispose() {
    _showSendButtonSubject.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ChatBloc, ChatState>(
      builder: (context, state) {
        return Positioned(
          bottom: 0,
          left: 0,
          child: state.loading
              ? Container()
              : Container(
                  height: 70 + paddingBottomHeight(context),
                  width: screenWidth(context),
                  padding: EdgeInsets.only(
                    bottom: paddingBottomHeight(context),
                    left: 16,
                    right: 16,
                    top: 16,
                  ),
                  decoration: BoxDecoration(
                    color: AppColors.background,
                    borderRadius: BorderRadius.circular(12),
                    border: Border.all(color: AppColors.background, width: 1),
                  ),
                  child: Container(
                    height: 50,
                    padding: EdgeInsets.symmetric(horizontal: 16),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(12),
                      color: AppColors.accentTransparent,
                    ),
                    child: Row(
                      children: [
                        Expanded(
                          child: TextFormField(
                            controller: searchController,
                            onChanged: (String? text) {
                              if (text != null) {
                                if (text.length == 0) {
                                  _showSendButtonSubject.add(false);
                                } else {
                                  _showSendButtonSubject.add(true);
                                }
                              }
                            },
                            style: TextStyle(
                              color: AppColors.black,
                              fontSize: 16,
                            ),
                            decoration: InputDecoration(
                              border: InputBorder.none,
                              focusedBorder: InputBorder.none,
                              enabledBorder: InputBorder.none,
                            ),
                          ),
                        ),
                        SizedBox(width: 8),
                        StreamBuilder(
                          stream: _showSendButtonSubject,
                          initialData: false,
                          builder:
                              (BuildContext context, AsyncSnapshot snapshot) {
                            return GestureDetector(
                              onTap: () {
                                if (state.webSocketConnected) {
                                  BlocProvider.of<ChatBloc>(context)
                                      .sendMessage(searchController.text);
                                } else if (state.chatRoom != null) {
                                  if (state.chatRoom!.chatId == null) {
                                    BlocProvider.of<ChatBloc>(context)
                                        .createChatRoomAndConnectToStomp(
                                            widget.withUserId!,
                                            searchController.text);
                                  }
                                }
                                searchController.clear();
                              },
                              child: AnimatedContainer(
                                duration: Duration(milliseconds: 200),
                                height: snapshot.data ? 28 : 0,
                                width: snapshot.data ? 28 : 0,
                                child: SvgPicture.asset(
                                  AppIcons.send,
                                  color: AppColors.accent,
                                  width: 28,
                                ),
                              ),
                            );
                          },
                        ),
                      ],
                    ),
                  ),
                ),
        );
      },
    );
  }
}
// Custom appbar widget for chat page

class MyAppBar extends StatelessWidget {
  final UserShort withUser;
  MyAppBar({required this.withUser});
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(
        top: statusBarHeight(context) + 10,
        left: 8,
        right: 24,
      ),
      child: Row(
        children: [
          IconButton(
            icon: Icon(
              Platform.isIOS
                  ? Icons.arrow_back_ios_rounded
                  : Icons.arrow_back_rounded,
              color: AppColors.black,
            ),
            onPressed: () {
              Navigator.pop(context);
            },
          ),
          SizedBox(width: 12),
          Expanded(
              child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            children: [
              Text18('${withUser.firstName} ${withUser.lastName}'),
              SizedBox(height: 2),
              Text14(
                withUser.role == null ? '' : withUser.role!.name,
                color: AppColors.textColor,
                fontWeight: FontWeight.w500,
              ),
            ],
          ))
        ],
      ),
    );
  }
}
