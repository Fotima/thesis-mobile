import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/svg.dart';
import 'package:hospitalproject/blocs/profile_edit/profile_edit_bloc.dart';
import 'package:hospitalproject/constants/assets/icon_constants.dart';
import 'package:hospitalproject/constants/values/color_constants.dart';
import 'package:hospitalproject/models/api/user/user.dart';
import 'package:hospitalproject/ui/shared/default_button.dart';
import 'package:hospitalproject/ui/shared/default_text_field.dart';
import 'package:hospitalproject/ui/shared/text14.dart';
import 'package:hospitalproject/ui/shared/text20.dart';
import 'package:hospitalproject/utils/screen_util.dart';
// UI view for Profile edit screen

class ProfileEditPage extends StatefulWidget {
  final UserProfile profileData;
  ProfileEditPage({
    required this.profileData,
  });
  @override
  _ProfileEditPageState createState() => _ProfileEditPageState();
}

class _ProfileEditPageState extends State<ProfileEditPage> {
  GlobalKey<FormState> formKey = GlobalKey<FormState>();
  TextEditingController firstNameController = TextEditingController();
  TextEditingController lastNameController = TextEditingController();
  TextEditingController phoneController = TextEditingController();
  @override
  void initState() {
    // Set values to the text editing controllers
    firstNameController.text = widget.profileData.firstName;
    lastNameController.text = widget.profileData.lastName;
    phoneController.text = widget.profileData.phoneNumber;

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => ProfileEditBloc(),
      child: Scaffold(
        body: Container(
          height: screenHeight(context),
          child: Stack(
            children: [
              Body(
                firstNameController: firstNameController,
                lastNameController: lastNameController,
                phoneNumberController: phoneController,
                formKey: formKey,
              ),
              MyAppBar(),
            ],
          ),
        ),
      ),
    );
  }
}

// Custom appbar widget for profile edit page
class MyAppBar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(
          top: statusBarHeight(context) + 20, right: 24, left: 24),
      // height: statusBarHeight(context) + 80,
      child: Column(
        children: [
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              GestureDetector(
                child: Icon(
                  Platform.isIOS ? Icons.arrow_back_ios : Icons.arrow_back,
                  color: AppColors.black,
                  size: 24,
                ),
                onTap: () {
                  Navigator.pop(context);
                },
              ),
              Expanded(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text20(
                      "Edit profile",
                      fontWeight: FontWeight.w600,
                      textAlign: TextAlign.center,
                    ),
                    SizedBox(width: 24)
                  ],
                ),
              )
            ],
          ),
          SizedBox(height: 32),
          SvgPicture.asset(
            AppIcons.personalInfo,
            height: screenHeight(context) / 7,
          )
        ],
      ),
    );
  }
}

// Custom widget for body content of a profile edit page
class Body extends StatelessWidget {
  final TextEditingController firstNameController,
      lastNameController,
      phoneNumberController;
  final GlobalKey<FormState> formKey;

  Body({
    required this.firstNameController,
    required this.lastNameController,
    required this.phoneNumberController,
    required this.formKey,
  });

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ProfileEditBloc, ProfileEditState>(
      builder: (context, state) {
        return Positioned(
          top: screenHeight(context) * 1 / 3,
          child: Container(
            padding: EdgeInsets.only(top: 32, left: 24, right: 24, bottom: 24),
            height: screenHeight(context) * 2 / 3,
            width: MediaQuery.of(context).size.width,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(32),
              boxShadow: [
                BoxShadow(
                  color: AppColors.accentTransparent.withOpacity(.5),
                  blurRadius: 8,
                  spreadRadius: 4,
                  offset: Offset(0, 4),
                )
              ],
              color: AppColors.white,
            ),
            child: Form(
              key: formKey,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text14(
                    "Please refer to the system admin to update other personal data in your account",
                    color: AppColors.fontColor,
                  ),
                  SizedBox(height: 24),
                  DefaultTextField(
                    controller: firstNameController,
                    hintText: 'John',
                    isRequired: true,
                  ),
                  SizedBox(height: 18),
                  DefaultTextField(
                    controller: lastNameController,
                    hintText: 'Doe',
                    isRequired: true,
                  ),
                  SizedBox(height: 18),
                  DefaultTextField(
                    controller: phoneNumberController,
                    hintText: 'Phone number',
                  ),
                  Expanded(child: Container()),
                  DefaultButton(
                    onPressed: () {
                      if (formKey.currentState!.validate()) {
                        BlocProvider.of<ProfileEditBloc>(context)
                            .updateProfileData(UserProfileUpdatePost(
                                firstName: firstNameController.text,
                                lastName: lastNameController.text,
                                phoneNumber: phoneNumberController.text));
                      }
                    },
                    text: state.loading ? null : 'Save',
                    child: state.loading
                        ? Center(
                            child: CircularProgressIndicator(),
                          )
                        : null,
                    enabled: !state.loading,
                  ),
                  SizedBox(height: 24),
                ],
              ),
            ),
          ),
        );
      },
    );
  }
}
