import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:hospitalproject/constants/assets/icon_constants.dart';
import 'package:hospitalproject/constants/values/color_constants.dart';
import 'package:hospitalproject/ui/shared/text16.dart';
import 'package:hospitalproject/utils/screen_util.dart';
// Custom widget for No data view

class NoDataContainer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        SvgPicture.asset(
          AppIcons.empty,
          width: screenWidth(context) / 2,
        ),
        SizedBox(height: 16),
        Text16(
          'No Data',
          color: AppColors.fontColor,
        ),
      ],
    );
  }
}
