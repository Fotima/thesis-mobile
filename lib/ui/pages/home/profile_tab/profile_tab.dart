import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/svg.dart';
import 'package:hospitalproject/blocs/profile/profile_bloc.dart';
import 'package:hospitalproject/constants/assets/icon_constants.dart';
import 'package:hospitalproject/constants/values/color_constants.dart';
import 'package:hospitalproject/constants/values/route_constants.dart';
import 'package:hospitalproject/models/api/user/user.dart';
import 'package:hospitalproject/ui/shared/app_text.dart';
import 'package:hospitalproject/ui/shared/text14.dart';
import 'package:hospitalproject/ui/shared/text16.dart';
import 'package:hospitalproject/ui/shared/text18.dart';
import 'package:hospitalproject/utils/screen_util.dart';
// UI view for Profile tab screen

class ProfileTab extends StatefulWidget {
  @override
  _ProfileTabState createState() => _ProfileTabState();
}

class _ProfileTabState extends State<ProfileTab> {
  late ProfileBloc bloc;
  @override
  void initState() {
    bloc = ProfileBloc();
    // When the UI is loaded, request profile data from the BLoC
    WidgetsBinding.instance!.addPostFrameCallback((timeStamp) {
      bloc.loadProfileData();
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // backgroundColor: AppColors.accentTransparent,
      body: BlocProvider(
        create: (context) => bloc,
        child: BlocBuilder<ProfileBloc, ProfileState>(
          builder: (context, state) {
            return Container(
              height: screenHeight(context),
              child: state.loading
                  ? Center(
                      child: CircularProgressIndicator(),
                    )
                  : state.userProfile == null
                      ? Container()
                      : Stack(
                          children: [
                            MyAppBar(),
                            MyBody(),
                            state.onTopLoading
                                ? Container(
                                    height: screenHeight(context),
                                    color: Colors.black.withOpacity(.08),
                                    child: Center(
                                      child: CircularProgressIndicator(),
                                    ),
                                  )
                                : Container()
                          ],
                        ),
            );
          },
        ),
      ),
    );
  }
}

// Custom appbar widget for the profile tab
class MyAppBar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(
          top: statusBarHeight(context) + 20, right: 24, left: 24),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              AppText(
                "Profile",
                fontWeight: FontWeight.bold,
                fontSize: 28,
              ),
            ],
          ),
          SizedBox(height: 32),
          UserInfoCard(),
        ],
      ),
    );
  }
}
// Custom container widget for user info

class UserInfoCard extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ProfileBloc, ProfileState>(
      builder: (context, state) {
        return Container(
          padding: EdgeInsets.symmetric(horizontal: 16, vertical: 16),
          decoration: BoxDecoration(
            color: AppColors.accentDark,
            borderRadius: BorderRadius.circular(12),
          ),
          child: Row(
            children: [
              Container(
                height: 50,
                width: 50,
                alignment: Alignment.center,
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  color: AppColors.background,
                ),
                child: Container(
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    color: AppColors.accentDark,
                  ),
                  height: 48,
                  width: 48,
                  child: Center(
                    child: Icon(
                      Icons.person,
                      size: 36,
                      color: AppColors.white,
                    ),
                  ),
                ),
              ),
              SizedBox(width: 12),
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Text18(
                      '${state.userProfile!.firstName} ${state.userProfile!.lastName}',
                      fontWeight: FontWeight.w600,
                      color: Colors.white,
                    ),
                    SizedBox(height: 2),
                    Text14(
                      state.userProfile!.role,
                      color: AppColors.greyDisabled,
                    ),
                    SizedBox(height: 2),
                    state.userProfile!.phoneNumber.isEmpty
                        ? Container()
                        : Text14(
                            state.userProfile!.phoneNumber,
                            color: AppColors.greyDisabled,
                          )
                  ],
                ),
              ),
              SizedBox(width: 12),
              GestureDetector(
                onTap: () async {
                  final value = await Navigator.pushNamed(
                      context, AppRoutes.profileEditPage,
                      arguments: state.userProfile);
                  if (value != null) {
                    UserProfile profile = value as UserProfile;
                    BlocProvider.of<ProfileBloc>(context)
                        .updateProfileData(profile);
                  }
                },
                child: SvgPicture.asset(
                  AppIcons.note,
                  color: AppColors.white,
                  height: 20,
                ),
              )
            ],
          ),
        );
      },
    );
  }
}

// Custom container widget for available options for a profile
class ProfileOptions extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ProfileBloc, ProfileState>(
      builder: (context, state) {
        return Column(
          children: [
            ProfileOption(
              icon: AppIcons.schedule,
              name: 'My Schedule',
              onTap: () {
                Navigator.pushNamed(context, AppRoutes.schedulePage,
                    arguments: state.userProfile);
              },
            ),
            Divider(color: AppColors.textFieldBackground),
            ProfileOption(
              icon: AppIcons.archive,
              name: 'Task archive',
              onTap: () {
                Navigator.pushNamed(
                  context,
                  AppRoutes.taskArchivePage,
                );
              },
            ),
            Divider(color: AppColors.textFieldBackground),
            ProfileOption(
              icon: state.userProfile!.checkedIn
                  ? AppIcons.checkIn
                  : AppIcons.checkOut,
              name: state.userProfile!.checkedIn ? 'Check out' : 'Check in',
              onTap: () {
                BlocProvider.of<ProfileBloc>(context).updateUserStatus();
              },
            ),
            Divider(color: AppColors.textFieldBackground),
            ProfileOption(
              icon: AppIcons.logOut,
              name: 'Log out',
              onTap: () {
                Navigator.pushNamedAndRemoveUntil(
                  context,
                  AppRoutes.authPage,
                  (route) => false,
                );
              },
            ),
            Divider(color: AppColors.textFieldBackground),
          ],
        );
      },
    );
  }
}

class ProfileOption extends StatelessWidget {
  final String icon;
  final String name;
  final VoidCallback onTap;
  ProfileOption({required this.icon, required this.name, required this.onTap});
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        onTap();
      },
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 24, vertical: 12),
        child: Row(
          children: [
            Container(
                height: 32,
                width: 32,
                padding: EdgeInsets.all(7),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(8),
                  color: AppColors.accent,
                ),
                child: SvgPicture.asset(
                  icon,
                  color: AppColors.white,
                )),
            SizedBox(width: 12),
            Expanded(child: Text16(name)),
            Icon(
              Icons.arrow_forward_ios_rounded,
              size: 20,
            ),
          ],
        ),
      ),
    );
  }
}

class MyBody extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Positioned(
      top: statusBarHeight(context) + 200,
      child: Container(
        height: screenHeight(context) - (statusBarHeight(context) + 60),
        width: MediaQuery.of(context).size.width,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(32),
          boxShadow: [
            BoxShadow(
              color: AppColors.accentTransparent.withOpacity(.5),
              blurRadius: 8,
              spreadRadius: 4,
              offset: Offset(0, 4),
            )
          ],
          color: AppColors.white,
        ),
        child: SingleChildScrollView(
          padding: EdgeInsets.symmetric(vertical: 32),
          child: Column(
            children: [
              ProfileOptions(),
            ],
          ),
        ),
      ),
    );
  }
}
