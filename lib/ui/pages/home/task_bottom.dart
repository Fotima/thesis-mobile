import 'package:flutter/material.dart';
import 'package:hospitalproject/models/ui/task_filter.dart';
import 'package:hospitalproject/ui/shared/bottom_sheet/default_bottom_sheet.dart';

// UI for modal sheet widget content
class StatusUpdateBottom extends StatelessWidget {
  final bool updateStatus;
  final int selectedStatus;
  StatusUpdateBottom({this.updateStatus = true, this.selectedStatus = 0});
  static Future show(BuildContext context,
      {bool updateStatus = true, int selectedStatus = 0}) async {
    return showModalBottomSheet(
      context: context,
      backgroundColor: Colors.transparent,
      builder: (context) {
        return StatusUpdateBottom(
          updateStatus: updateStatus,
          selectedStatus: selectedStatus,
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return DefaultBottomSheet(
      title: updateStatus ? 'Update task status' : 'Task status',
      children: [
        DefaultMaterialActionSheetAction(
          text: 'Assigned',
          selectedValue: selectedStatus,
          value: 0,
        ),
        DefaultMaterialActionSheetAction(
          text: 'In progress',
          selectedValue: selectedStatus,
          value: 1,
        ),
        DefaultMaterialActionSheetAction(
          text: 'Finished',
          selectedValue: selectedStatus,
          value: 2,
        ),
      ],
    );
  }
}

class FilterBottom extends StatelessWidget {
  final TaskFilter? taskFilter;
  final Function onSelect;
  FilterBottom({
    this.taskFilter,
    required this.onSelect,
  });
  static Future show(
      BuildContext context, TaskFilter? taskFilter, Function onSelect) async {
    final value = await showModalBottomSheet(
      context: context,
      backgroundColor: Colors.transparent,
      builder: (context) {
        return FilterBottom(
          taskFilter: taskFilter,
          onSelect: onSelect,
        );
      },
    );
    if (value != null) {
      if (value == 1) {
        final selected = await StatusUpdateBottom.show(context,
            updateStatus: false,
            selectedStatus: taskFilter == null ? 0 : taskFilter.value);
        if (selected != null) {
          onSelect(TaskFilter(type: 1, value: selected));
        }
      } else if (value == 2) {
        final selected = await FilterByPriority.show(
            context, taskFilter == null ? 0 : taskFilter.value);
        if (selected != null) {
          onSelect(TaskFilter(type: 2, value: selected));
        }
      } else {
        onSelect(TaskFilter(type: -1, value: -1));
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return DefaultBottomSheet(
      title: "Filter by",
      children: [
        DefaultMaterialActionSheetAction(
          text: 'Task Status',
          selectedValue: taskFilter == null ? 0 : taskFilter!.type,
          value: 1,
        ),
        DefaultMaterialActionSheetAction(
          text: 'Task Priority',
          selectedValue: taskFilter == null ? 0 : taskFilter!.type,
          value: 2,
        ),
        DefaultMaterialActionSheetAction(
          text: 'No Filter',
          selectedValue: taskFilter == null ? 0 : taskFilter!.type,
          value: -1,
        ),
      ],
    );
  }
}

class FilterByPriority extends StatelessWidget {
  final int selectedValue;

  FilterByPriority({this.selectedValue = -1});

  static Future show(
    BuildContext context,
    int selectedValue,
  ) async {
    return showModalBottomSheet(
      context: context,
      backgroundColor: Colors.transparent,
      builder: (context) {
        return FilterByPriority(
          selectedValue: selectedValue,
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return DefaultBottomSheet(
      title: 'Task priority',
      children: [
        DefaultMaterialActionSheetAction(
          text: 'High',
          selectedValue: selectedValue,
          value: 0,
        ),
        DefaultMaterialActionSheetAction(
          text: 'Normal',
          selectedValue: selectedValue,
          value: 1,
        ),
        DefaultMaterialActionSheetAction(
          text: 'Low',
          selectedValue: selectedValue,
          value: 2,
        ),
      ],
    );
  }
}
