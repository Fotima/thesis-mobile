import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:hospitalproject/blocs/home/home_bloc.dart';
import 'package:hospitalproject/constants/api/api_contants.dart';
import 'package:hospitalproject/constants/assets/icon_constants.dart';
import 'package:hospitalproject/constants/values/color_constants.dart';
import 'package:hospitalproject/constants/values/route_constants.dart';
import 'package:hospitalproject/models/api/auth/auth.dart';
import 'package:hospitalproject/utils/screen_util.dart';
import 'package:hospitalproject/ui/shared/text14.dart';
import 'package:hospitalproject/ui/shared/text16.dart';
import 'package:hospitalproject/ui/shared/top_message.dart';
import 'package:hospitalproject/utils/api_utils.dart';
import 'package:hospitalproject/models/api/task/task.dart';
import 'package:hospitalproject/models/ui/task_filter.dart';
import 'package:hospitalproject/ui/pages/home/task_bottom.dart';
import 'package:hospitalproject/ui/shared/app_text.dart';
import 'package:hospitalproject/utils/shared_pref/shared_pref_helper.dart';

import 'no_data_container.dart';

// UI for tasks tab
class TasksTab extends StatefulWidget {
  final LoginResponse loginResponse;
  TasksTab({required this.loginResponse});

  @override
  _TasksTabState createState() => _TasksTabState();
}

class _TasksTabState extends State<TasksTab> {
  List<String> permissions = [];
  @override
  void initState() {
    permissions = SharedPrefHelper.getPermissions();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<HomeBloc, HomeState>(
      builder: (context, state) {
        return Container(
          height: screenHeight(context),
          child: Stack(
            children: [
              HomeBody(),
              MyAppBar(
                loginResponse: widget.loginResponse,
                permissions: permissions,
              ),
              state.onTopLoading
                  ? Container(
                      height: screenHeight(context),
                      width: screenWidth(context),
                      color: AppColors.black.withOpacity(.3),
                      child: Center(
                        child: CircularProgressIndicator(),
                      ),
                    )
                  : Container()
            ],
          ),
        );
      },
    );
  }
}

// Custom widget for body for tasks tab
class HomeBody extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<HomeBloc, HomeState>(
      builder: (context, state) {
        return Positioned(
          top: statusBarHeight(context) + 120,
          child: Container(
            padding: EdgeInsets.only(top: 3),
            height: screenHeight(context) - (statusBarHeight(context) + 154),
            width: MediaQuery.of(context).size.width,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(32),
              boxShadow: [
                BoxShadow(
                  color: AppColors.accentTransparent.withOpacity(.5),
                  blurRadius: 8,
                  spreadRadius: 4,
                  offset: Offset(0, 4),
                )
              ],
              // color: AppColors.white,
              color: AppColors.accentTransparent,
            ),
            child: state.loading
                ? Center(
                    child: CircularProgressIndicator(),
                  )
                : state.mytasks?.length == 0
                    ? NoDataContainer()
                    : RefreshIndicator(
                        backgroundColor: Colors.white,
                        onRefresh: () async {
                          BlocProvider.of<HomeBloc>(context).loadTasks();
                          await Future.delayed(Duration(seconds: 1));
                        },
                        child: ListView.builder(
                          physics: AlwaysScrollableScrollPhysics(),
                          padding: EdgeInsets.only(
                              left: 20, right: 20, top: 24, bottom: 48),
                          itemCount: state.mytasks!.length,
                          itemBuilder: (context, index) {
                            return TaskCard(
                              taskInfo: state.mytasks![index],
                            );
                          },
                        ),
                      ),
          ),
        );
      },
    );
  }
}

// Customw widget container for task item data
class TaskCard extends StatelessWidget {
  final TaskInfo taskInfo;
  final bool showMoreOption;
  TaskCard({required this.taskInfo, this.showMoreOption = true});
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(bottom: 32),
      padding: EdgeInsets.only(left: 20, right: 20, bottom: 20),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(8),
        color: AppColors.white,
        boxShadow: [
          BoxShadow(
            color: AppColors.accentTransparent,
            blurRadius: 4,
            spreadRadius: 0,
            offset: Offset(0, 1),
          ),
        ],
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          TaskStatusLine(
            taskStatus: taskInfo.status,
          ),
          SizedBox(height: 22),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SvgPicture.asset(AppIcons.note),
                  SizedBox(width: 6),
                  Text14(ApiUtils.getFormattedDate(taskInfo.assignedDate),
                      color: AppColors.textColor),
                ],
              ),
              showMoreOption
                  ? PopupMenuButton(
                      itemBuilder: (context) {
                        return [
                          PopupMenuItem(
                            child: Text16('Update status'),
                            value: 1,
                          ),
                        ];
                      },
                      color: Colors.white,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(8)),
                      padding: EdgeInsets.all(0),
                      iconSize: 24,
                      child: Icon(
                        Icons.more_horiz,
                        color: AppColors.fontColor,
                      ),
                      offset: Offset(0, -20),
                      onSelected: (value) async {
                        final value = await StatusUpdateBottom.show(context,
                            selectedStatus: taskInfo.status);
                        if (value != null) {
                          if (value < taskInfo.status) {
                            showMessage(
                                "Task cannot be updated to selected status",
                                alert: true);
                          } else {
                            BlocProvider.of<HomeBloc>(context).updateTaskStatus(
                                TaskStatusPost(
                                    status: value, taskId: taskInfo.id));
                          }
                        }
                      },
                    )
                  : Container(),
            ],
          ),
          SizedBox(height: 4),
          Text14(
              'By ${taskInfo.creatorUser.firstName} ${taskInfo.creatorUser.lastName}',
              color: AppColors.textColor),
          SizedBox(height: 18),
          AppText(
            taskInfo.title,
            fontSize: 22,
            fontWeight: FontWeight.w500,
          ),
          SizedBox(height: 2),
          Text16(
            taskInfo.description,
            color: AppColors.textColor,
            lineHeight: 1.2,
          ),
          SizedBox(height: 6),
          Row(
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              Icon(
                Icons.location_on,
                color: AppColors.accent,
                size: 20,
              ),
              SizedBox(width: 6),
              Text16(
                taskInfo.taskPlacement,
                color: AppColors.accent,
              )
            ],
          ),
          SizedBox(height: 16),
          Divider(color: AppColors.fontColor.withOpacity(.4)),
          SizedBox(height: 16),
          Row(
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              Icon(
                Icons.access_time_rounded,
                size: 20,
              ),
              SizedBox(width: 6),
              Text16(
                ApiUtils.getTaskTime(taskInfo.taskStart, taskInfo.taskEnd),
                color: AppColors.textColor,
              )
            ],
          ),
          SizedBox(height: 10),
          Row(
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              Text16(
                'Priority:',
                color: AppColors.fontColor,
                fontWeight: FontWeight.w600,
              ),
              SizedBox(width: 6),
              Text16(
                ApiUtils.getPriorityString(taskInfo.priority),
                color: ApiUtils.getStatusColor(taskInfo.priority),
              )
            ],
          ),
          SizedBox(height: 8),
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text16(
                'Assigned to:',
                color: AppColors.fontColor,
                fontWeight: FontWeight.w600,
              ),
              SizedBox(width: 6),
              Expanded(
                child: Text16(
                  taskInfo.assignedRole == null
                      ? taskInfo.assignedUserNames!.join(', ')
                      : "${taskInfo.assignedRole!.name} role",
                ),
              )
            ],
          ),
          taskInfo.lastModifiedBy != null
              ? Column(
                  children: [
                    SizedBox(height: 12),
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text14(
                          'Last modified by:',
                          color: AppColors.fontColor,
                        ),
                        SizedBox(width: 6),
                        Expanded(
                          child: Text14(
                            '${taskInfo.lastModifiedBy!.firstName} ${taskInfo.lastModifiedBy!.lastName}',
                            color: AppColors.fontColor,
                            fontWeight: FontWeight.w500,
                          ),
                        )
                      ],
                    ),
                  ],
                )
              : Container(),
          taskInfo.lastModifiedDate != null
              ? Column(
                  children: [
                    SizedBox(height: 8),
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text14(
                          'Last modified at:',
                          color: AppColors.fontColor,
                        ),
                        SizedBox(width: 6),
                        Expanded(
                          child: Text14(
                            ApiUtils.getFormattedDateTime(
                                taskInfo.lastModifiedDate!),
                            color: AppColors.fontColor,
                            fontWeight: FontWeight.w500,
                          ),
                        )
                      ],
                    ),
                  ],
                )
              : Container()
        ],
      ),
    );
  }
}
// Custom UI to illustrate the progress status of the task

class TaskStatusLine extends StatelessWidget {
  final int taskStatus;

  TaskStatusLine({required this.taskStatus});

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 4,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(2),
        color: ApiUtils.getStatusColor(taskStatus),
      ),
    );
  }
}

// Custom appbar for tasks bar screen
class MyAppBar extends StatelessWidget {
  final LoginResponse loginResponse;
  final List<String> permissions;

  MyAppBar({
    required this.loginResponse,
    required this.permissions,
  });

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<HomeBloc, HomeState>(
      builder: (context, state) {
        return Container(
          padding: EdgeInsets.only(
              top: statusBarHeight(context), right: 24, left: 24),
          height: statusBarHeight(context) + 124,
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  AppText(
                    "Hello ${loginResponse.user.firstName}!",
                    color: AppColors.accentDark,
                    fontWeight: FontWeight.w700,
                    textAlign: TextAlign.left,
                    fontSize: 26,
                  ),
                  SizedBox(height: 4),
                  Text16(
                    BlocProvider.of<HomeBloc>(context).getTodayDate(),
                    color: AppColors.fontColor,
                  ),
                ],
              ),
              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Row(
                    children: [
                      GestureDetector(
                        onTap: () {
                          BlocProvider.of<HomeBloc>(context).loadTasks();
                        },
                        child: Container(
                          height: 38,
                          width: 38,
                          decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            color: AppColors.accentTransparent,
                          ),
                          child: Icon(
                            Icons.refresh_rounded,
                            color: AppColors.accentAlt,
                            size: 22,
                          ),
                        ),
                      ),
                      SizedBox(width: 8),
                      GestureDetector(
                        onTap: () {
                          if (state.mytasks!.isNotEmpty) {
                            FilterBottom.show(context, state.taskFilter,
                                (TaskFilter? value) {
                              if (value != null) {
                                BlocProvider.of<HomeBloc>(context)
                                    .updateFilter(value);
                              }
                            });
                          }
                        },
                        child: Container(
                          height: 38,
                          width: 38,
                          decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            color: AppColors.accentTransparent,
                          ),
                          child: Icon(
                            Icons.menu_open_sharp,
                            color: AppColors.accent,
                            size: 22,
                          ),
                        ),
                      ),
                    ],
                  ),
                  loginResponse.hasLowerRoles &&
                          permissions.contains(ApiConstants.createTask)
                      ? GestureDetector(
                          onTap: () {
                            Navigator.pushNamed(context, AppRoutes.newTaskPage);
                          },
                          child: Container(
                            padding: EdgeInsets.symmetric(
                                horizontal: 12, vertical: 6),
                            margin: EdgeInsets.only(top: 6),
                            child: Text16(
                              'Create task',
                              color: AppColors.accent,
                            ),
                          ),
                        )
                      : Container(),
                ],
              ),
            ],
          ),
        );
      },
    );
  }
}
