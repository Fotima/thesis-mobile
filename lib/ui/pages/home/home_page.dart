import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/svg.dart';
import 'package:hospitalproject/blocs/home/home_bloc.dart';
import 'package:hospitalproject/constants/api/api_contants.dart';
import 'package:hospitalproject/constants/assets/icon_constants.dart';
import 'package:hospitalproject/constants/values/color_constants.dart';
import 'package:hospitalproject/models/api/auth/auth.dart';
import 'package:hospitalproject/ui/pages/home/tasks_tab.dart';
import 'package:hospitalproject/ui/shared/text12.dart';
import 'package:hospitalproject/utils/screen_util.dart';
import 'package:hospitalproject/utils/shared_pref/shared_pref_helper.dart';

import 'announcement_tab/announcement_tab.dart';
import 'chat_tab/chat_tab.dart';
import 'contacts_tab/contacts_tab.dart';
import 'profile_tab/profile_tab.dart';

// UI view for Profile tab screen
// hange background message from firebase service
Future<void> hanldeBakckgrounMessage(RemoteMessage message) async {
  print('background message ${message.notification!.body}');
}

class HomePage extends StatefulWidget {
  final LoginResponse loginResponse;

  HomePage({required this.loginResponse});
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  late FirebaseMessaging firebaseMessaging;
  late HomeBloc bloc;
  List<String> permissions = [];
  var tabs = [];
  @override
  void initState() {
    bloc = HomeBloc();
    firebaseMessaging = FirebaseMessaging.instance;
    // get list of permissions
    permissions = SharedPrefHelper.getPermissions();
    initTab(permissions);
    print('requested token');
    // get the firebase token, if it is null or updated, push a new token to the seerver
    firebaseMessaging.getToken().then((value) {
      print(value);

      bloc.updateFibaseToken(value);
    });
    checkForInitMessage();
    // Configure notification seettings on the devices
    configureNotifications();
    // when the UI is loaded, load list of tasks from the server
    WidgetsBinding.instance!.addPostFrameCallback((timeStamp) {
      bloc.loadTasks();
    });

    super.initState();
  }

  checkForInitMessage() async {
    RemoteMessage? initialMessage =
        await FirebaseMessaging.instance.getInitialMessage();
    if (initialMessage != null) {
      RemoteNotification notification = RemoteNotification(
        title: initialMessage.notification?.title,
        body: initialMessage.notification?.body,
      );
      bloc.handleNotificationGet(initialMessage.data, notification, true);
    }
  }
  // configure firebase notification

  Future<void> configureNotifications() async {
    FirebaseMessaging.onBackgroundMessage(hanldeBakckgrounMessage);

    NotificationSettings notificationSettings =
        await firebaseMessaging.requestPermission(
      alert: true,
      badge: true,
      announcement: true,
      carPlay: true,
      criticalAlert: true,
    );
    if (notificationSettings.authorizationStatus ==
        AuthorizationStatus.authorized) {
      print('User granted permission');
      registerNotificationHandling();
    } else {
      print('User declined or has not accepted permission');
    }
    return;
  }
  // register notification listener on application

  void registerNotificationHandling() {
    FirebaseMessaging.onMessage.listen((RemoteMessage message) {
      print("message recieved");
      print(message.notification!.body);
      bloc.handleNotificationGet(message.data, message.notification, true);
    });
    FirebaseMessaging.onMessageOpenedApp.listen((RemoteMessage message) {
      print('Message clicked!');
    });
  }

  initTab(List<String> permissions) {
    tabs = [
      Container(),
      AnnouncementTab(),
      // ContactsTab(),
      // ChatTab(),
      // ProfileTab(),
    ];
    if (permissions.contains(ApiConstants.callAllUsers) ||
        permissions.contains(ApiConstants.callUsersLowerRank)) {
      tabs.add(ContactsTab());
    } else {
      tabs.add(Container());
    }
    if (permissions.contains(ApiConstants.messageAllUsers) ||
        permissions.contains(ApiConstants.messageLowerRankUsers)) {
      tabs.add(ChatTab());
    } else {
      tabs.add(Container());
    }
    tabs.add(ProfileTab());
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => bloc,
      child: BlocBuilder<HomeBloc, HomeState>(
        builder: (context, state) {
          return Scaffold(
            body: AnimatedSwitcher(
              duration: Duration(milliseconds: 200),
              child: state.selectedTab == 0
                  ? TasksTab(
                      loginResponse: widget.loginResponse,
                    )
                  : tabs[state.selectedTab],
            ),
            bottomNavigationBar: Container(
                child: BottomNavigationBar(
                    paddingBottomHeight(context), permissions)),
          );
        },
      ),
    );
  }
}

// Custom widget for bottom navigation
class BottomNavigationBar extends StatelessWidget {
  final double statusbarHeight;
  final List<String> permissions;
  BottomNavigationBar(this.statusbarHeight, this.permissions);
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<HomeBloc, HomeState>(
      builder: (context, state) {
        return Container(
          color: state.selectedTab == 0
              ? AppColors.accentTransparent
              : Colors.white,
          child: Container(
            height: 56 + statusbarHeight,
            padding: EdgeInsets.only(
                left: 24, right: 24, top: 8, bottom: 8 + statusbarHeight),
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.vertical(
                top: Radius.circular(32),
              ),
            ),
            child: Row(
              children: [
                NavigationItem(
                  icon: AppIcons.checklistalt,
                  title: 'Tasks',
                  index: 0,
                  selected: state.selectedTab == 0,
                ),
                NavigationItem(
                  icon: AppIcons.news,
                  title: 'News board',
                  index: 1,
                  selected: state.selectedTab == 1,
                  iconSize: 18,
                ),
                permissions.contains(ApiConstants.callAllUsers) ||
                        permissions.contains(ApiConstants.callUsersLowerRank)
                    ? NavigationItem(
                        icon: AppIcons.contacts,
                        title: 'Contacts',
                        index: 2,
                        selected: state.selectedTab == 2,
                      )
                    : Container(),
                permissions.contains(ApiConstants.messageAllUsers) ||
                        permissions.contains(ApiConstants.messageLowerRankUsers)
                    ? NavigationItem(
                        icon: AppIcons.chat,
                        title: 'Chat',
                        index: 3,
                        selected: state.selectedTab == 3,
                      )
                    : Container(),
                NavigationItem(
                  icon: AppIcons.user,
                  title: 'Profile',
                  index: 4,
                  selected: state.selectedTab == 4,
                ),
              ],
            ),
          ),
        );
      },
    );
  }
}

// Custom widget for navigation item
class NavigationItem extends StatelessWidget {
  final String title;
  final String icon;
  final int index;
  final bool selected;
  final double? iconSize;
  NavigationItem(
      {required this.icon,
      required this.title,
      this.selected = false,
      this.iconSize,
      this.index = 0});
  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: GestureDetector(
        onTap: () {
          BlocProvider.of<HomeBloc>(context).updateTab(index);
        },
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            SvgPicture.asset(
              icon,
              width: iconSize ?? 24,
              color: selected ? AppColors.accent : AppColors.fontColor,
            ),
            Row(
              children: [
                Expanded(
                  child: Text12(
                    title,
                    color: selected ? AppColors.accent : AppColors.fontColor,
                    textAlign: TextAlign.center,
                  ),
                )
              ],
            )
          ],
        ),
      ),
    );
  }
}
