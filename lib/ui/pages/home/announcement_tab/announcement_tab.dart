import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:hospitalproject/blocs/announcement/announcement_bloc.dart';
import 'package:hospitalproject/constants/api/api_contants.dart';
import 'package:hospitalproject/constants/values/color_constants.dart';
import 'package:hospitalproject/constants/values/route_constants.dart';
import 'package:hospitalproject/models/announcement/announcement.dart';
import 'package:hospitalproject/ui/pages/home/no_data_container.dart';
import 'package:hospitalproject/ui/shared/app_text.dart';
import 'package:hospitalproject/ui/shared/text14.dart';
import 'package:hospitalproject/ui/shared/text16.dart';
import 'package:hospitalproject/ui/shared/text18.dart';
import 'package:hospitalproject/utils/api_utils.dart';
import 'package:hospitalproject/utils/screen_util.dart';
import 'package:hospitalproject/utils/shared_pref/shared_pref_helper.dart';

// UI view for Announcement tab screen
class AnnouncementTab extends StatefulWidget {
  @override
  _AnnouncementTabState createState() => _AnnouncementTabState();
}

class _AnnouncementTabState extends State<AnnouncementTab> {
  late AnnouncementBloc bloc;

  List<String>? permissions = [];
  @override
  void initState() {
    bloc = AnnouncementBloc();
    permissions = SharedPrefHelper.getPermissions();
    // When the UI of the screen is built, call load all announcement function at BloC clas
    WidgetsBinding.instance!.addPostFrameCallback((timeStamp) {
      bloc.loadAllAnnouncements();
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => bloc,
      child: Container(
        height: screenHeight(context),
        width: screenWidth(context),
        child: Stack(
          children: [
            MyBody(),
            MayAppBar(
              permissions: permissions,
            ),
          ],
        ),
      ),
    );
  }
}

// Custom widget for the body of announcement tab
class MyBody extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<AnnouncementBloc, AnnouncementState>(
      builder: (context, state) {
        return Positioned(
          top: statusBarHeight(context) + 90,
          child: Container(
            height: screenHeight(context) - (statusBarHeight(context) + 80),
            width: screenWidth(context),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(32),
              boxShadow: [
                BoxShadow(
                  color: AppColors.accentTransparent.withOpacity(.5),
                  blurRadius: 8,
                  spreadRadius: 4,
                  offset: Offset(0, 4),
                )
              ],
              color: AppColors.accentTransparent,
            ),
            child: state.loading
                ? Center(
                    child: CircularProgressIndicator(),
                  )
                : RefreshIndicator(
                    backgroundColor: Colors.white,
                    onRefresh: () async {
                      BlocProvider.of<AnnouncementBloc>(context)
                          .loadAllAnnouncements();
                      await Future.delayed(Duration(seconds: 1));
                    },
                    child: state.announcements!.isEmpty
                        ? Center(
                            child: NoDataContainer(),
                          )
                        : StaggeredGridView.countBuilder(
                            padding: EdgeInsets.only(
                                left: 16, right: 16, top: 24, bottom: 104),
                            crossAxisCount: 4,
                            itemCount: state.announcements!.length,
                            itemBuilder: (BuildContext context, int index) =>
                                NewsItem(
                              info: state.announcements![index],
                            ),
                            staggeredTileBuilder: (int index) =>
                                StaggeredTile.fit(2),
                            mainAxisSpacing: 12,
                            crossAxisSpacing: 12,
                          ),
                  ),
          ),
        );
      },
    );
  }
}

// Custom widget for news container of announcement tab
class NewsItem extends StatelessWidget {
  final AnnouncementInfo info;
  NewsItem({required this.info});
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(
        horizontal: 16,
        vertical: 18,
      ),
      decoration: BoxDecoration(
        color: AppColors.white,
        borderRadius: BorderRadius.circular(12),
        boxShadow: [
          BoxShadow(
            color: AppColors.accentTransparent,
            spreadRadius: 2,
            blurRadius: 2,
            offset: Offset(0, 1),
          ),
        ],
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: [
          Text18(
            info.title ?? '',
            fontWeight: FontWeight.w600,
            color: Colors.black,
          ),
          SizedBox(height: 8),
          Text16(
            info.description ?? '',
          ),
          SizedBox(height: 8),
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            children: [
              Icon(
                Icons.person,
                size: 18,
                color: AppColors.fontColor,
              ),
              SizedBox(width: 4),
              Expanded(
                child: Text14(
                  'Fotima Khayrullaeva',
                  color: AppColors.fontColor,
                ),
              )
            ],
          ),
          SizedBox(height: 4),
          Row(
            mainAxisSize: MainAxisSize.min,
            children: [
              Icon(
                Icons.access_time_rounded,
                size: 18,
                color: AppColors.fontColor,
              ),
              SizedBox(width: 4),
              Text14(
                ApiUtils.getFormattedDate(info.date ?? 0),
                color: AppColors.fontColor,
              )
            ],
          )
        ],
      ),
    );
  }
}

// Custom appbar widget for announcement tab
class MayAppBar extends StatelessWidget {
  final List<String>? permissions;
  MayAppBar({this.permissions});
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(
          top: statusBarHeight(context) + 20, right: 24, left: 24),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          AppText(
            "News board",
            fontSize: 28,
            fontWeight: FontWeight.bold,
          ),
          permissions == null
              ? Container()
              : permissions!.contains(ApiConstants.createAnnouncement)
                  ? IconButton(
                      icon: Icon(
                        Icons.add,
                        size: 30,
                        color: AppColors.accent,
                      ),
                      onPressed: () async {
                        final value = await Navigator.pushNamed(
                            context, AppRoutes.newAnnouncementPage);
                        if (value != null) {
                          BlocProvider.of<AnnouncementBloc>(context)
                              .loadAllAnnouncements();
                        }
                      },
                    )
                  : Container()
        ],
      ),
    );
  }
}
