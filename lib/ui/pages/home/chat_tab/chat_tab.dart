import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/svg.dart';
import 'package:hospitalproject/blocs/chat_tab/chat_tab_bloc.dart';
import 'package:hospitalproject/constants/assets/icon_constants.dart';
import 'package:hospitalproject/constants/values/color_constants.dart';
import 'package:hospitalproject/models/api/chat_room/chat_room.dart';
import 'package:hospitalproject/ui/pages/chat_page/chat_page.dart';
import 'package:hospitalproject/ui/shared/app_text.dart';
import 'package:hospitalproject/ui/shared/text14.dart';
import 'package:hospitalproject/ui/shared/text16.dart';
import 'package:hospitalproject/utils/api_utils.dart';
import 'package:hospitalproject/utils/screen_util.dart';

// UI view for Chat tab screen

class ChatTab extends StatefulWidget {
  @override
  _ChatTabState createState() => _ChatTabState();
}

class _ChatTabState extends State<ChatTab> {
  late ChatTabBloc bloc;
  @override
  void initState() {
    bloc = ChatTabBloc();
    // When the UI of the screen is build, call load my chats function from BloC object
    WidgetsBinding.instance!.addPostFrameCallback((timeStamp) {
      bloc.loadMyChats();
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => bloc,
      child: Container(
        height: screenHeight(context),
        width: screenWidth(context),
        child: Stack(
          children: [
            MyBody(),
            MayAppBar(),
          ],
        ),
      ),
    );
  }
}

// Custom body widget for chat tab
class MyBody extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ChatTabBloc, ChatTabState>(
      builder: (context, state) {
        return Positioned(
          top: statusBarHeight(context) + 90,
          child: Container(
            height: screenHeight(context) - (statusBarHeight(context) + 80),
            width: screenWidth(context),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(32),
              boxShadow: [
                BoxShadow(
                  color: AppColors.accentTransparent.withOpacity(.5),
                  blurRadius: 8,
                  spreadRadius: 4,
                  offset: Offset(0, 4),
                )
              ],
              color: AppColors.accentTransparent,
            ),
            child: state.loading
                ? Center(
                    child: CircularProgressIndicator(),
                  )
                : state.mychats!.length == 0
                    ? Padding(
                        padding: EdgeInsets.symmetric(horizontal: 24),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            SvgPicture.asset(
                              AppIcons.noData,
                              width: screenHeight(context) / 3,
                            ),
                            SizedBox(height: 12),
                            Text14(
                              'No data',
                              color: AppColors.fontColor,
                            )
                          ],
                        ),
                      )
                    : ListView.builder(
                        itemCount: state.mychats!.length,
                        padding: EdgeInsets.only(top: 16, right: 16, left: 16),
                        itemBuilder: (context, index) {
                          return ChatItem(
                            chatRoom: state.mychats![index],
                          );
                        },
                      ),
          ),
        );
      },
    );
  }
}

// Custom container widget for chat item of chat tab
class ChatItem extends StatelessWidget {
  final ChatRoom chatRoom;
  ChatItem({required this.chatRoom});
  final colors = [
    AppColors.green.withOpacity(.2),
    AppColors.accentAlt.withOpacity(.2),
    AppColors.red.withOpacity(.2),
    AppColors.orange.withOpacity(.2)
  ];
  final fontColors = [
    AppColors.green,
    AppColors.accentAlt,
    AppColors.red,
    AppColors.orange,
  ];
  @override
  Widget build(BuildContext context) {
    final int colorIndex = Random().nextInt(3);

    return GestureDetector(
      onTap: () {
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => ChatPage(
                      withUser: chatRoom.listOfUser![0],
                      chatId: chatRoom.chatId,
                    )));
      },
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 24, vertical: 18),
        margin: EdgeInsets.only(bottom: 16),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(16),
          color: Colors.white,
          boxShadow: [
            BoxShadow(
              color: AppColors.accentTransparent,
              spreadRadius: 2,
              blurRadius: 2,
              offset: Offset(0, 1),
            ),
          ],
        ),
        child: Row(
          children: [
            Container(
              height: 44,
              width: 44,
              decoration: BoxDecoration(
                shape: BoxShape.circle,
                color: colors[colorIndex],
              ),
              child: Center(
                child: Text14(
                  '${chatRoom.listOfUser![0].firstName.substring(0, 1).toUpperCase()}${chatRoom.listOfUser![0].lastName.substring(0, 1).toUpperCase()}',
                  color: fontColors[colorIndex],
                ),
              ),
            ),
            SizedBox(width: 12),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text16(
                    '${chatRoom.listOfUser![0].firstName} ${chatRoom.listOfUser![0].lastName}',
                    fontWeight: FontWeight.w500,
                  ),
                  SizedBox(height: 2),
                  Text14(
                    chatRoom.listOfUser![0].role!.name,
                    color: AppColors.textColor,
                    fontWeight: FontWeight.w500,
                  ),
                  chatRoom.lastMessage == null
                      ? Container()
                      : SizedBox(height: 2),
                  chatRoom.lastMessage == null
                      ? Container()
                      : Text14(
                          chatRoom.lastMessage ?? '',
                          color: AppColors.fontColor,
                        ),
                ],
              ),
            ),
            SizedBox(width: 12),
            Text14(
              ApiUtils.getTime(chatRoom.lastMessageTime!),
              color: AppColors.fontColor,
            )
          ],
        ),
      ),
    );
  }
}

// Custom appbar for chat tab
class MayAppBar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(
          top: statusBarHeight(context) + 20, right: 24, left: 24),
      child: AppText(
        "Chat",
        fontSize: 28,
        fontWeight: FontWeight.bold,
      ),
    );
  }
}
