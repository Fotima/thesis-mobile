import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/svg.dart';
import 'package:hospitalproject/blocs/contacts_tab/contacts_tab_bloc.dart';
import 'package:hospitalproject/constants/api/api_contants.dart';
import 'package:hospitalproject/constants/assets/icon_constants.dart';
import 'package:hospitalproject/constants/values/color_constants.dart';
import 'package:hospitalproject/models/api/user/user.dart';
import 'package:hospitalproject/models/api/user_short/user_short.dart';
import 'package:hospitalproject/ui/pages/chat_page/chat_page.dart';
import 'package:hospitalproject/ui/pages/home/no_data_container.dart';
import 'package:hospitalproject/ui/shared/bottom_sheet/default_bottom_sheet.dart';
import 'package:hospitalproject/ui/shared/text14.dart';
import 'package:hospitalproject/ui/shared/text16.dart';
import 'package:hospitalproject/utils/screen_util.dart';
import 'package:hospitalproject/utils/shared_pref/shared_pref_helper.dart';
// custom widget for list contacts on contacts tab screen

class ContactsList extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ContactsBloc, ContactsState>(
      builder: (context, state) {
        return state.contacts!.length == 0
            ? NoDataContainer()
            : RefreshIndicator(
                backgroundColor: Colors.white,
                onRefresh: () async {
                  // request list of contacts from the server and update UI
                  BlocProvider.of<ContactsBloc>(context).updateContacts();
                  await Future.delayed(Duration(seconds: 1));
                },
                child: ListView.builder(
                  padding: EdgeInsets.only(
                      left: 16,
                      right: 16,
                      top: 24,
                      bottom: 90 + paddingBottomHeight(context)),
                  itemCount: state.contacts!.length,
                  itemBuilder: (context, index) {
                    return RoleGroup(
                      roleGroup: state.contacts![index],
                      permissions: state.permissions!,
                    );
                  },
                ),
              );
      },
    );
  }
}

// Custom widget to show the grouped users by role for contacts tab
class RoleGroup extends StatelessWidget {
  final UserByRole roleGroup;
  final List<String> permissions;

  RoleGroup({
    required this.roleGroup,
    required this.permissions,
  });
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text16(
            roleGroup.role,
            fontWeight: FontWeight.w500,
            color: AppColors.accentDark,
          ),
          SizedBox(height: 8),
          ListView.builder(
            shrinkWrap: true,
            primary: false,
            padding: EdgeInsets.all(0),
            itemCount: roleGroup.users.length,
            itemBuilder: (context, index) {
              return ContactItem(
                item: roleGroup.users[index],
                permissions: permissions,
              );
            },
          ),
        ],
      ),
    );
  }
}

// Custom widget container for contacts item from list of contacts
class ContactItem extends StatelessWidget {
  final UserShort item;
  final List<String> permissions;
  ContactItem({
    required this.item,
    required this.permissions,
  });
  final colors = [
    AppColors.green.withOpacity(.2),
    AppColors.accentAlt.withOpacity(.2),
    AppColors.red.withOpacity(.2),
    AppColors.orange.withOpacity(.2)
  ];
  final fontColors = [
    AppColors.green,
    AppColors.accentAlt,
    AppColors.red,
    AppColors.orange,
  ];
  bool showCall() {
    if (permissions.contains(ApiConstants.callAllUsers)) {
      return true;
    } else if (permissions.contains(ApiConstants.callUsersLowerRank)) {
      int? myRank = SharedPrefHelper.getRoleRank();
      if (myRank != null && item.role!.rank != null) {
        if (myRank < item.role!.rank!) {
          return true;
        } else {
          return false;
        }
      }
      return false;
    } else {
      return false;
    }
  }

  bool showMessage() {
    if (permissions.contains(ApiConstants.messageAllUsers)) {
      return true;
    } else if (permissions.contains(ApiConstants.messageLowerRankUsers)) {
      int? myRank = SharedPrefHelper.getRoleRank();
      if (myRank != null && item.role!.rank != null) {
        if (myRank < item.role!.rank!) {
          return true;
        } else {
          return false;
        }
      }
      return false;
    } else {
      return false;
    }
  }

  List<DefaultMaterialActionSheetAction> getBottomItems() {
    List<DefaultMaterialActionSheetAction> list = [];
    if (showCall()) {
      if (item.phoneNumber != null) {
        list.add(DefaultMaterialActionSheetAction(
          text: "Call",
          value: 1,
          fontColor: AppColors.black,
          leadingIcon: SvgPicture.asset(
            AppIcons.call,
            color: AppColors.accent,
          ),
        ));
      }
    }
    if (showMessage()) {
      list.add(DefaultMaterialActionSheetAction(
        text: "Send Message",
        value: 2,
        fontColor: AppColors.black,
        leadingIcon: SvgPicture.asset(
          AppIcons.chat,
          color: AppColors.accent,
        ),
      ));
    }
    return list;
  }

  @override
  Widget build(BuildContext context) {
    final int colorIndex = Random().nextInt(3);
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 16, vertical: 16),
      margin: EdgeInsets.only(bottom: 16),
      decoration: BoxDecoration(
        color: AppColors.white,
        borderRadius: BorderRadius.circular(12),
      ),
      child: Row(
        children: [
          Container(
            height: 40,
            width: 40,
            decoration: BoxDecoration(
              shape: BoxShape.circle,
              color: colors[colorIndex],
            ),
            child: Center(
                child: Text14(
              '${item.firstName.substring(0, 1).toUpperCase()}${item.lastName.substring(0, 1).toUpperCase()}',
              color: fontColors[colorIndex],
            )),
          ),
          SizedBox(width: 12),
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text16('${item.firstName} ${item.lastName}'),
                SizedBox(height: 2),
                Text14(item.role == null ? '' : item.role!.name,
                    color: AppColors.fontColor),
              ],
            ),
          ),
          SizedBox(width: 12),
          IconButton(
            icon: Icon(Icons.more_vert_rounded),
            onPressed: () async {
              final value = await showModalBottomSheet(
                context: context,
                builder: (context) {
                  return DefaultBottomSheet(
                    children: getBottomItems(),
                  );
                },
              );
              if (value != null) {
                if (value == 1) {
                  BlocProvider.of<ContactsBloc>(context)
                      .createCallHistory(item.id, item.phoneNumber!);
                } else {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => ChatPage(
                        withUserId: item.id,
                        withUser: item,
                      ),
                    ),
                  );
                }
              }
            },
          )
        ],
      ),
    );
  }
}
