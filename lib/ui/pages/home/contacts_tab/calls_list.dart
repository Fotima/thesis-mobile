import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:hospitalproject/blocs/contacts_tab/contacts_tab_bloc.dart';
import 'package:hospitalproject/constants/values/color_constants.dart';
import 'package:hospitalproject/models/api/calls/calls.dart';
import 'package:hospitalproject/ui/shared/default_button.dart';
import 'package:hospitalproject/ui/shared/default_text_field.dart';
import 'package:hospitalproject/ui/shared/text14.dart';
import 'package:hospitalproject/ui/shared/text16.dart';
import 'package:hospitalproject/ui/shared/text18.dart';
import 'package:hospitalproject/ui/shared/text20.dart';
import 'package:hospitalproject/utils/api_utils.dart';
import 'package:hospitalproject/utils/screen_util.dart';
import 'package:flutter_animated_dialog/flutter_animated_dialog.dart';

import '../no_data_container.dart';

// custom widget for list of calls of contacts tab screen
class CallsList extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ContactsBloc, ContactsState>(
      builder: (context, state) {
        return state.callHistory!.length == 0
            ? NoDataContainer()
            : Stack(
                children: [
                  ListView.builder(
                    padding: EdgeInsets.only(
                        left: 16,
                        right: 16,
                        top: 24,
                        bottom: 90 + paddingBottomHeight(context)),
                    itemCount: state.callHistory!.length,
                    itemBuilder: (context, index) {
                      return CallItem(
                        history: state.callHistory![index],
                      );
                    },
                  ),
                  state.callHistoryloading
                      ? Container(
                          color: Colors.black.withOpacity(.05),
                          height: screenHeight(context) -
                              (statusBarHeight(context) + 90),
                          child: Center(
                            child: CircularProgressIndicator(),
                          ),
                        )
                      : Container()
                ],
              );
      },
    );
  }
}

// Custom container widget for call item of the call history
class CallItem extends StatelessWidget {
  final CallsHistory history;
  CallItem({required this.history});
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {},
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 16, vertical: 16),
        margin: EdgeInsets.only(bottom: 16),
        decoration: BoxDecoration(
          color: AppColors.white,
          borderRadius: BorderRadius.circular(12),
        ),
        child: Row(
          children: [
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text16(
                      '${history.calledUser.firstName} ${history.calledUser.lastName}'),
                  SizedBox(height: 4),
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: [
                      Icon(
                        Icons.access_time_rounded,
                        size: 20,
                      ),
                      SizedBox(width: 4),
                      Text14(
                        ApiUtils.getFormattedDateTime(history.callTime),
                        color: AppColors.fontColor,
                      )
                    ],
                  ),
                ],
              ),
            ),
            SizedBox(width: 12),
            // Icon(Icons.add_comment_rounded),
            IconButton(
              onPressed: () async {
                final value = await showAnimatedDialog(
                  context: context,
                  barrierDismissible: true,
                  builder: (BuildContext context) {
                    if (history.note == null) {
                      return CreateCommentDialog();
                    } else {
                      return CommentDataDialog(
                        history: history,
                      );
                    }
                  },
                  animationType: DialogTransitionType.size,
                  curve: Curves.fastOutSlowIn,
                  duration: Duration(milliseconds: 400),
                );
                if (value != null && history.note == null) {
                  String note = value as String;
                  if (note.isNotEmpty) {
                    BlocProvider.of<ContactsBloc>(context)
                        .addNoteToCallHistory(history.id, note);
                  }
                }
              },
              icon: Icon(
                history.note == null
                    ? Icons.add_comment_rounded
                    : Icons.comment,
                color: AppColors.accent,
              ),
            )
          ],
        ),
      ),
    );
  }
}

// Custom widget for dialog box for reading comment on the call history
class CommentDataDialog extends StatelessWidget {
  final CallsHistory history;
  CommentDataDialog({required this.history});
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 32),
      padding: EdgeInsets.all(16),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(16),
        color: Colors.white,
      ),
      child: Material(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              children: [
                Expanded(child: Container()),
                Expanded(
                  child: Text20(
                    'Note',
                    fontWeight: FontWeight.w600,
                    textAlign: TextAlign.center,
                  ),
                ),
                Expanded(
                  child: Align(
                    alignment: Alignment.centerRight,
                    child: IconButton(
                      icon: Icon(Icons.close),
                      onPressed: () {
                        Navigator.pop(context);
                      },
                    ),
                  ),
                ),
              ],
            ),

            SizedBox(height: 4),
            Row(
              crossAxisAlignment: CrossAxisAlignment.end,
              children: [
                Icon(
                  Icons.call_made_rounded,
                  size: 20,
                ),
                SizedBox(width: 4),
                Text16(
                  '${history.calledUser.firstName} ${history.calledUser.lastName}',
                  fontWeight: FontWeight.w500,
                )
              ],
            ),
            SizedBox(height: 6),
            Row(
              crossAxisAlignment: CrossAxisAlignment.end,
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Icon(
                  Icons.access_time_rounded,
                  size: 20,
                ),
                SizedBox(width: 4),
                Text14(
                  ApiUtils.getFormattedDateTime(history.callTime),
                  color: AppColors.fontColor,
                )
              ],
            ),
            SizedBox(height: 24),
            Text16(
              history.note ?? '',
              textAlign: TextAlign.left,
            ),
            SizedBox(height: 24),
            // DefaultButton(
            //   onPressed: () {
            //     Navigator.pop(context);
            //   },
            //   text: 'Close',
            // )
          ],
        ),
      ),
    );
  }
}

//  Custom form widget for dialog box for creating comment on the call history

class CreateCommentDialog extends StatelessWidget {
  final TextEditingController controller = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 32),
      padding: EdgeInsets.all(16),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(16),
        color: Colors.white,
      ),
      child: Material(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Row(),
            Text18(
              'Add Note',
              fontWeight: FontWeight.w600,
            ),
            SizedBox(height: 4),
            Text14(
              'Write a note about selected call',
              color: AppColors.fontColor,
            ),
            SizedBox(height: 12),
            DefaultTextField(
              controller: controller,
              maxLines: 4,
            ),
            SizedBox(height: 16),
            DefaultButton(
              onPressed: () {
                Navigator.pop(context, controller.text);
              },
              text: 'Save',
            )
          ],
        ),
      ),
    );
  }
}
