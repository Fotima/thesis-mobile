import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:hospitalproject/blocs/contacts_tab/contacts_tab_bloc.dart';
import 'package:hospitalproject/constants/values/color_constants.dart';
import 'package:hospitalproject/ui/shared/text18.dart';
import 'package:hospitalproject/utils/screen_util.dart';
import 'package:hospitalproject/utils/shared_pref/shared_pref_helper.dart';

import 'calls_list.dart';
import 'contacts_list.dart';
// UI view for Auth screen

class ContactsTab extends StatefulWidget {
  @override
  _ContactsTabState createState() => _ContactsTabState();
}

class _ContactsTabState extends State<ContactsTab> {
  late ContactsBloc bloc;
  List<String> permissions = [];
  @override
  void initState() {
    permissions = SharedPrefHelper.getPermissions();
    bloc = ContactsBloc(permissions);
    WidgetsBinding.instance!.addPostFrameCallback((timeStamp) {
      bloc.loadInitialData();
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => bloc,
      child: Container(
        height: screenHeight(context),
        child: Stack(
          children: [
            MyBody(),
            MyAppBar(),
          ],
        ),
      ),
    );
  }
}

class MyBody extends StatelessWidget {
  final tabs = [ContactsList(), CallsList()];

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ContactsBloc, ContactsState>(
      builder: (context, state) {
        return Positioned(
          top: statusBarHeight(context) + 100,
          child: Container(
            height: screenHeight(context) - (statusBarHeight(context) + 90),
            width: MediaQuery.of(context).size.width,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(32),
              boxShadow: [
                BoxShadow(
                  color: AppColors.accentTransparent.withOpacity(.5),
                  blurRadius: 8,
                  spreadRadius: 4,
                  offset: Offset(0, 4),
                )
              ],
              color: AppColors.accentTransparent,
            ),
            child: AnimatedSwitcher(
              duration: Duration(milliseconds: 300),
              child: state.loading
                  ? Center(
                      child: CircularProgressIndicator(),
                    )
                  : tabs[state.currentTab],
            ),
          ),
        );
      },
    );
  }
}

class MyAppBar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ContactsBloc, ContactsState>(
      builder: (context, state) {
        return Container(
          padding: EdgeInsets.only(
              top: statusBarHeight(context) + 30, right: 24, left: 24),
          child: Stack(
            clipBehavior: Clip.none,
            children: [
              AnimatedPositioned(
                duration: Duration(milliseconds: 300),
                left: state.currentTab == 0 ? 0 : screenWidth(context) / 2 - 20,
                child: Container(
                  height: 42,
                  width: screenWidth(context) / 2 - 30,
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(12)),
                ),
              ),
              Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  AppBarTab(
                    index: 0,
                    title: 'Contacts',
                    isActive: state.currentTab == 0,
                  ),
                  AppBarTab(
                    index: 1,
                    title: 'Calls',
                    isActive: state.currentTab == 1,
                  )
                ],
              ),
            ],
          ),
        );
      },
    );
  }
}

class AppBarTab extends StatelessWidget {
  final int index;
  final String title;
  final bool isActive;

  AppBarTab({required this.index, required this.isActive, required this.title});
  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: GestureDetector(
        onTap: () {
          BlocProvider.of<ContactsBloc>(context).updateTabIndex(index);
        },
        child: Container(
          height: 42,
          alignment: Alignment.center,
          child: Text18(
            title,
            color: isActive ? AppColors.accent : AppColors.black,
            fontWeight: isActive ? FontWeight.w600 : FontWeight.normal,
          ),
        ),
      ),
    );
  }
}
