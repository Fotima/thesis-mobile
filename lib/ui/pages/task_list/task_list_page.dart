import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:hospitalproject/blocs/task_list/task_list_bloc.dart';
import 'package:hospitalproject/constants/values/color_constants.dart';
import 'package:hospitalproject/models/api/task/task.dart';
import 'package:hospitalproject/ui/pages/home/no_data_container.dart';
import 'package:hospitalproject/ui/pages/home/tasks_tab.dart';
import 'package:hospitalproject/ui/shared/text20.dart';
// UI view for List of tasks screen

class FilteredTaskListPage extends StatefulWidget {
  final TaskFilterPost filterPost;
  FilteredTaskListPage({required this.filterPost});

  @override
  _FilteredTaskListPageState createState() => _FilteredTaskListPageState();
}

class _FilteredTaskListPageState extends State<FilteredTaskListPage> {
  late TaskListBloc bloc;
  @override
  void initState() {
    bloc = TaskListBloc();
    // When UI of the screen is loaded, load the tasks by given constrains from the server, via BloC class
    WidgetsBinding.instance!.addPostFrameCallback((timeStamp) {
      bloc.loadTasks(widget.filterPost);
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => bloc,
      child: BlocBuilder<TaskListBloc, TaskListState>(
        builder: (context, state) {
          return Scaffold(
            appBar: AppBar(
              backgroundColor: AppColors.background,
              elevation: 0,
              iconTheme: IconTheme.of(context).copyWith(color: AppColors.black),
              title: Text20(
                'Tasks',
                fontWeight: FontWeight.w600,
              ),
            ),
            // if the state is loading, show the circular progress indicator
            body: state.loading
                ? Center(
                    child: CircularProgressIndicator(),
                  )
                // if state is loaded, show the list of tasks on the screen
                : state.tasks!.isEmpty
                    ? Center(child: NoDataContainer())
                    : ListView.builder(
                        padding:
                            EdgeInsets.symmetric(horizontal: 24, vertical: 24),
                        itemCount: state.tasks!.length,
                        itemBuilder: (context, index) {
                          return TaskCard(
                            taskInfo: state.tasks![index],
                            showMoreOption: false,
                          );
                        },
                      ),
          );
        },
      ),
    );
  }
}
