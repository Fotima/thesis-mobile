import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/svg.dart';
import 'package:hospitalproject/blocs/announcement/announcement_bloc.dart';
import 'package:hospitalproject/constants/assets/icon_constants.dart';
import 'package:hospitalproject/constants/values/color_constants.dart';
import 'package:hospitalproject/ui/shared/default_button.dart';
import 'package:hospitalproject/ui/shared/default_text_field.dart';
import 'package:hospitalproject/ui/shared/text14.dart';
import 'package:hospitalproject/ui/shared/text20.dart';
import 'package:hospitalproject/utils/screen_util.dart';

class NewAnnouncementPage extends StatefulWidget {
  @override
  _NewAnnouncementPageState createState() => _NewAnnouncementPageState();
}

class _NewAnnouncementPageState extends State<NewAnnouncementPage> {
  GlobalKey<FormState> formKey = GlobalKey<FormState>();
  TextEditingController titleController = TextEditingController();
  TextEditingController descriptionController = TextEditingController();
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => AnnouncementBloc(),
      child: Scaffold(
        body: Container(
          height: screenHeight(context),
          child: Stack(
            children: [
              Body(
                titleController: titleController,
                descriptionController: descriptionController,
                formKey: formKey,
              ),
              MyAppBar(),
            ],
          ),
        ),
      ),
    );
  }
}

class Body extends StatelessWidget {
  final TextEditingController titleController, descriptionController;
  final GlobalKey<FormState> formKey;

  Body({
    required this.titleController,
    required this.descriptionController,
    required this.formKey,
  });

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<AnnouncementBloc, AnnouncementState>(
      builder: (context, state) {
        return Positioned(
          top: screenHeight(context) * 1 / 3,
          child: Container(
            padding: EdgeInsets.only(top: 32, left: 24, right: 24, bottom: 24),
            height: screenHeight(context) * 2 / 3,
            width: MediaQuery.of(context).size.width,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(32),
              boxShadow: [
                BoxShadow(
                  color: AppColors.accentTransparent.withOpacity(.5),
                  blurRadius: 8,
                  spreadRadius: 4,
                  offset: Offset(0, 4),
                )
              ],
              color: AppColors.white,
            ),
            child: Form(
              key: formKey,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text14(
                    "This message will be seen by all users",
                    color: AppColors.fontColor,
                  ),
                  SizedBox(height: 24),
                  DefaultTextField(
                    controller: titleController,
                    hintText: 'Title',
                    isRequired: true,
                  ),
                  SizedBox(height: 18),
                  DefaultTextField(
                    controller: descriptionController,
                    hintText: 'Description',
                    isRequired: false,
                    maxLines: 10,
                  ),
                  Expanded(child: Container()),
                  DefaultButton(
                    onPressed: () {
                      if (formKey.currentState!.validate()) {
                        BlocProvider.of<AnnouncementBloc>(context)
                            .createNewAnnouncement(titleController.text,
                                descriptionController.text);
                      }
                    },
                    text: state.onTopLoading ? null : 'Save',
                    child: state.onTopLoading
                        ? Center(
                            child: CircularProgressIndicator(),
                          )
                        : null,
                    enabled: !state.onTopLoading,
                  ),
                  SizedBox(height: 24),
                ],
              ),
            ),
          ),
        );
      },
    );
  }
}

class MyAppBar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(
          top: statusBarHeight(context) + 20, right: 24, left: 24),
      // height: statusBarHeight(context) + 80,
      child: Column(
        children: [
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              GestureDetector(
                child: Icon(
                  Platform.isIOS ? Icons.arrow_back_ios : Icons.arrow_back,
                  color: AppColors.black,
                  size: 24,
                ),
                onTap: () {
                  Navigator.pop(context);
                },
              ),
              Expanded(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text20(
                      "Add  announcement",
                      fontWeight: FontWeight.w600,
                      textAlign: TextAlign.center,
                    ),
                    SizedBox(width: 24)
                  ],
                ),
              )
            ],
          ),
          SizedBox(height: 32),
          SvgPicture.asset(
            AppIcons.newAnnouncement,
            height: screenHeight(context) / 7,
          )
        ],
      ),
    );
  }
}
