import 'dart:io';

import 'package:bot_toast/bot_toast.dart';
import 'package:device_preview/device_preview.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:hospitalproject/utils/navigator/locator.dart';
import 'package:hospitalproject/utils/navigator/main_route_generator.dart';
import 'package:hospitalproject/utils/navigator/navigation_service.dart';
import 'package:hospitalproject/utils/shared_pref/shared_pref_helper.dart';
import 'package:firebase_core/firebase_core.dart';
import 'api/api_provider.dart';
import 'constants/values/color_constants.dart';
import 'constants/values/route_constants.dart';

// main function to start the application
void main() async {
  // initialize required services
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  await SharedPrefHelper.init();
  // set up the status bar stule
  SystemChrome.setSystemUIOverlayStyle(
    SystemUiOverlayStyle(
      statusBarIconBrightness: Brightness.dark,
      statusBarBrightness: Brightness.light,
      systemNavigationBarIconBrightness: Brightness.dark,
    ),
  );
  runApp(MyApp());
  setupLocator();
  setUpLogging();
  runApp(
    DevicePreview(
      enabled: false,
      builder: (context) => MyApp(),
    ),
  );
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  final NavigationService navigationService = locator<NavigationService>();

  @override
  void initState() {
    // initialize the chopper server
    ApiProvider.create();

    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    // set up the material app that is on top of all widgets of the application
    return MaterialApp(
      title: 'Hosptal platform',
      debugShowCheckedModeBanner: false,
      builder: BotToastInit(),
      theme: ThemeData(
        appBarTheme: AppBarTheme(
          color: AppColors.background,
          brightness: Brightness.light,
          iconTheme: IconThemeData(color: AppColors.black),
        ),
        primaryColor: Colors.white,
        primaryColorDark: Colors.white,
        accentColor: AppColors.accent,
        scaffoldBackgroundColor: AppColors.background,
        backgroundColor: AppColors.background,
        canvasColor: Colors.white,
        iconTheme: IconThemeData(color: AppColors.fontColor),
      ),
      locale: const Locale('en'),
      initialRoute: AppRoutes.authPage,
      onGenerateRoute: MainRouteGenerator.generateRoute,
      navigatorKey: locator<NavigationService>().navigatorKey,
    );
  }
}
