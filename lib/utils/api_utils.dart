import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:hospitalproject/constants/values/color_constants.dart';
// util convert some values from the server to UI acceptable values

class ApiUtils {
  static String getPriorityString(int priority) {
    switch (priority) {
      case 0:
        return "Low";
      case 1:
        return "Normal";
      case 2:
        return 'High';
      default:
        return 'Normal';
    }
  }

  static Color getStatusColor(int status) {
    switch (status) {
      case 0:
        return AppColors.accent;
      case 1:
        return AppColors.orange;
      case 2:
        return AppColors.lightGreen;
      default:
        return AppColors.accent;
    }
  }

  static Color getPriority(int priority) {
    switch (priority) {
      case 0:
        return AppColors.fontColor;
      case 1:
        return AppColors.orange;
      case 2:
        return AppColors.red;
      default:
        return AppColors.fontColor;
    }
  }

  static String getDateMonth(int date) {
    DateTime d = DateTime.fromMillisecondsSinceEpoch(date);
    return DateFormat('dd MMMM').format(d);
  }

  static String getFormattedDate(int date) {
    DateTime d = DateTime.fromMillisecondsSinceEpoch(date);
    return DateFormat('dd MMMM, yyyy').format(d);
  }

  static String getFormattedDateTime(int date) {
    DateTime d = DateTime.fromMillisecondsSinceEpoch(date);
    return DateFormat('dd MMMM, yyyy HH:mm ').format(d);
  }

  static String getTime(int date) {
    DateFormat format = DateFormat("HH:mm");
    DateTime d = DateTime.fromMillisecondsSinceEpoch(date);
    return format.format(d);
  }

  static String getTaskTime(int? start, int? end) {
    DateTime startDate;
    DateFormat format = DateFormat('HH:mm');
    DateFormat dateFormat = DateFormat('dd MMMM');
    String day;
    if (start == null && end == null) {
      return '';
    } else if (end == null) {
      startDate = DateTime.fromMillisecondsSinceEpoch(start!);
      day = dateFormat.format(startDate);
      return '$day, ${format.format(startDate)}';
    } else {
      startDate = DateTime.fromMillisecondsSinceEpoch(start!);
      DateTime endDate = DateTime.fromMillisecondsSinceEpoch(end);
      day = dateFormat.format(startDate);
      return '$day, ${format.format(startDate)} - ${format.format(endDate)}';
    }
  }
}
