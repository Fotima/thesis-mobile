import 'package:flutter/material.dart';
import 'package:logging/logging.dart';

// configuration of a logger for an application
void setUpLogging() {
  Logger.root.level = Level.ALL;
  Logger.root.onRecord.listen((data) {
    debugPrint('${data.level.name} ${data.time} ${data.message} ');
  });
}
