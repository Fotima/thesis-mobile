import 'package:intl/intl.dart';
// util to convert date data to suitable format for ui

class MyUtils {
  static DateTime firstDayOfWeek(DateTime day) {
    day = new DateTime.utc(day.year, day.month, day.day, 12);
    var decreaseNum = day.weekday % 7;
    return day.subtract(new Duration(days: decreaseNum - 1));
  }

  static DateTime lastDayOfWeek(DateTime day) {
    day = new DateTime.utc(day.year, day.month, day.day, 12);

    var increaseNum = day.weekday % 7;
    return day.add(new Duration(days: 7 - increaseNum + 1));
  }

  static String getTodayDate() {
    DateFormat format = DateFormat("MMMM, yyyy");
    DateTime now = DateTime.now();
    return format.format(now);
  }

  static Iterable<DateTime> daysInRange(DateTime start, DateTime end) sync* {
    var i = start;
    var offset = start.timeZoneOffset;
    while (i.isBefore(end)) {
      yield i;
      i = i.add(new Duration(days: 1));
      var timeZoneDiff = i.timeZoneOffset - offset;
      if (timeZoneDiff.inSeconds != 0) {
        offset = i.timeZoneOffset;
        i = i.subtract(new Duration(seconds: timeZoneDiff.inSeconds));
      }
    }
  }

  static bool isNumeric(String s) {
    // return double.parse(s, (e) => null) != null;
    return double.tryParse(s) != null;
  }
}
