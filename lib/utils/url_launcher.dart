import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

// ignore: avoid_void_async
// util to run launcher service

void launchURL(String urlFrom, bool isWebView) async {
  final url = urlFrom;

  if (await canLaunch(url)) {
    if (isWebView) {
      await launch(url, forceWebView: true);
    } else {
      await launch(url);
    }
  } else {
    debugPrint('Could not launch $url');
  }
}

void launchTel(String phone) async {
  final String tel = phone
      .replaceAll('+', '')
      .replaceAll(')', '')
      .replaceAll('(', '')
      .replaceAll(' ', '');

  await launch('tel:$tel');
}

void launchEmail(String email, String subject, {String? body}) async {
  final Uri url = Uri(scheme: 'mailto', path: email, queryParameters: {
    'subject': subject,
  });
  await launch(url.toString());
}
