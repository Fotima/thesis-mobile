import 'package:hospitalproject/constants/values/shared_pref_constants.dart';
import 'package:shared_preferences/shared_preferences.dart';

// ignore: avoid_classes_with_only_static_members
// util to get and set values from shared preferences service
class SharedPrefHelper {
  static SharedPreferences? prefs;
  static Future<void> init() async {
    if (prefs == null) {
      prefs = await SharedPreferences.getInstance();
    }
  }

  static void dispose() => prefs = null;
  static Future<bool> clear() => prefs!.clear();

  static String? getToken() => prefs!.getString(ShPrefKeys.token);
  static set token(String value) => prefs!.setString(ShPrefKeys.token, value);

  static String? getFirebaseToken() =>
      prefs!.getString(ShPrefKeys.firebaseToken);
  static set firebaseToken(String value) =>
      prefs!.setString(ShPrefKeys.firebaseToken, value);

  static bool isInitialOpen() => prefs!.getBool(ShPrefKeys.initialOpen) ?? true;
  static set setInitialOpen(bool value) =>
      prefs!.setBool(ShPrefKeys.initialOpen, value);

  static int? getMyId() => prefs!.getInt(ShPrefKeys.myId);
  static set setMyId(int value) => prefs!.setInt(ShPrefKeys.myId, value);

  static List<String> getPermissions() =>
      prefs!.getStringList(ShPrefKeys.permissions) ?? [];
  static set setPermissions(List<String> value) =>
      prefs!.setStringList(ShPrefKeys.permissions, value);

  static int? getRoleRank() => prefs!.getInt(ShPrefKeys.roleRank);
  static set setRoleRank(int value) =>
      prefs!.setInt(ShPrefKeys.roleRank, value);
}
