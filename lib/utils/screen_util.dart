import 'package:flutter/material.dart';
// screen size utils
double screenHeight(BuildContext context) => MediaQuery.of(context).size.height;

double screenWidth(BuildContext context) => MediaQuery.of(context).size.width;

double statusBarHeight(BuildContext context) =>
    MediaQuery.of(context).padding.top;

double paddingBottomHeight(BuildContext context) =>
    MediaQuery.of(context).padding.bottom;
double appBarHeight(BuildContext context) => AppBar().preferredSize.height;

double customAppBarHight(BuildContext context) =>
    appBarHeight(context) + statusBarHeight(context);
