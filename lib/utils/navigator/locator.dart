import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:logging/logging.dart';

import 'navigation_service.dart';

// configuration of a navigator service
GetIt locator = GetIt.instance;

void setupLocator() {
  locator.registerLazySingleton(() => NavigationService());
}

setUpLogging() {
  Logger.root.level = Level.ALL;
  Logger.root.onRecord.listen(
    (data) {
      debugPrint('${data.level.name} ${data.time} ${data.message} ');
    },
  );
}
