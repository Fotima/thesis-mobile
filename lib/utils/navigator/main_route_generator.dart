import 'package:flutter/material.dart';
import 'package:hospitalproject/constants/values/route_constants.dart';
import 'package:hospitalproject/models/api/auth/auth.dart';
import 'package:hospitalproject/models/api/task/task.dart';
import 'package:hospitalproject/models/api/user/user.dart';
import 'package:hospitalproject/ui/pages/auth/auth_page.dart';
import 'package:hospitalproject/ui/pages/home/home_page.dart';
import 'package:hospitalproject/ui/pages/new_announcement/new_announcement_page.dart';
import 'package:hospitalproject/ui/pages/new_task/new_task_page.dart';
import 'package:hospitalproject/ui/pages/profile_edit/profile_edit_page.dart';
import 'package:hospitalproject/ui/pages/schedule/schedule_page.dart';
import 'package:hospitalproject/ui/pages/task_archive/task_archive_page.dart';
import 'package:hospitalproject/ui/pages/task_list/task_list_page.dart';

import 'cupertino_style_navigation_route.dart';

// Route generator responsible to route screens by route name
class MainRouteGenerator {
  static Route<dynamic>? generateRoute(RouteSettings settings) {
    switch (settings.name) {
      case AppRoutes.authPage:
        return CustomCupertinoStyleNavigationRoute(
          builder: (_) => AuthPage(),
        );
      case AppRoutes.homePage:
        return CustomCupertinoStyleNavigationRoute(
          builder: (_) => HomePage(
            loginResponse: settings.arguments as LoginResponse,
          ),
        );
      case AppRoutes.newTaskPage:
        return CustomCupertinoStyleNavigationRoute(
          builder: (_) => NewTaskPage(),
        );
      case AppRoutes.profileEditPage:
        return CustomCupertinoStyleNavigationRoute(
          builder: (_) => ProfileEditPage(
            profileData: settings.arguments as UserProfile,
          ),
        );
      case AppRoutes.schedulePage:
        return CustomCupertinoStyleNavigationRoute(
          builder: (_) => SchedulePage(
            userProfile: settings.arguments as UserProfile,
          ),
        );
      case AppRoutes.taskArchivePage:
        return CustomCupertinoStyleNavigationRoute(
          builder: (_) => TaskArchivePage(),
        );
      case AppRoutes.taskListPage:
        return CustomCupertinoStyleNavigationRoute(
          builder: (_) => FilteredTaskListPage(
              filterPost: settings.arguments as TaskFilterPost),
        );
      case AppRoutes.newAnnouncementPage:
        return CustomCupertinoStyleNavigationRoute(
            builder: (_) => NewAnnouncementPage());
      default:
        return null;
      // return CustomCupertinoStyleNavigationRoute(
      //   builder: (_) => HomePage(),
      // );
    }
  }
  //  static Route<dynamic> _errorRoute() {
  //   return MaterialPageRoute(builder: (_) => ErrorPage());
  // }
}
