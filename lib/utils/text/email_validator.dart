// util to validate the email
class EmailValidator {
  static bool isEmail(String email) {
    bool isActive = email.contains('@') &&
        email.contains('.') &&
        !email.contains('..') &&
        !email.contains('.@') &&
        !email.contains('@.');

    if (isActive) {
      final int lastDotIndex = email.lastIndexOf('.');
      final int atIndex = email.lastIndexOf('@');
      final String domen = email.substring(lastDotIndex);
      final String beforeAtSymbol = email.substring(0, atIndex);

      isActive = domen.length > 1 &&
          lastDotIndex > atIndex &&
          beforeAtSymbol.isNotEmpty;

      return isActive;
    }

    return false;
  }
}
