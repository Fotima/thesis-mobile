// util to check null or empty value
bool isNotNullNotEmpty(String? text) {
  if (text == null) {
    return false;
  }

  if (text.isEmpty) {
    return false;
  }

  return true;
}
