import 'dart:io';
import 'package:chopper/chopper.dart';
import 'package:flutter/material.dart';
import 'package:hospitalproject/services/announcement/announcement_service.dart';
import 'package:hospitalproject/services/auth/auth_service.dart';
import 'package:hospitalproject/services/chat_service.dart/chat_service.dart';
import 'package:hospitalproject/services/contacts/contacts_service.dart';
import 'package:hospitalproject/services/task/task_service.dart';
import 'package:hospitalproject/services/user/user_service.dart';
import 'package:http/io_client.dart' as http;
import 'custom_converter.dart';

// Static service classes are created in order to follow singleton principle and connect to the service once
class ApiProvider {
  static late ChopperClient _client;
  static late AuthService authService;
  static late TaskService taskService;
  static late ContactsService contactsService;
  static late UserService userService;
  static late ChatService chatService;
  static late AnnouncementService announcementService;

  ///Services
  static create({String? token}) {
    // chopper client init
    _client = ChopperClient(
      client: http.IOClient(
        HttpClient()..connectionTimeout = Duration(seconds: 40),
      ),
      services: [
        AuthService.create(),
        TaskService.create(),
        ContactsService.create(),
        UserService.create(),
        ChatService.create(),
        AnnouncementService.create(),
      ],
      interceptors: getInterceptors(token),
      converter: CustomDataConverter(),
    );

    authService = _client.getService<AuthService>();
    taskService = _client.getService<TaskService>();
    contactsService = _client.getService<ContactsService>();
    userService = _client.getService<UserService>();
    chatService = _client.getService<ChatService>();
    announcementService = _client.getService<AnnouncementService>();
  }

// adding headers to the chopper client
  static List getInterceptors(token) {
    List interceptors = [];

    interceptors.add(HttpLoggingInterceptor());
    if (token != null) {
      debugPrint('Current token is $token');
      interceptors.add(HeadersInterceptor({
        HttpHeaders.authorizationHeader: 'Bearer $token',
      }));
    }
    return interceptors;
  }

  static dispose() {
    _client.dispose();
  }
}
