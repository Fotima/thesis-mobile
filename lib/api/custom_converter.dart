import 'package:chopper/chopper.dart';
import 'package:hospitalproject/models/announcement/announcement.dart';
import 'package:hospitalproject/models/api/auth/auth.dart';
import 'package:hospitalproject/models/api/base/base.dart';
import 'package:hospitalproject/models/api/calls/calls.dart';
import 'package:hospitalproject/models/api/chat/chat.dart';
import 'package:hospitalproject/models/api/chat_room/chat_room.dart';
import 'package:hospitalproject/models/api/role/role.dart';
import 'package:hospitalproject/models/api/task/task.dart';
import 'package:hospitalproject/models/api/user/user.dart';
import 'package:hospitalproject/models/api/user_short/user_short.dart';

// Json converter to convert incoming json object to the required model classes
class CustomDataConverter extends JsonConverter {
  @override
  Response<BodyType> convertResponse<BodyType, InnerType>(Response response) {
    final Response dynamicResponse = super.convertResponse(response);
    var body = dynamicResponse.body;
    if (body is String)
      return decodeJson<BodyType, InnerType>(response) as Response<BodyType>;
    if (body is bool)
      return decodeJson<BodyType, InnerType>(response) as Response<BodyType>;
    if (body is Map) {
      if (body.isEmpty) {
        return decodeJson<BodyType, InnerType>(response) as Response<BodyType>;
      }
    }
    final BodyType customBody =
        convertToCustomObject<BodyType, InnerType>(body);

    return dynamicResponse.copyWith<BodyType>(
      body: customBody,
    );
  }

  BodyType convertToCustomObject<BodyType, SingleItemType>(dynamic element) {
    if (element is List)
      return deserializeListOf<BodyType, SingleItemType>(element);
    else
      return deserialize<SingleItemType>(element);
  }

  dynamic deserializeListOf<BodyType, SingleItemType>(
    List dynamicList,
  ) {
    List<SingleItemType> list = dynamicList
        .map<SingleItemType>((element) => deserialize<SingleItemType>(element))
        .toList();
    return list;
  }

// deserialize json object to app class models
  dynamic deserialize<SingleItemType>(Map<String, dynamic> json) {
    switch (SingleItemType) {
      case LoginResponse:
        return LoginResponse.fromJson(json);
      case BaseResponse:
        return BaseResponse.fromJson(json);
      case TaskInfo:
        return TaskInfo.fromJson(json);
      case RoleShort:
        return RoleShort.fromJson(json);
      case UserShort:
        return UserShort.fromJson(json);

      case CallsHistory:
        return CallsHistory.fromJson(json);
      case UserProfile:
        return UserProfile.fromJson(json);
      case ChatRoom:
        return ChatRoom.fromJson(json);
      case ChatMessagePageable:
        return ChatMessagePageable.fromJson(json);
      case UserByRole:
        return UserByRole.fromJson(json);
      case AnnouncementInfo:
        return AnnouncementInfo.fromJson(json);
      default:
        return null;
    }
  }
}
