import 'dart:convert';

import 'package:bloc/bloc.dart';
import 'package:chopper/chopper.dart';
import 'package:hospitalproject/api/api_provider.dart';
import 'package:hospitalproject/models/api/task/task.dart';
import 'package:hospitalproject/ui/shared/top_message.dart';
import 'package:hospitalproject/utils/network/api_helper.dart';
// BLoC object for state management at Task list  screen

class TaskListState {
  final bool loading;
  final List<TaskInfo>? tasks;
  TaskListState({
    this.loading = true,
    this.tasks,
  });

  TaskListState copyWith({
    bool? loading,
    List<TaskInfo>? tasks,
  }) {
    return TaskListState(
      loading: loading ?? this.loading,
      tasks: tasks ?? this.tasks,
    );
  }
}

class TaskListBloc extends Cubit<TaskListState> {
  TaskListBloc()
      : super(TaskListState(
          tasks: [],
        ));

// load list of task with a filtering constraints
  void loadTasks(TaskFilterPost post) async {
    emit(state.copyWith(loading: true));
    Response<List<TaskInfo>> response =
        await request(ApiProvider.taskService.filterTasks(post));
    // if the request is sucessfull, emit the response to the server
    if (response.isSuccessful) {
      emit(state.copyWith(loading: false, tasks: response.body));
    } else {
      // if the request is failed, show the error message
      emit(state.copyWith(loading: false));
      String message = 'Error';
      try {
        final Map<String, dynamic> error =
            jsonDecode(response.error.toString());
        message = error['message'];
      } catch (e) {
        message = response.error.toString();
      }
      showMessage(message, isError: true);
    }
  }
}
