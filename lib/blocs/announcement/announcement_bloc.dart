import 'dart:convert';

import 'package:bloc/bloc.dart';
import 'package:chopper/chopper.dart';
import 'package:flutter/material.dart';
import 'package:hospitalproject/api/api_provider.dart';
import 'package:hospitalproject/models/announcement/announcement.dart';
import 'package:hospitalproject/ui/shared/top_message.dart';
import 'package:hospitalproject/utils/navigator/locator.dart';
import 'package:hospitalproject/utils/navigator/navigation_service.dart';
import 'package:hospitalproject/utils/network/api_helper.dart';

// BLoC object for state management at Announcement screen
class AnnouncementState {
  final bool loading;
  final bool onTopLoading;
  final List<AnnouncementInfo>? announcements;
  AnnouncementState({
    this.loading = true,
    this.onTopLoading = false,
    this.announcements,
  });

  AnnouncementState copyWith({
    bool? loading,
    bool? onTopLoading,
    List<AnnouncementInfo>? announcements,
  }) {
    return AnnouncementState(
      loading: loading ?? this.loading,
      onTopLoading: onTopLoading ?? this.onTopLoading,
      announcements: announcements ?? this.announcements,
    );
  }
}

class AnnouncementBloc extends Cubit<AnnouncementState> {
  AnnouncementBloc() : super(AnnouncementState(announcements: []));

// loads all announcements from the server
  void loadAllAnnouncements() async {
    // emits the loading state
    emit(state.copyWith(loading: true));
    Response<List<AnnouncementInfo>> response =
        await request(ApiProvider.announcementService.getall());
    // if response is sucessfull, emit loading state false with the request response
    if (response.isSuccessful) {
      emit(state.copyWith(
        loading: false,
        announcements: response.body,
      ));
    } else {
      // if response is not successfull, emit loading state false with no request response
      emit(state.copyWith(loading: false));
      String message = 'Error';
      try {
        final Map<String, dynamic> error =
            jsonDecode(response.error.toString());
        message = error['message'];
      } catch (e) {
        message = response.error.toString();
      }
      showMessage(message, isError: true);
    }
  }

// post announcement object to the server
  void createNewAnnouncement(String title, String description) async {
    // emit loadging state
    emit(state.copyWith(onTopLoading: true));

    Response<AnnouncementInfo> response = await request(
        ApiProvider.announcementService.createAnnouncement(
            AnnouncementPost(title: title, description: description)));
// if request is sucessfull, close current screen and navigate to the previous screen
    if (response.isSuccessful) {
      showMessage("Announcement is created successfully");
      Navigator.pop(locator<NavigationService>().navigatorKey.currentContext!,
          response.body);
    } else {
      // if request is failed, show the error message
      emit(state.copyWith(onTopLoading: false));
      String message = 'Error';
      try {
        final Map<String, dynamic> error =
            jsonDecode(response.error.toString());
        message = error['message'];
      } catch (e) {
        message = response.error.toString();
      }
      showMessage(message, isError: true);
    }
  }
}
