import 'dart:convert';

import 'package:bloc/bloc.dart';
import 'package:chopper/chopper.dart';
import 'package:flutter/material.dart';
import 'package:hospitalproject/api/api_provider.dart';
import 'package:hospitalproject/models/api/user/user.dart';
import 'package:hospitalproject/ui/shared/top_message.dart';
import 'package:hospitalproject/utils/navigator/locator.dart';
import 'package:hospitalproject/utils/navigator/navigation_service.dart';
import 'package:hospitalproject/utils/network/api_helper.dart';
// BLoC object for state management at  Profile edit screen

class ProfileEditState {
  final bool loading;
  ProfileEditState({
    this.loading = false,
  });

  ProfileEditState copyWith({
    bool? loading,
  }) {
    return ProfileEditState(
      loading: loading ?? this.loading,
    );
  }
}

class ProfileEditBloc extends Cubit<ProfileEditState> {
  ProfileEditBloc() : super(ProfileEditState());

// push update profile object to the server and update user data on db
  void updateProfileData(UserProfileUpdatePost post) async {
    emit(state.copyWith(loading: true));
    Response<UserProfile> response =
        await request(ApiProvider.userService.updateMyProfile(post));
    if (response.isSuccessful) {
      showMessage("User data is updated successfully");
      Navigator.pop(locator<NavigationService>().navigatorKey.currentContext!,
          response.body);
    } else {
      emit(state.copyWith(loading: false));
      String message = 'Error';
      try {
        final Map<String, dynamic> error =
            jsonDecode(response.error.toString());
        message = error['message'];
      } catch (e) {
        message = response.error.toString();
      }
      showMessage(message, isError: true);
    }
  }
}
