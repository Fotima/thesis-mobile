import 'dart:convert';

import 'package:bloc/bloc.dart';
import 'package:chopper/chopper.dart';
import 'package:hospitalproject/api/api_provider.dart';
import 'package:hospitalproject/models/api/user/user.dart';
import 'package:hospitalproject/ui/shared/top_message.dart';
import 'package:hospitalproject/utils/network/api_helper.dart';
// BLoC object for state management at  Profile screen

class ProfileState {
  final bool loading;
  final UserProfile? userProfile;
  final bool onTopLoading;
  ProfileState({
    this.loading = true,
    this.onTopLoading = false,
    this.userProfile,
  });

  ProfileState copyWith({
    bool? loading,
    bool? onTopLoading,
    UserProfile? userProfile,
  }) {
    return ProfileState(
      loading: loading ?? this.loading,
      onTopLoading: onTopLoading ?? this.onTopLoading,
      userProfile: userProfile ?? this.userProfile,
    );
  }
}

class ProfileBloc extends Cubit<ProfileState> {
  ProfileBloc() : super(ProfileState());

// load profile data of a user from the server
  void loadProfileData() async {
    // emit loading state
    emit(state.copyWith(loading: true));
    Response<UserProfile> response =
        await request(ApiProvider.userService.getmyProfileData());
    if (response.isSuccessful) {
      emit(state.copyWith(loading: false, userProfile: response.body));
    } else {
      // if the request is failed, show an error message
      emit(state.copyWith(loading: false));
      String message = 'Error';
      try {
        final Map<String, dynamic> error =
            jsonDecode(response.error.toString());
        message = error['message'];
      } catch (e) {
        message = response.error.toString();
      }
      showMessage(message, isError: true);
    }
  }

  updateProfileData(UserProfile data) {
    emit(state.copyWith(userProfile: data));
  }

// update user status (checked in) on the server and on UI
  updateUserStatus() async {
    emit(state.copyWith(onTopLoading: true));
    bool status = !(state.userProfile!.checkedIn);
    UserStatusPost post = UserStatusPost(status: status);

    Response response =
        await request(ApiProvider.userService.updateMyStatus(post));
    if (response.isSuccessful) {
      state.userProfile!.checkedIn = status;
      emit(state.copyWith(onTopLoading: false, userProfile: state.userProfile));
      if (status) {
        showMessage("You checked in");
      } else {
        showMessage("You checked out");
      }
    } else {
      emit(state.copyWith(onTopLoading: false));
      String message = 'Error';
      try {
        final Map<String, dynamic> error =
            jsonDecode(response.error.toString());
        message = error['message'];
      } catch (e) {
        message = response.error.toString();
      }
      showMessage(message, isError: true);
    }
  }
}
