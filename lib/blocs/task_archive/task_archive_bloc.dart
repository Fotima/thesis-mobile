import 'dart:convert';

import 'package:chopper/chopper.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:hospitalproject/api/api_provider.dart';
import 'package:hospitalproject/models/api/task/task.dart';
import 'package:hospitalproject/models/api/user_short/user_short.dart';
import 'package:hospitalproject/ui/shared/top_message.dart';
import 'package:hospitalproject/utils/network/api_helper.dart';
// BLoC object for state management at Task archive  screen

class TaskArchiveState {
  final bool loading;
  final List<UserShort>? users;
  final TaskFilterPost? post;
  final List<UserShort>? commonWithUsers;
  final List<UserShort>? assignedByUsers;

  TaskArchiveState({
    this.loading = true,
    this.users,
    this.post,
    this.assignedByUsers,
    this.commonWithUsers,
  });
  TaskArchiveState copyWith({
    bool? loading,
    List<UserShort>? users,
    TaskFilterPost? post,
    List<UserShort>? commonWithUsers,
    List<UserShort>? assignedByUsers,
  }) {
    return TaskArchiveState(
      loading: loading ?? this.loading,
      users: users ?? this.users,
      post: post ?? this.post,
      commonWithUsers: commonWithUsers ?? this.commonWithUsers,
      assignedByUsers: assignedByUsers ?? this.assignedByUsers,
    );
  }
}

class TaskArchiveBloc extends Cubit<TaskArchiveState> {
  TaskArchiveBloc()
      : super(
          TaskArchiveState(
            users: [],
            assignedByUsers: [],
            commonWithUsers: [],
            post: TaskFilterPost(assignedUsers: [], createdUsers: []),
          ),
        );

// load all users of a system from the server
  loadUsers() async {
    emit(state.copyWith(loading: true));
    Response<List<UserShort>> response =
        await request(ApiProvider.userService.getAllUsers());
    // if request is successfull, emit the data to the UI
    if (response.isSuccessful) {
      emit(state.copyWith(loading: false, users: response.body));
    } else {
      emit(state.copyWith(loading: false));
      String message = 'Error';
      try {
        final Map<String, dynamic> error =
            jsonDecode(response.error.toString());
        message = error['message'];
      } catch (e) {
        message = response.error.toString();
      }
      showMessage(message, isError: true);
    }
  }
  // update from date value on UI

  updateFromDate(DateTime date) {
    state.post!.fromDate = date.millisecondsSinceEpoch;
    emit(state.copyWith(post: state.post));
  }

// update to Date value on UI
  updateToDate(DateTime date) {
    state.post!.toDate = date.millisecondsSinceEpoch;
    emit(state.copyWith(post: state.post));
  }

// update "created by me" check box value on UI
  updateCreateByMe(bool value) {
    state.post!.myTasks = value;
    if (value) {
      state.post!.assignedUsers!.clear();
      state.post!.createdUsers!.clear();
    }
    emit(state
        .copyWith(post: state.post, commonWithUsers: [], assignedByUsers: []));
  }

// update task with common with users list value on UI
  updateCommonWith(List<UserShort> users) {
    state.post!.assignedUsers = users.map((e) => e.id).toList();
    emit(state.copyWith(post: state.post, commonWithUsers: users));
  }

// update list of users selected for "authors" filter on UI
  updateCreatorUsers(List<UserShort> users) {
    state.post!.createdUsers = users.map((e) => e.id).toList();
    emit(state.copyWith(post: state.post, assignedByUsers: users));
  }

// clear value on the form on UI
  resetFilterValues() {
    emit(state.copyWith(
        post: TaskFilterPost(createdUsers: [], assignedUsers: []),
        commonWithUsers: [],
        assignedByUsers: []));
  }
}
