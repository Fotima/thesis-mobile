import 'dart:convert';

import 'package:chopper/chopper.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:hospitalproject/api/api_provider.dart';
import 'package:hospitalproject/models/api/role/role.dart';
import 'package:hospitalproject/models/api/task/task.dart';
import 'package:hospitalproject/models/api/user_short/user_short.dart';
import 'package:hospitalproject/ui/shared/top_message.dart';
import 'package:hospitalproject/utils/navigator/locator.dart';
import 'package:hospitalproject/utils/navigator/navigation_service.dart';
import 'package:hospitalproject/utils/network/api_helper.dart';
// BLoC object for state management at new create task screen

class NewTaskState {
  final int selectedAssignType;
  final List<RoleShort>? roles;
  final List<UserShort>? users;
  final bool loading;
  final TaskCreate? taskCreate;
  final bool ontopLoading;
  NewTaskState({
    this.selectedAssignType = 0,
    this.loading = true,
    this.roles,
    this.users,
    this.taskCreate,
    this.ontopLoading = false,
  });

  NewTaskState copyWith({
    int? selectedAssignType,
    List<RoleShort>? roles,
    List<UserShort>? users,
    bool? loading,
    TaskCreate? taskCreate,
    bool? ontopLoading,
  }) {
    return NewTaskState(
      selectedAssignType: selectedAssignType ?? this.selectedAssignType,
      roles: roles ?? this.roles,
      users: users ?? this.users,
      loading: loading ?? this.loading,
      taskCreate: taskCreate ?? this.taskCreate,
      ontopLoading: ontopLoading ?? this.ontopLoading,
    );
  }
}

class NewTaskBloc extends Cubit<NewTaskState> {
  NewTaskBloc()
      : super(NewTaskState(
          users: [],
          roles: [],
          taskCreate: TaskCreate(),
        ));

// load list of roles and users from the server
  void loadInitialData() async {
    Response<List<RoleShort>> rolesResponse =
        await request(ApiProvider.taskService.getRolesLowerRank());

    Response<List<UserShort>> usersResponse =
        await request(ApiProvider.taskService.getUsersLowerRank());
    // if requests are successfull, emit returned lists to the UI view
    if (rolesResponse.isSuccessful && usersResponse.isSuccessful) {
      emit(state.copyWith(
        roles: rolesResponse.body!,
        users: usersResponse.body,
        loading: false,
      ));
    } else {
      emit(state.copyWith(loading: false));
      showMessage("Error", isError: true);
    }
  }

//  update assign type boolean on UI
  void updateAssignType(int index) {
    emit(state.copyWith(selectedAssignType: index));
  }

// update start date on UI
  updateStartDate(String date) {
    state.taskCreate!.taskStart = date;
    emit(state.copyWith(taskCreate: state.taskCreate!));
  }

// update end date on UI
  updateEndDate(String date) {
    state.taskCreate!.taskEnd = date;
    emit(state.copyWith(taskCreate: state.taskCreate!));
  }

// update task priority value on UI
  updatePriority(int priority) {
    state.taskCreate!.priority = priority;
    emit(state.copyWith(taskCreate: state.taskCreate!));
  }

// update assigned users or a role value on UI
  updateAssigned(bool isRole, dynamic value) {
    if (isRole) {
      state.taskCreate!.assignedRoleId = value.id;
      state.taskCreate!.assignedUserIds = null;
    } else {
      state.taskCreate!.assignedRoleId = null;
      List<UserShort> users = value;
      state.taskCreate!.assignedUserIds = users.map((e) => e.id).toList();
    }
  }

// push new task object to the server
  void createNewTask(String title, String description, String location) async {
    state.taskCreate!.title = title;
    state.taskCreate!.description = description;
    state.taskCreate!.taskPlacement = location;
    emit(state.copyWith(ontopLoading: true));
    // var numbers = List<int>.generate(100, (index) => index);
    // final startTime = DateTime.now().second;
    // await Future.forEach(numbers, (item) async {
    Response<TaskInfo> response =
        await request(ApiProvider.taskService.createTask(state.taskCreate!));
    // if the request is sucessfull, close the current screen
    if (response.isSuccessful) {
      emit(state.copyWith(ontopLoading: false));
      showMessage("Task created");
      Navigator.pop(locator<NavigationService>().navigatorKey.currentContext!);
    } else {
      // if the request is failed, show the error message
      emit(state.copyWith(ontopLoading: false));
      String message = 'Error';
      try {
        final Map<String, dynamic> error =
            jsonDecode(response.error.toString());
        message = error['message'];
      } catch (e) {
        message = response.error.toString();
      }
      showMessage(message, isError: true);
    }
    // });
    // final result = DateTime.now().second - startTime;
    // print('result $result');
  }
}
