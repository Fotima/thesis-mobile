import 'dart:convert';

import 'package:bloc/bloc.dart';
import 'package:chopper/chopper.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:hospitalproject/api/api_provider.dart';
import 'package:hospitalproject/models/api/base/base.dart';
import 'package:hospitalproject/models/api/task/task.dart';
import 'package:hospitalproject/models/ui/task_filter.dart';
import 'package:hospitalproject/ui/shared/top_message.dart';
import 'package:hospitalproject/utils/network/api_helper.dart';
import 'package:hospitalproject/utils/shared_pref/shared_pref_helper.dart';

// BLoC object for state management at Home tab screen
class HomeState {
  final bool loading;
  final List<TaskInfo>? mytasks;
  final bool onTopLoading;
  final TaskFilter? taskFilter;
  final List<TaskInfo>? allTasks;
  final int selectedTab;
  HomeState({
    this.loading = true,
    this.mytasks,
    this.onTopLoading = false,
    this.taskFilter,
    this.allTasks,
    this.selectedTab = 0,
  });

  HomeState copyWith({
    bool? loading,
    List<TaskInfo>? mytasks,
    bool? onTopLoading,
    TaskFilter? taskFilter,
    List<TaskInfo>? allTasks,
    int? selectedTab,
  }) {
    return HomeState(
      loading: loading ?? this.loading,
      mytasks: mytasks ?? this.mytasks,
      onTopLoading: onTopLoading ?? this.onTopLoading,
      taskFilter: taskFilter ?? this.taskFilter,
      allTasks: allTasks ?? this.allTasks,
      selectedTab: selectedTab ?? this.selectedTab,
    );
  }
}

class HomeBloc extends Cubit<HomeState> {
  HomeBloc() : super(HomeState(mytasks: [], allTasks: []));
  updateTab(int index) {
    emit(state.copyWith(selectedTab: index));
  }

// load tasks of a user for current day
  loadTasks() async {
    emit(state.copyWith(loading: true));
    var start = DateTime.now().millisecondsSinceEpoch;
    Response<List<TaskInfo>> taskResponse =
        await request(ApiProvider.taskService.getMyTasks());
    var result = DateTime.now().millisecondsSinceEpoch - start;
    print('result: $result');
    // if the request is sucessfulle, return the response to the UI view
    if (taskResponse.isSuccessful) {
      state.allTasks!.addAll(taskResponse.body!);
      taskResponse.body!.sort((a, b) => a.taskStart!.compareTo(b.taskStart!));
      emit(state.copyWith(
        mytasks: taskResponse.body,
        loading: false,
        allTasks: state.allTasks,
      ));
    } else {
      // if the request is failed, show the error message
      emit(state.copyWith(loading: false));
      String message = 'Error';
      try {
        final Map<String, dynamic> error =
            jsonDecode(taskResponse.error.toString());
        message = error['message'];
      } catch (e) {
        message = taskResponse.error.toString();
      }
      showMessage(message, isError: true);
    }
  }

//push  update of the progress status of the task
  updateTaskStatus(TaskStatusPost post) async {
    emit(state.copyWith(onTopLoading: true));
    Response<TaskInfo> response =
        await request(ApiProvider.taskService.updateTaskStatus(post));

//  if the request is sucessfull, update the status of the corresponding task on the UI view
    if (response.isSuccessful) {
      state.mytasks!.firstWhere((element) => element.id == post.taskId).status =
          post.status;
      emit(state.copyWith(mytasks: state.mytasks, onTopLoading: false));
    } else {
      // if the request is failed, show the error message
      emit(state.copyWith(onTopLoading: false));
      String message = 'Error';
      try {
        final Map<String, dynamic> error =
            jsonDecode(response.error.toString());
        message = error['message'];
      } catch (e) {
        message = response.error.toString();
      }
      showMessage(message, isError: true);
    }
  }
  // push firebase token of a user to the server

  updateFibaseToken(String? token) async {
    if (token != null) {
      String? savedToken = SharedPrefHelper.getFirebaseToken();
      if (savedToken != token) {
        Response<BaseResponse> response =
            await request(ApiProvider.authService.updateFirebaseToken(token));
        if (response.isSuccessful && response.body!.result) {
          SharedPrefHelper.firebaseToken = token;
        } else {
          String message = 'Error';
          try {
            final Map<String, dynamic> error =
                jsonDecode(response.error.toString());
            message = error['message'];
          } catch (e) {
            message = response.error.toString();
          }
          showMessage(message, isError: true);
        }
      }
    }
  }

// get received notification message from the firebase and show the message on the screen
  handleNotificationGet(Map<String, dynamic>? data,
      RemoteNotification? notification, bool applicationIsActive) {
    if (notification != null) {
      if (applicationIsActive) {
        showMessage(" ${notification.body}",
            alert: true, title: "${notification.title} ");
      } else {
        showMessage(" ${notification.body}",
            alert: true, title: "${notification.title} ");
      }
    }
  }

  void updateFilter(TaskFilter taskFilter) async {
    if (taskFilter.type == -1) {
      state.mytasks!.addAll(state.allTasks!);
      emit(state.copyWith(taskFilter: taskFilter, mytasks: state.mytasks));
    } else if (taskFilter.type == 1) {
      state.mytasks!.clear();
      state.allTasks?.forEach((element) {
        if (element.status == taskFilter.value) {
          state.mytasks!.add(element);
        }
      });
      emit(state.copyWith(taskFilter: taskFilter, mytasks: state.mytasks));
    } else if (taskFilter.type == 2) {
      state.mytasks!.clear();
      state.allTasks?.forEach((element) {
        if (element.priority == taskFilter.value) {
          state.mytasks!.add(element);
        }
      });
      emit(state.copyWith(taskFilter: taskFilter, mytasks: state.mytasks));
    }
  }

  String getTodayDate() {
    DateTime now = DateTime.now();
    String today = DateFormat("dd MMMM, EEEE").format(now);
    return today;
  }
}
