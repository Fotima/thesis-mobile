import 'dart:convert';

import 'package:bloc/bloc.dart';
import 'package:chopper/chopper.dart';
import 'package:hospitalproject/api/api_provider.dart';
import 'package:hospitalproject/models/api/chat_room/chat_room.dart';
import 'package:hospitalproject/ui/shared/top_message.dart';
// BLoC object for state management at Chat tab screen

class ChatTabState {
  final bool loading;
  final List<ChatRoom>? mychats;

  ChatTabState({this.loading = true, this.mychats});

  ChatTabState copyWith({
    bool? loading,
    List<ChatRoom>? mychats,
  }) {
    return ChatTabState(
      loading: loading ?? this.loading,
      mychats: mychats ?? this.mychats,
    );
  }
}

class ChatTabBloc extends Cubit<ChatTabState> {
  ChatTabBloc()
      : super(ChatTabState(
          mychats: [],
        ));

// load all chat rooms of a user
  void loadMyChats() async {
    // emit loading state-true
    emit(state.copyWith(loading: true));
    Response<List<ChatRoom>> response =
        await ApiProvider.chatService.getMyChats();
// emit loading state false and
// if the request is successfull,  emit the response to the UI view
    if (response.isSuccessful) {
      emit(state.copyWith(loading: false, mychats: response.body));
    } else {
      // if request is failed, show the error message
      emit(state.copyWith(loading: false));
      String message = 'Error';
      try {
        final Map<String, dynamic> error =
            jsonDecode(response.error.toString());
        message = error['message'];
      } catch (e) {
        message = response.error.toString();
      }
      showMessage(message, isError: true);
    }
  }
}
