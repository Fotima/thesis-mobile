import 'dart:convert';

import 'package:bloc/bloc.dart';
import 'package:chopper/chopper.dart';
import 'package:hospitalproject/api/api_provider.dart';
import 'package:hospitalproject/constants/api/api_contants.dart';
import 'package:hospitalproject/models/api/calls/calls.dart';
import 'package:hospitalproject/models/api/user/user.dart';
import 'package:hospitalproject/ui/shared/top_message.dart';
import 'package:hospitalproject/utils/network/api_helper.dart';
import 'package:hospitalproject/utils/shared_pref/shared_pref_helper.dart';
import 'package:hospitalproject/utils/url_launcher.dart';

// BLoC object for state management at Contacts tab screen

class ContactsState {
  final int currentTab;
  final bool loading;
  final List<UserByRole>? contacts;
  final List<CallsHistory>? callHistory;
  final bool updateCallHistory;
  final bool callHistoryloading;
  final List<String>? permissions;

  ContactsState({
    this.currentTab = 0,
    this.loading = true,
    this.contacts,
    this.callHistory,
    this.updateCallHistory = false,
    this.callHistoryloading = false,
    this.permissions,
  });

  ContactsState copyWith({
    int? currentTab,
    bool? loading,
    List<UserByRole>? contacts,
    List<CallsHistory>? callHistory,
    bool? updateCallHistory,
    bool? callHistoryloading,
    List<String>? permissions,
  }) {
    return ContactsState(
      currentTab: currentTab ?? this.currentTab,
      loading: loading ?? this.loading,
      callHistoryloading: callHistoryloading ?? this.callHistoryloading,
      updateCallHistory: updateCallHistory ?? this.updateCallHistory,
      callHistory: callHistory ?? this.callHistory,
      contacts: contacts ?? this.contacts,
      permissions: permissions ?? this.permissions,
    );
  }
}

class ContactsBloc extends Cubit<ContactsState> {
  ContactsBloc(List<String>? permissions)
      : super(ContactsState(
            contacts: [], callHistory: [], permissions: permissions));

// load list of contacts from the server
  void loadInitialData() async {
    // emit loading state
    emit(state.copyWith(loading: true));
    // get list of permissions of a role
    List<String> permission = SharedPrefHelper.getPermissions();
    Response<List<UserByRole>> contactsResponse;
    // if a user has permission to view all contacts, load list of all users
    if (permission.contains(ApiConstants.messageAllUsers) ||
        permission.contains(ApiConstants.callAllUsers)) {
      contactsResponse =
          await request(ApiProvider.contactsService.getAllUserContacts());
    } else {
      // if user has permission to view users with a lower or equal rank, load the corresponding list from the server
      contactsResponse =
          await request(ApiProvider.contactsService.getAllUserContactsRank());
    }
    // load call history and notes of a user from the server
    Response<List<CallsHistory>> callHistoryResponse =
        await request(ApiProvider.contactsService.getMyCallHistory());

// if the requests are sucessfull, emit the response to the ui view
    if (contactsResponse.isSuccessful && callHistoryResponse.isSuccessful) {
      emit(state.copyWith(
          loading: false,
          contacts: contactsResponse.body,
          callHistory: callHistoryResponse.body));
    } else {
      // else show the error message
      emit(state.copyWith(loading: false));
      showMessage("Something went wrong", isError: true);
    }
  }

// update the list of calls
  void updateCallHistory() async {
    // emit loading state
    emit(state.copyWith(callHistoryloading: true));
    Response<List<CallsHistory>> response =
        await request(ApiProvider.contactsService.getMyCallHistory());
    // emit the result to the UI view
    if (response.isSuccessful) {
      emit(state.copyWith(
        callHistoryloading: false,
        callHistory: response.body,
        updateCallHistory: false,
      ));
    } else {
      // if response is failed, show the error message
      emit(state.copyWith(callHistoryloading: false));
      String message = 'Error';
      try {
        final Map<String, dynamic> error =
            jsonDecode(response.error.toString());
        message = error['message'];
      } catch (e) {
        message = response.error.toString();
      }
      showMessage(message, isError: true);
    }
  }

// add a call history to the calls data

  void createCallHistory(int userId, String phoneNumber) async {
    var start = DateTime.now().millisecondsSinceEpoch;
// push the data to the server
    Response<CallsHistory> response = await request(ApiProvider.contactsService
        .createCallHistory(CallHistoryPost(calledUserId: userId)));
    var result = DateTime.now().millisecondsSinceEpoch - start;
    print('result:$result');
// launch the phone service of the device
    launchTel(phoneNumber);
    if (response.isSuccessful) {
      emit(state.copyWith(updateCallHistory: true));
    } else {
      // if the request is failed, show the message
      String message = 'Error';
      try {
        final Map<String, dynamic> error =
            jsonDecode(response.error.toString());
        message = error['message'];
      } catch (e) {
        message = response.error.toString();
      }
      showMessage(message, isError: true);
    }
  }

// add note to the call history on the server
  void addNoteToCallHistory(int historyId, String note) async {
    // emit loading state true
    emit(state.copyWith(callHistoryloading: true));
    Response<CallsHistory> response = await request(ApiProvider.contactsService
        .addNoteCallHistory(CallHistoryNotePost(id: historyId, note: note)));

// if the request is sucessfull, update the call history on the UI view
    if (response.isSuccessful) {
      updateCallHistory();
    } else {
      // show error message
      emit(state.copyWith(callHistoryloading: false));
      String message = 'Error';
      try {
        final Map<String, dynamic> error =
            jsonDecode(response.error.toString());
        message = error['message'];
      } catch (e) {
        message = response.error.toString();
      }
      showMessage(message, isError: true);
    }
  }

// update list of contacts
  void updateContacts() async {
    List<String> permission = SharedPrefHelper.getPermissions();

    Response<List<UserByRole>> response;
    if (permission.contains(ApiConstants.messageAllUsers) ||
        permission.contains(ApiConstants.callAllUsers)) {
      response =
          await request(ApiProvider.contactsService.getAllUserContacts());
    } else {
      response =
          await request(ApiProvider.contactsService.getAllUserContactsRank());
    }
    if (response.isSuccessful) {
      emit(state.copyWith(
        contacts: response.body,
        updateCallHistory: false,
      ));
    } else {
      String message = 'Error';
      try {
        final Map<String, dynamic> error =
            jsonDecode(response.error.toString());
        message = error['message'];
      } catch (e) {
        message = response.error.toString();
      }
      showMessage(message, isError: true);
    }
  }

// update the tab view
  void updateTabIndex(int index) {
    emit(state.copyWith(currentTab: index));
    if (index == 1 && state.updateCallHistory) {
      updateCallHistory();
    }
  }
}
