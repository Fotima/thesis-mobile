import 'dart:convert';

import 'package:bloc/bloc.dart';
import 'package:chopper/chopper.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:hospitalproject/api/api_provider.dart';
import 'package:hospitalproject/constants/values/route_constants.dart';
import 'package:hospitalproject/models/api/auth/auth.dart';
import 'package:hospitalproject/ui/shared/top_message.dart';
import 'package:hospitalproject/utils/navigator/locator.dart';
import 'package:hospitalproject/utils/navigator/navigation_service.dart';
import 'package:hospitalproject/utils/network/api_helper.dart';
import 'package:hospitalproject/utils/shared_pref/shared_pref_helper.dart';

// BLoC object for state management at Auth screen
class AuthState {
  final bool loading;

  AuthState({
    this.loading = false,
  });

  AuthState copyWith({
    bool? loading,
  }) {
    return AuthState(
      loading: loading ?? this.loading,
    );
  }
}

class AuthBloc extends Cubit<AuthState> {
  AuthBloc() : super(AuthState());

// login to the system
  void login(String userName, String password) async {
    // emit loading state
    emit(state.copyWith(loading: true));
    LoginPost loginPost = LoginPost(password: password, username: userName);
    Response<LoginResponse> response =
        await request(ApiProvider.authService.login(loginPost));
    // if request is sucessfull, save the token from the response body
    if (response.isSuccessful) {
      if (response.body != null) {
        SharedPrefHelper.token = response.body!.accessToken;

        SharedPrefHelper.setMyId = response.body!.user.id;
        if (response.body!.user.role!.rank != null) {
          SharedPrefHelper.setRoleRank = response.body!.user.role!.rank!;
        }

        SharedPrefHelper.setPermissions =
            response.body!.user.role!.permissions ?? [];

        ApiProvider.create(token: response.body!.accessToken);
        // if the user account is not approved or it is a new one, confirm the count with google sign in method
        if (SharedPrefHelper.isInitialOpen() ||
            !response.body!.accountConfirmed) {
          String? email = await signInWithGoogle();
          emit(state.copyWith(loading: false));
          if (email != null) {
            confirmEmail(email, response.body!);
          } else {
            print("Something went wrong, your email data is required ");
          }
        } else {
          // if the account is confirmed and the request is successfull, navigate to the home screen
          emit(state.copyWith(loading: false));
          Navigator.pushNamedAndRemoveUntil(
              locator<NavigationService>().navigatorKey.currentContext!,
              AppRoutes.homePage,
              (route) => false,
              arguments: response.body!);
        }
      }
    } else {
      emit(state.copyWith(loading: false));

      String message = 'Error';
      try {
        final Map<String, dynamic> error =
            jsonDecode(response.error.toString());
        message = error['message'];
      } catch (e) {
        message = response.error.toString();
      }
      showMessage(message, isError: true);
    }
  }

  Future<String?> signInWithGoogle() async {
    final GoogleSignIn googleSignIn = GoogleSignIn(
      scopes: ['email'],
    );
    final FirebaseAuth firebaseAuth = FirebaseAuth.instance;
    GoogleSignInAccount? signInAccount;
    try {
      signInAccount = await googleSignIn.signIn();
    } catch (e) {
      print(e);
    }
    if (signInAccount != null) {
      GoogleSignInAuthentication googleSignInAuthentication =
          await signInAccount.authentication;
      AuthCredential credential = GoogleAuthProvider.credential(
          accessToken: googleSignInAuthentication.accessToken,
          idToken: googleSignInAuthentication.idToken);

      UserCredential userCredential =
          await firebaseAuth.signInWithCredential(credential);
      User? currentUser = firebaseAuth.currentUser;

      if (userCredential.user?.uid == currentUser?.uid) {
        print('the same');
      }

      if (userCredential.user!.email == null) {
        return null;
      } else {
        return userCredential.user!.email!;
      }
    } else {
      return null;
    }
  }
  // confirm the account with google sign in method

  confirmEmail(String email, LoginResponse response) async {
    emit(state.copyWith(loading: true));
    Response response =
        await request(ApiProvider.authService.confirmEmail({"email": email}));
    emit(state.copyWith(loading: false));
    if (response.isSuccessful) {
      // if confirmation is sucessfull, navigate to home screen
      SharedPrefHelper.setInitialOpen = false;
      Navigator.pushNamedAndRemoveUntil(
          locator<NavigationService>().navigatorKey.currentContext!,
          AppRoutes.homePage,
          (route) => false,
          arguments: response);
    } else {
      // else stay on login screen
      String message = 'Error';
      try {
        final Map<String, dynamic> error =
            jsonDecode(response.error.toString());
        message = error['message'];
      } catch (e) {
        message = response.error.toString();
      }
      showMessage(message, isError: true);
    }
  }
}
