import 'dart:convert';
import 'package:hospitalproject/constants/api/url_constants.dart';
import 'package:hospitalproject/models/api/chat_room/chat_room.dart';

import 'package:chopper/chopper.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:hospitalproject/api/api_provider.dart';
import 'package:hospitalproject/models/api/chat/chat.dart';
import 'package:hospitalproject/ui/shared/top_message.dart';
import 'package:hospitalproject/utils/network/api_helper.dart';
import 'package:hospitalproject/utils/shared_pref/shared_pref_helper.dart';
import 'package:stomp_dart_client/stomp.dart';
import 'package:stomp_dart_client/stomp_config.dart';
import 'package:stomp_dart_client/stomp_frame.dart';
import 'package:web_socket_channel/io.dart';

// BLoC object for state management at Chat screen

class ChatState {
  final bool loading;
  final int page;
  final bool pageLoading;
  final List<ChatMessageByDate>? messageByDate;
  final bool webSocketConnected;
  final ChatRoom? chatRoom;
  final IOWebSocketChannel? channel;
  ChatState({
    this.loading = true,
    this.page = 0,
    this.pageLoading = false,
    this.messageByDate,
    this.webSocketConnected = false,
    this.chatRoom,
    this.channel,
  });

  ChatState copyWith({
    bool? loading,
    int? page,
    bool? pageLoading,
    List<ChatMessageByDate>? messageByDate,
    bool? webSocketConnected,
    ChatRoom? chatRoom,
    IOWebSocketChannel? channel,
  }) {
    return ChatState(
      loading: loading ?? this.loading,
      page: page ?? this.page,
      pageLoading: pageLoading ?? this.pageLoading,
      messageByDate: messageByDate ?? this.messageByDate,
      webSocketConnected: webSocketConnected ?? this.webSocketConnected,
      chatRoom: chatRoom ?? this.chatRoom,
      channel: channel ?? this.channel,
    );
  }
}

class ChatBloc extends Cubit<ChatState> {
  ChatBloc()
      : super(ChatState(
          messageByDate: [],
        ));
  late StompClient client;
  // Load message history from the server and emit it to the UI view
  loadChatMessages(int page, int? withUserId, int? chatId) async {
    if (page == 0) {
      emit(state.copyWith(loading: true));
    } else {
      emit(state.copyWith(pageLoading: true));
    }
    Response<ChatMessagePageable> response = await request(
        ApiProvider.chatService.getMessagesOfChat(page,
            ChatMessagePost(chatId: chatId, withUserId: withUserId).toJson()));
    // if request is sucessfull, group messages by date and emit them to the UI view
    if (response.isSuccessful) {
      if (response.body!.content!.isNotEmpty) {
        response.body!.content!.forEach((element) {
          if (state.messageByDate!.any((t) => element.date == t.date)) {
            if (state.messageByDate!
                    .firstWhere((i) => i.date == element.date)
                    .messages ==
                null) {
              state.messageByDate!
                  .firstWhere((i) => i.date == element.date)
                  .messages = [];
            }
            state.messageByDate!
                .firstWhere((i) => i.date == element.date)
                .messages!
                .addAll(element.messages!);
          } else {
            state.messageByDate!.add(element);
          }
        });
      }
      // connect to the web socket service with chat room id and user token
      if (response.body!.chatRoom != null) {
        if (response.body!.chatRoom!.chatId != null) {
          connectToSocket(response.body!.chatRoom!.chatId!);
          // stompConnect(response.body!.chatRoom!.chatId!);
        }
      }
      emit(state.copyWith(
        loading: false,
        pageLoading: false,
        page: page,
        messageByDate: state.messageByDate,
        chatRoom: response.body!.chatRoom,
      ));
    } else {
      // if the request is failed, show the error message
      emit(state.copyWith(
        loading: false,
        pageLoading: false,
      ));
      String message = 'Error';
      try {
        final Map<String, dynamic> error =
            jsonDecode(response.error.toString());
        message = error['message'];
      } catch (e) {
        message = response.error.toString();
      }
      showMessage(message, isError: true);
    }
  }
  // create chat room two users if the conversation is initial
  // connect to the web socket service with new created chat room id

  createChatRoomAndConnectToStomp(int withUserId, String message) async {
    Response<ChatRoom> response =
        await request(ApiProvider.chatService.createChatRoom(withUserId));
    if (response.isSuccessful) {
      emit(state.copyWith(chatRoom: response.body));
      connectToSocket(
        response.body!.chatId!,
        message: message,
      );
    } else {
      String message = 'Error';
      try {
        final Map<String, dynamic> error =
            jsonDecode(response.error.toString());
        message = error['message'];
      } catch (e) {
        message = response.error.toString();
      }
      showMessage(message, isError: true);
    }
  }

// send message to the ws service
  sendMessage(String message) {
    String wsmessage = json.encode(WSMessage(
            data: ChatCreatePost(
                chatId: state.chatRoom!.chatId, message: message))
        .toJson());
    print("sent $wsmessage");
    state.channel!.sink.add(wsmessage);
  }

// add message received from ws service to the group of messages and emit it to the UI view
  addNewMessage(String newMessage) {
    var data = json.decode(newMessage);
    ChatMessage message = ChatMessage.fromJson(data['data']);

    DateTime now = DateTime.now();

    if (state.messageByDate!.isNotEmpty) {
      ChatMessageByDate lastDate = state.messageByDate!.last;
      DateTime date = DateTime.fromMillisecondsSinceEpoch(lastDate.date!);

      if (now.day == date.day &&
          now.month == date.month &&
          now.year == date.year) {
        state.messageByDate!.last.messages!.add(message);
      }
    } else {
      state.messageByDate!.add(ChatMessageByDate(
          date: now.millisecondsSinceEpoch, messages: [message]));
    }
    emit(state.copyWith(messageByDate: state.messageByDate));
  }

// connect to the ws service
  stompConnect(int chatId, {String? message}) {
    String? token = SharedPrefHelper.getToken();
    String url = '${AppUrls.webSocketChat}?chat_id=$chatId&token=$token';

    StompClient client = StompClient(
        config: StompConfig(
            url: url,
            onConnect: onConnectCallback,
            onStompError: (error) {
              print('error');
            }));
  }

  void onConnectCallback(StompFrame connectFrame) {
    // client is connected and ready
    print('client is connected and ready');
  }

  connectToSocket(int chatId, {String? message}) {
    String? token = SharedPrefHelper.getToken();
    if (token != null && !state.webSocketConnected) {
      String url = '${AppUrls.webSocketChat}?chat_id=$chatId&token=$token';
      if (state.channel == null) {
        IOWebSocketChannel ch =
            IOWebSocketChannel.connect(Uri.parse(url), headers: {
          'UPGRADE': 'websocket',
          'CONNECTION': 'upgrade',
          // 'Origin': 'ws://ec2-54-247-109-62.eu-west-1.compute.amazonaws.com'
        });
        emit(state.copyWith(channel: ch, webSocketConnected: true));
      }

      state.channel!.stream.listen((data) {
        addNewMessage(data);
      }, onError: (e) {
        print('error $e');
      }, onDone: () {
        print('Socket connected');
      });
      if (message != null) {
        sendMessage(message);
      }
    }
  }
}
