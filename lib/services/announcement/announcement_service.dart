import 'package:chopper/chopper.dart';
import 'package:hospitalproject/constants/api/url_constants.dart';
import 'package:hospitalproject/models/announcement/announcement.dart';

part 'announcement_service.chopper.dart';

@ChopperApi(baseUrl: AppUrls.base)
abstract class AnnouncementService extends ChopperService {
  static AnnouncementService create([ChopperClient? client]) =>
      _$AnnouncementService(client ?? ChopperClient());

  @Get(path: '${AppUrls.getAllAnnouncement}')
  Future<Response<List<AnnouncementInfo>>> getall();

  @Post(path: '${AppUrls.createAnnouncement}')
  Future<Response<AnnouncementInfo>> createAnnouncement(
      @Body() AnnouncementPost post);
}
