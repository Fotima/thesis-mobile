// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'announcement_service.dart';

// **************************************************************************
// ChopperGenerator
// **************************************************************************

// ignore_for_file: always_put_control_body_on_new_line, always_specify_types, prefer_const_declarations
class _$AnnouncementService extends AnnouncementService {
  _$AnnouncementService([ChopperClient? client]) {
    if (client == null) return;
    this.client = client;
  }

  @override
  final definitionType = AnnouncementService;

  @override
  Future<Response<List<AnnouncementInfo>>> getall() {
    final $url =
        'http://ec2-54-247-109-62.eu-west-1.compute.amazonaws.com/api/v1/announcement/list';
    final $request = Request('GET', $url, client.baseUrl);
    return client.send<List<AnnouncementInfo>, AnnouncementInfo>($request);
  }

  @override
  Future<Response<AnnouncementInfo>> createAnnouncement(AnnouncementPost post) {
    final $url =
        'http://ec2-54-247-109-62.eu-west-1.compute.amazonaws.com/api/v1/announcement/create';
    final $body = post;
    final $request = Request('POST', $url, client.baseUrl, body: $body);
    return client.send<AnnouncementInfo, AnnouncementInfo>($request);
  }
}
