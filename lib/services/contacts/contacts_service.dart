import 'package:chopper/chopper.dart';
import 'package:hospitalproject/constants/api/url_constants.dart';
import 'package:hospitalproject/models/api/calls/calls.dart';
import 'package:hospitalproject/models/api/user/user.dart';
import 'package:hospitalproject/models/api/user_short/user_short.dart';

part 'contacts_service.chopper.dart';

@ChopperApi(baseUrl: AppUrls.base)
abstract class ContactsService extends ChopperService {
  static ContactsService create([ChopperClient? client]) =>
      _$ContactsService(client ?? ChopperClient());

  @Get(path: '${AppUrls.userContacts}')
  Future<Response<List<UserByRole>>> getAllUserContacts();

  @Get(path: '${AppUrls.userContactsRank}')
  Future<Response<List<UserByRole>>> getAllUserContactsRank();

  @Get(path: '${AppUrls.myNotes}')
  Future<Response<List<CallsHistory>>> getMyCallHistory();

  @Post(path: '${AppUrls.createCallHistory}')
  Future<Response<CallsHistory>> createCallHistory(
      @Body() CallHistoryPost post);

  @Post(path: '${AppUrls.addNoteToCallHistory}')
  Future<Response<CallsHistory>> addNoteCallHistory(
      @Body() CallHistoryNotePost post);
}
