// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'contacts_service.dart';

// **************************************************************************
// ChopperGenerator
// **************************************************************************

// ignore_for_file: always_put_control_body_on_new_line, always_specify_types, prefer_const_declarations
class _$ContactsService extends ContactsService {
  _$ContactsService([ChopperClient? client]) {
    if (client == null) return;
    this.client = client;
  }

  @override
  final definitionType = ContactsService;

  @override
  Future<Response<List<UserByRole>>> getAllUserContacts() {
    final $url =
        'http://ec2-54-247-109-62.eu-west-1.compute.amazonaws.com/api/v1/user/contacts';
    final $request = Request('GET', $url, client.baseUrl);
    return client.send<List<UserByRole>, UserByRole>($request);
  }

  @override
  Future<Response<List<UserByRole>>> getAllUserContactsRank() {
    final $url =
        'http://ec2-54-247-109-62.eu-west-1.compute.amazonaws.com/api/v1/user/contacts/rank';
    final $request = Request('GET', $url, client.baseUrl);
    return client.send<List<UserByRole>, UserByRole>($request);
  }

  @override
  Future<Response<List<CallsHistory>>> getMyCallHistory() {
    final $url =
        'http://ec2-54-247-109-62.eu-west-1.compute.amazonaws.com/api/v1/notes/my';
    final $request = Request('GET', $url, client.baseUrl);
    return client.send<List<CallsHistory>, CallsHistory>($request);
  }

  @override
  Future<Response<CallsHistory>> createCallHistory(CallHistoryPost post) {
    final $url =
        'http://ec2-54-247-109-62.eu-west-1.compute.amazonaws.com/api/v1/notes/create';
    final $body = post;
    final $request = Request('POST', $url, client.baseUrl, body: $body);
    return client.send<CallsHistory, CallsHistory>($request);
  }

  @override
  Future<Response<CallsHistory>> addNoteCallHistory(CallHistoryNotePost post) {
    final $url =
        'http://ec2-54-247-109-62.eu-west-1.compute.amazonaws.com/api/v1/notes/addnote';
    final $body = post;
    final $request = Request('POST', $url, client.baseUrl, body: $body);
    return client.send<CallsHistory, CallsHistory>($request);
  }
}
