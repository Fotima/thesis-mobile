import 'package:chopper/chopper.dart';
import 'package:hospitalproject/constants/api/url_constants.dart';
import 'package:hospitalproject/models/api/base/base.dart';
import 'package:hospitalproject/models/api/chat/chat.dart';
import 'package:hospitalproject/models/api/chat_room/chat_room.dart';

part 'chat_service.chopper.dart';

@ChopperApi(baseUrl: AppUrls.base)
abstract class ChatService extends ChopperService {
  static ChatService create([ChopperClient? client]) =>
      _$ChatService(client ?? ChopperClient());

  @Get(path: '${AppUrls.getMyChats}')
  Future<Response<List<ChatRoom>>> getMyChats();

  @Post(path: '${AppUrls.getMyChats}')
  Future<Response<BaseResponse>> sendMessage(@Body() ChatCreatePost post);

  @Get(path: '${AppUrls.getMessagesOfChat}?page={page}')
  Future<Response<ChatMessagePageable>> getMessagesOfChat(
    @Path('page') int page,
    @QueryMap() Map<String, dynamic> query,
  );
  @Get(path: '${AppUrls.createNewChatRoom}?withUserId={withUserId}')
  Future<Response<ChatRoom>> createChatRoom(@Path('withUserId') int withUserId);
}
