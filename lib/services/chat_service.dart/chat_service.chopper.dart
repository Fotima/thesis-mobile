// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'chat_service.dart';

// **************************************************************************
// ChopperGenerator
// **************************************************************************

// ignore_for_file: always_put_control_body_on_new_line, always_specify_types, prefer_const_declarations
class _$ChatService extends ChatService {
  _$ChatService([ChopperClient? client]) {
    if (client == null) return;
    this.client = client;
  }

  @override
  final definitionType = ChatService;

  @override
  Future<Response<List<ChatRoom>>> getMyChats() {
    final $url =
        'http://ec2-54-247-109-62.eu-west-1.compute.amazonaws.com/api/v1/chat/list';
    final $request = Request('GET', $url, client.baseUrl);
    return client.send<List<ChatRoom>, ChatRoom>($request);
  }

  @override
  Future<Response<BaseResponse>> sendMessage(ChatCreatePost post) {
    final $url =
        'http://ec2-54-247-109-62.eu-west-1.compute.amazonaws.com/api/v1/chat/list';
    final $body = post;
    final $request = Request('POST', $url, client.baseUrl, body: $body);
    return client.send<BaseResponse, BaseResponse>($request);
  }

  @override
  Future<Response<ChatMessagePageable>> getMessagesOfChat(
      int page, Map<String, dynamic> query) {
    final $url =
        'http://ec2-54-247-109-62.eu-west-1.compute.amazonaws.com/api/v1/chat/messages?page=$page';
    final $params = query;
    final $request = Request('GET', $url, client.baseUrl, parameters: $params);
    return client.send<ChatMessagePageable, ChatMessagePageable>($request);
  }

  @override
  Future<Response<ChatRoom>> createChatRoom(int withUserId) {
    final $url =
        'http://ec2-54-247-109-62.eu-west-1.compute.amazonaws.com/api/v1/chat/create?withUserId=$withUserId';
    final $request = Request('GET', $url, client.baseUrl);
    return client.send<ChatRoom, ChatRoom>($request);
  }
}
