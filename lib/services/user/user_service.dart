import 'package:chopper/chopper.dart';
import 'package:hospitalproject/constants/api/url_constants.dart';
import 'package:hospitalproject/models/api/user/user.dart';
import 'package:hospitalproject/models/api/user_short/user_short.dart';
part 'user_service.chopper.dart';

@ChopperApi(baseUrl: AppUrls.base)
abstract class UserService extends ChopperService {
  static UserService create([ChopperClient? client]) =>
      _$UserService(client ?? ChopperClient());

  @Get(path: '${AppUrls.myProfileData}')
  Future<Response<UserProfile>> getmyProfileData();

  @Post(path: '${AppUrls.updateMyProfile}')
  Future<Response<UserProfile>> updateMyProfile(
      @Body() UserProfileUpdatePost post);

  @Post(path: '${AppUrls.updateMyStatus}')
  Future<Response> updateMyStatus(@Body() UserStatusPost post);

  @Get(path: '${AppUrls.getAllUsers}')
  Future<Response<List<UserShort>>> getAllUsers();
}
