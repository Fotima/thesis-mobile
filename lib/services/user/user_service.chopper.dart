// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'user_service.dart';

// **************************************************************************
// ChopperGenerator
// **************************************************************************

// ignore_for_file: always_put_control_body_on_new_line, always_specify_types, prefer_const_declarations
class _$UserService extends UserService {
  _$UserService([ChopperClient? client]) {
    if (client == null) return;
    this.client = client;
  }

  @override
  final definitionType = UserService;

  @override
  Future<Response<UserProfile>> getmyProfileData() {
    final $url =
        'http://ec2-54-247-109-62.eu-west-1.compute.amazonaws.com/api/v1/user/mobile/profile';
    final $request = Request('GET', $url, client.baseUrl);
    return client.send<UserProfile, UserProfile>($request);
  }

  @override
  Future<Response<UserProfile>> updateMyProfile(UserProfileUpdatePost post) {
    final $url =
        'http://ec2-54-247-109-62.eu-west-1.compute.amazonaws.com/api/v1/user/mobile/update';
    final $body = post;
    final $request = Request('POST', $url, client.baseUrl, body: $body);
    return client.send<UserProfile, UserProfile>($request);
  }

  @override
  Future<Response<dynamic>> updateMyStatus(UserStatusPost post) {
    final $url =
        'http://ec2-54-247-109-62.eu-west-1.compute.amazonaws.com/api/v1/user/status';
    final $body = post;
    final $request = Request('POST', $url, client.baseUrl, body: $body);
    return client.send<dynamic, dynamic>($request);
  }

  @override
  Future<Response<List<UserShort>>> getAllUsers() {
    final $url =
        'http://ec2-54-247-109-62.eu-west-1.compute.amazonaws.com/api/v1/user/list';
    final $request = Request('GET', $url, client.baseUrl);
    return client.send<List<UserShort>, UserShort>($request);
  }
}
