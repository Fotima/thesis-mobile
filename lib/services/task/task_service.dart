import 'package:chopper/chopper.dart';
import 'package:hospitalproject/constants/api/url_constants.dart';
import 'package:hospitalproject/models/api/role/role.dart';
import 'package:hospitalproject/models/api/task/task.dart';
import 'package:hospitalproject/models/api/user_short/user_short.dart';

part 'task_service.chopper.dart';

@ChopperApi(baseUrl: AppUrls.base)
abstract class TaskService extends ChopperService {
  static TaskService create([ChopperClient? client]) =>
      _$TaskService(client ?? ChopperClient());

  @Get(path: '${AppUrls.getMyTasks}')
  Future<Response<List<TaskInfo>>> getMyTasks();

  @Post(path: '${AppUrls.updateTaskStatus}')
  Future<Response<TaskInfo>> updateTaskStatus(
      @Body() TaskStatusPost taskStatusPost);

  @Get(path: '${AppUrls.roleLowerRank}')
  Future<Response<List<RoleShort>>> getRolesLowerRank();

  @Get(path: '${AppUrls.usersLowerRank}')
  Future<Response<List<UserShort>>> getUsersLowerRank();

  @Post(path: '${AppUrls.createTask}')
  Future<Response<TaskInfo>> createTask(@Body() TaskCreate taskCreate);

  @Post(path: '${AppUrls.filterTasks}')
  Future<Response<List<TaskInfo>>> filterTasks(@Body() TaskFilterPost post);
}
