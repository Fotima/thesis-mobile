// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'task_service.dart';

// **************************************************************************
// ChopperGenerator
// **************************************************************************

// ignore_for_file: always_put_control_body_on_new_line, always_specify_types, prefer_const_declarations
class _$TaskService extends TaskService {
  _$TaskService([ChopperClient? client]) {
    if (client == null) return;
    this.client = client;
  }

  @override
  final definitionType = TaskService;

  @override
  Future<Response<List<TaskInfo>>> getMyTasks() {
    final $url =
        'http://ec2-54-247-109-62.eu-west-1.compute.amazonaws.com/api/v1/task/me';
    final $request = Request('GET', $url, client.baseUrl);
    return client.send<List<TaskInfo>, TaskInfo>($request);
  }

  @override
  Future<Response<TaskInfo>> updateTaskStatus(TaskStatusPost taskStatusPost) {
    final $url =
        'http://ec2-54-247-109-62.eu-west-1.compute.amazonaws.com/api/v1/task/status/update';
    final $body = taskStatusPost;
    final $request = Request('POST', $url, client.baseUrl, body: $body);
    return client.send<TaskInfo, TaskInfo>($request);
  }

  @override
  Future<Response<List<RoleShort>>> getRolesLowerRank() {
    final $url =
        'http://ec2-54-247-109-62.eu-west-1.compute.amazonaws.com/api/v1/role/lowerRank';
    final $request = Request('GET', $url, client.baseUrl);
    return client.send<List<RoleShort>, RoleShort>($request);
  }

  @override
  Future<Response<List<UserShort>>> getUsersLowerRank() {
    final $url =
        'http://ec2-54-247-109-62.eu-west-1.compute.amazonaws.com/api/v1/user/byRank';
    final $request = Request('GET', $url, client.baseUrl);
    return client.send<List<UserShort>, UserShort>($request);
  }

  @override
  Future<Response<TaskInfo>> createTask(TaskCreate taskCreate) {
    final $url =
        'http://ec2-54-247-109-62.eu-west-1.compute.amazonaws.com/api/v1/task/create';
    final $body = taskCreate;
    final $request = Request('POST', $url, client.baseUrl, body: $body);
    return client.send<TaskInfo, TaskInfo>($request);
  }

  @override
  Future<Response<List<TaskInfo>>> filterTasks(TaskFilterPost post) {
    final $url =
        'http://ec2-54-247-109-62.eu-west-1.compute.amazonaws.com/api/v1/task/filter';
    final $body = post;
    final $request = Request('POST', $url, client.baseUrl, body: $body);
    return client.send<List<TaskInfo>, TaskInfo>($request);
  }
}
