import 'package:chopper/chopper.dart';
import 'package:hospitalproject/constants/api/url_constants.dart';
import 'package:hospitalproject/models/api/auth/auth.dart';
import 'package:hospitalproject/models/api/base/base.dart';

part 'auth_service.chopper.dart';

@ChopperApi(baseUrl: AppUrls.base)
abstract class AuthService extends ChopperService {
  static AuthService create([ChopperClient? client]) =>
      _$AuthService(client ?? ChopperClient());

  @Post(path: '${AppUrls.login}')
  Future<Response<LoginResponse>> login(@Body() LoginPost loginPost);

  @Post(path: '${AppUrls.emailConfirm}')
  Future<Response> confirmEmail(@Body() Map post);

  @Post(path: '${AppUrls.updateFirebaseToken}/{token}')
  Future<Response<BaseResponse>> updateFirebaseToken(
      @Path('token') String token);
}
